#version 450 core

layout(location = 0) in vec3 ia_Position;
layout(location = 1) in vec3 ia_Velocity;
layout(location = 2) in float ia_Age;
layout(location = 3) in vec4 ia_Color;

uniform mat4 u_MVP;
uniform mat4 u_MV;
uniform mat4 u_P;

uniform float u_NearZ;
uniform float u_FarZ;

layout(location = 0) out float out_Age;
layout(location = 1) out vec4 out_Color;
layout(location = 2) out vec2 out_Texcoord;
layout(location = 3) out float out_VolZ;

vec2 ComputeTexcoord() {
  if (gl_VertexID == 0) return vec2(0.5, 0.5);
  int id = (gl_VertexID - 1) % 4;
  vec2 xs[] = vec2[](vec2(0.0, 1.0), vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(0.0, 0.0));
  return xs[id];
}

vec3 ComputePosition() {
  if (gl_VertexID == 0) return vec3(0.0, 0.0, 0.0);
  int id = (gl_VertexID - 1) % 4;
  vec3 xs[] = vec3[](vec3(-1.0, -1.0, 0.0), vec3(1.0, -1.0, 0.0), vec3(1.0, 1.0, 0.0), vec3(-1.0, 1.0, 0.0));
  return xs[id];
}

void main() {
  vec2 tc = ComputeTexcoord();
  vec3 unitOffset = ComputePosition();
  vec3 position = 0.01 * unitOffset + (u_MV * vec4(ia_Position, 1.0)).xyz;
  gl_Position = u_P * vec4(position, 1.0);
  out_Age = ia_Age;
  out_Color = ia_Color;
  out_Texcoord = tc;
  float viewZ = (u_MV * vec4(ia_Position, 1.0)).z;
  out_VolZ = (viewZ - u_NearZ) / (u_FarZ - u_NearZ);
}
