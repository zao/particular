#version 450 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

layout(location = 0) in float in_Age[];
layout(location = 1) in vec4 in_Color[];
layout(location = 2) in vec2 in_Texcoord[];
layout(location = 3) in float in_VolZ[];

layout(location = 0) out float out_Age;
layout(location = 1) out vec4 out_Color;
layout(location = 2) out vec2 out_Texcoord;

uniform int u_SliceCount;

void main() {
	for (int i = 0; i < 3; ++i) {
		vec4 pos = gl_in[i].gl_Position;
		float relZ = clamp(in_VolZ[i], 0.0, 1.0);
		gl_Layer = int(relZ * (u_SliceCount - 1));
		gl_Position = pos;
		out_Age = relZ;
		out_Color = in_Color[i];
		out_Texcoord = in_Texcoord[i];
		EmitVertex();
	}
	EndPrimitive();
}
