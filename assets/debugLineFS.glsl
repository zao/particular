#version 450 core

in vec4 a_Color;

out vec4 rs_Color;

void main() {
	rs_Color = a_Color;
}
