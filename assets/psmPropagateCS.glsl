#version 450 core

layout(local_size_x = 1, local_size_y = 1) in;

layout(r8) uniform restrict image3D u_Volume;

void main() {
	uvec2 xy = gl_GlobalInvocationID.xy;
	uint sliceCount = imageSize(u_Volume).z;
	for (uint slice = 1; slice < sliceCount; ++slice) {
		float src = imageLoad(u_Volume, ivec3(xy, slice-1)).x;
		float dst = imageLoad(u_Volume, ivec3(xy, slice)).x;
		imageStore(u_Volume, ivec3(xy, slice), vec4(src * dst));
	}
}
