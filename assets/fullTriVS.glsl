#version 450 core

out vec2 a_Texcoord;

void main() {
  vec2 p[] = vec2[](vec2(-1.0, -1.0), vec2(3.0, -1.0), vec2(-1.0, 3.0));
  vec2 tc[] = vec2[](vec2(0.0, 0.0), vec2(2.0, 0.0), vec2(0.0, 2.0));
  gl_Position = vec4(p[gl_VertexID], 0.0, 1.0);
  a_Texcoord = tc[gl_VertexID];
}
