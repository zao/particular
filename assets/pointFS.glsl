#version 450 core

in float a_Age;
in vec4 a_Color;
in vec2 a_Texcoord;
in vec4 a_PositionW;
in vec4 a_PositionV;

out vec4 rs_Color;

uniform sampler2D u_DiffuseTex;
uniform sampler2D u_NormalTex;
uniform sampler3D u_PSMTex;
uniform float u_PSMNearZ;
uniform float u_PSMFarZ;
uniform mat4 u_SpotV;
uniform mat4 u_SpotP;
uniform float u_SpotCos;
uniform float u_SpotConstantF;
uniform float u_SpotLinearF;
uniform float u_SpotQuadraticF;

uniform float u_Meh;

float SamplePSM(vec3 positionL) {
	vec4 positionLP = u_SpotP * vec4(positionL, 1.0);
	positionLP.xyz /= positionLP.w;
	float zMap = (positionL.z - u_PSMNearZ) / (u_PSMFarZ - u_PSMNearZ);
	zMap -= 0.01;
	return texture(u_PSMTex, vec3(positionLP.xy / 2.0 + 0.5, zMap)).r;
}

void main() {
	vec3 positionL = (u_SpotV * a_PositionW).xyz;

	vec4 c = texture(u_DiffuseTex, a_Texcoord);
	vec3 n = texture(u_NormalTex, a_Texcoord).rgb * 2.0 - 1.0;
	n *= vec3(-1.0, -1.0, -1.0);

	vec3 lightDir = -normalize(positionL);
	float spotEffect = dot(-lightDir, vec3(0.0, 0.0, 1.0));
	float I = 0.0;
	if (spotEffect > u_SpotCos) {
		float dist = length(positionL);
		float shade = SamplePSM(positionL);
		float attenuation = 1.0 / (
			u_SpotConstantF +
			u_SpotLinearF * dist +
			u_SpotQuadraticF * dist * dist);
		I += shade * attenuation;
	}
	float a = c.a * a_Color.a;
	rs_Color = vec4(I * c.rgb * a_Color.rgb, 1.0) * a;
}
