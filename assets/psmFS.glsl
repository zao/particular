#version 450 core

layout(location = 0) in float out_Age;
layout(location = 1) in vec4 out_Color;
layout(location = 2) in vec2 out_Texcoord;

out vec4 rs_Color;

uniform sampler2D u_ColorTex;

void main() {
  vec4 c = texture(u_ColorTex, out_Texcoord);
  rs_Color = 1.0 - (c.aaaa * out_Color.aaaa);
}
