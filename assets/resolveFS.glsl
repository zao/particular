#version 450 core

in vec2 a_Texcoord;

out vec4 rs_Color;

uniform sampler2D u_ColorTex;

void main() {
  vec4 vIn = texture(u_ColorTex, a_Texcoord);
  rs_Color = vec4(vIn.xyz / (vIn.xyz + 1.0), 1.0) * vIn.a;
  rs_Color = vIn;
}
