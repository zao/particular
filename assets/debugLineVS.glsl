#version 450 core

layout(location = 0) in vec3 ia_Position;
layout(location = 1) in vec4 ia_Color;

uniform mat4 u_VP;

out vec4 a_Color;

void main() {
	gl_Position = u_VP * vec4(ia_Position, 1.0);
	a_Color = ia_Color;
}
