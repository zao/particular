#pragma once

#include <cstdio>

namespace util {
    inline std::unique_ptr<std::vector<char>> SlurpTextFile(char const* filename) {
        FILE* fh = fopen(filename, "r");
        if (!fh) return {};
        fseek(fh, 0, SEEK_END);
        auto cb = ftell(fh);
        fseek(fh, 0, SEEK_SET);
        std::unique_ptr<std::vector<char>> ret = std::make_unique<std::vector<char>>(cb + 1);
        if (cb) {
            fread(ret->data(), ret->size(), 1, fh);
        }
        fclose(fh);
        return ret;
    }
}