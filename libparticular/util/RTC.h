#pragma once

#include <Windows.h>

namespace util {
	struct RTC {
		RTC() {
            Reset();
		}

        void Reset() {
            freq = GetTSF();
            base = GetTSC();
            now = base;
        }

		uint64_t GetTSF() const {
			uint64_t x;
			QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&x));
			return x;
		}

		uint64_t GetTSC() const {
			uint64_t x;
			QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&x));
			return x;
		}

		void Update() {
			now = GetTSC();
		}

		double Now() const {
			return static_cast<double>(now - base) / freq;
		}

		uint64_t freq;
		uint64_t base;
		uint64_t now;
	};
}