#pragma once

#include "particular/OpenGLIncludes.h"

#include <memory>
#include <string>
#include <vector>

namespace util {
	using ResourcePtr = std::shared_ptr<GLuint>;

	inline ResourcePtr CreateBuffer() {
		auto ret = ResourcePtr(new GLuint{}, [&](GLuint* p) { glDeleteBuffers(1, p); delete p; });
		glCreateBuffers(1, ret.get());
		return ret;
	}

	inline ResourcePtr CreateFramebuffer() {
		auto ret = ResourcePtr(new GLuint{}, [&](GLuint* p) { glDeleteFramebuffers(1, p); delete p; });
		glGenFramebuffers(1, ret.get());
		return ret;
	}

	inline ResourcePtr CreateProgram() {
		auto ret = ResourcePtr(new GLuint{}, [&](GLuint* p) { glDeleteProgram(*p); delete p; });
		*ret = glCreateProgram();
		return ret;
	}
	
	inline ResourcePtr CreateShader(GLenum type) {
		auto ret = ResourcePtr(new GLuint{}, [&](GLuint* p) { glDeleteShader(*p); delete p; });
		*ret = glCreateShader(type);
		return ret;
	}

	inline ResourcePtr CreateTexture(GLenum type) {
		auto ret = ResourcePtr(new GLuint{}, [&](GLuint* p) { glDeleteTextures(1, p); delete p; });
		glCreateTextures(type, 1, ret.get());
		return ret;
	}

	inline ResourcePtr CreateVertexArray() {
		auto ret = ResourcePtr(new GLuint{}, [&](GLuint* p) { glDeleteVertexArrays(1, p); delete p; });
		glCreateVertexArrays(1, ret.get());
		return ret;
	}

	struct UniformEntry {
		GLint location;
		std::string name;
	};

    struct UniformBlock {
        GLint index;
        std::string name;
    };

	struct CompileResult {
		ResourcePtr id;
		bool success;
		std::string infoLog;
		GLenum type;
	};

	struct LinkResult {
		ResourcePtr id;
		bool success;
		std::string infoLog;
		std::vector<UniformEntry> uniforms;
        std::vector<UniformBlock> uniformBlocks;
	};

	enum CompileFlags {
		CompileFlags_None = 0,
	};

	inline CompileResult CompileShader(GLenum type, char const* data, size_t size, CompileFlags = CompileFlags_None) {
		CompileResult ret{};
		ret.id = CreateShader(type);
		ret.type = type;
		auto id = *ret.id;
		GLint sourceLength = static_cast<GLint>(size);
		glShaderSource(id, 1, &data, &sourceLength);
		glCompileShader(id);
		GLint status{}, logLength{};
		glGetShaderiv(id, GL_COMPILE_STATUS, &status);
		ret.success = status == GL_TRUE;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 0) {
			std::vector<GLchar> buf(logLength);
			glGetShaderInfoLog(id, buf.size(), nullptr, buf.data());
			ret.infoLog = buf.data();
		}
		return ret;
	}

    inline CompileResult CompileShader(GLenum type, std::vector<char> const* data, CompileFlags flags = CompileFlags_None) {
        return CompileShader(type, data->data(), data->size(), flags);
    }

    inline CompileResult CompileShader(GLenum type, std::shared_ptr<std::vector<char>> const& data, CompileFlags flags = CompileFlags_None) {
        return CompileShader(type, data->data(), data->size(), flags);
    }

    inline CompileResult CompileShader(GLenum type, std::unique_ptr<std::vector<char>> const& data, CompileFlags flags = CompileFlags_None) {
        return CompileShader(type, data->data(), data->size(), flags);
    }

    inline CompileResult CompileShader(GLenum type, std::vector<char> const& data, CompileFlags flags = CompileFlags_None) {
        return CompileShader(type, data.data(), data.size(), flags);
    }

	enum LinkFlags {
		LinkFlags_None = 0,
		LinkFlags_EnumerateUniforms = 1,
	};

	inline LinkResult LinkProgram(std::initializer_list<ResourcePtr> shaders, LinkFlags flags = LinkFlags_None) {
		LinkResult ret{};
		ret.id = CreateProgram();
		auto id = *ret.id;
        for (auto& shader : shaders) {
            glAttachShader(id, *shader);
        }
		glLinkProgram(id);
		GLint status{}, logLength{};
		glGetProgramiv(id, GL_LINK_STATUS, &status);
		ret.success = status == GL_TRUE;
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 0) {
			std::vector<GLchar> buf(logLength);
			glGetProgramInfoLog(id, buf.size(), nullptr, buf.data());
			ret.infoLog = buf.data();
		}
		if (flags & LinkFlags_EnumerateUniforms) {
			GLint uniformCount{};
			GLint maxCB{};
			glGetProgramiv(id, GL_ACTIVE_UNIFORMS, &uniformCount);
			glGetProgramiv(id, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxCB);
			std::vector<GLchar> nameBuf(maxCB);
			for (GLint i = 0; i < uniformCount; ++i) {
				GLint size{};
				GLenum type{};
				glGetActiveUniform(id, i, nameBuf.size(), nullptr, &size, &type, nameBuf.data());
				ret.uniforms.push_back({i, nameBuf.data()});
			}
            GLint blockCount{};
            glGetProgramiv(id, GL_ACTIVE_UNIFORM_BLOCKS, &blockCount);
            glGetProgramiv(id, GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH, &maxCB);
            nameBuf.resize(maxCB);
            for (GLint i = 0; i < blockCount; ++i) {
                glGetActiveUniformBlockName(id, i, maxCB, nullptr, nameBuf.data());
                ret.uniformBlocks.push_back({i, nameBuf.data()});
            }
		}
		return ret;
	}
}