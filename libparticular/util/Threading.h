#pragma once

#define PC_SERIALIZE_WORK 0

#include <Windows.h>

#include <atomic>
#include <condition_variable>
#include <deque>
#include <future>
#include <mutex>
#include <thread>
#include <vector>

namespace threading {
    DWORD const MS_VC_EXCEPTION = 0x406D1388;

    #pragma pack(push,8)
    typedef struct tagTHREADNAME_INFO
    {
        DWORD dwType; // Must be 0x1000.
        LPCSTR szName; // Pointer to name (in user addr space).
        DWORD dwThreadID; // Thread ID (-1=caller thread).
        DWORD dwFlags; // Reserved for future use, must be zero.
    } THREADNAME_INFO;
    #pragma pack(pop)

    static void SetThreadName(DWORD dwThreadID, char const* threadName)
    {
        THREADNAME_INFO info;
        info.dwType = 0x1000;
        info.szName = threadName;
        info.dwThreadID = dwThreadID;
        info.dwFlags = 0;

        __try {
            RaiseException( MS_VC_EXCEPTION, 0, sizeof(info)/sizeof(ULONG_PTR), (ULONG_PTR*)&info );
        }
        __except(EXCEPTION_EXECUTE_HANDLER) {}
    }

    struct Worker {
        std::unique_ptr<std::thread> handle;
        std::atomic<bool> terminate{false};
    };
    struct WorkQueue {
        using Fun = std::function<void()>;
        std::mutex mutex;
        std::condition_variable wake;
        std::deque<Fun> work;

        template <typename Op>
        void Enqueue(Op&& op) {
#if PC_SERIALIZE_WORK
            op();
#else
            std::unique_lock<std::mutex> lk(mutex);
            work.push_back(std::move(op));
            wake.notify_one();
#endif
        }
    };

    struct WorkSet {
        explicit WorkSet(size_t count)
            : completions(count)
        {}

        void Complete(size_t id) {
            completions[id].set_value();
        }

        void Wait() {
            for (auto& p : completions) {
                p.get_future().wait();
            }
        }

        std::vector<std::promise<void>> completions;
    };
}