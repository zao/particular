#if 1
//! Structure describing PCM audio data format, with basic helper functions.
struct t_pcmspec
{
	inline t_pcmspec() {reset();}
	inline t_pcmspec(const t_pcmspec & p_source) {*this = p_source;}
	unsigned m_sample_rate;
	unsigned m_bits_per_sample;
	unsigned m_channels,m_channel_config;
	bool m_float;

	inline unsigned align() const {return (m_bits_per_sample / 8) * m_channels;}

	unsigned time_to_bytes(double p_time) const {return (unsigned) (p_time * m_sample_rate) * (m_bits_per_sample / 8) * m_channels;}
	double bytes_to_time(unsigned p_bytes) const {return (double) (p_bytes / ((m_bits_per_sample / 8) * m_channels)) / (double) m_sample_rate;}

	inline bool operator==(/*const t_pcmspec & p_spec1,*/const t_pcmspec & p_spec2) const
	{
		return /*p_spec1.*/m_sample_rate == p_spec2.m_sample_rate 
			&& /*p_spec1.*/m_bits_per_sample == p_spec2.m_bits_per_sample
			&& /*p_spec1.*/m_channels == p_spec2.m_channels
			&& /*p_spec1.*/m_channel_config == p_spec2.m_channel_config
			&& /*p_spec1.*/m_float == p_spec2.m_float;
	}

	inline bool operator!=(/*const t_pcmspec & p_spec1,*/const t_pcmspec & p_spec2) const
	{
		return !(*this == p_spec2);
	}

	inline void reset() {m_sample_rate = 0; m_bits_per_sample = 0; m_channels = 0; m_channel_config = 0; m_float = false;}
	inline bool is_valid() const
	{
		return m_sample_rate >= 1000 && m_sample_rate <= 1000000 &&
			m_channels > 0 && m_channels <= 256 && m_channel_config != 0 &&
			(m_bits_per_sample == 8 || m_bits_per_sample == 16 || m_bits_per_sample == 24 || m_bits_per_sample == 32);
	}
};
#endif

class WaveOut {
public:
	WaveOut();
	MMRESULT Open(const t_pcmspec & p_spec,double p_buffer_length,unsigned p_device);
	void Close();

	~WaveOut();
	
	MMRESULT WriteData(const void * p_data,unsigned p_size);

	double GetLatency();
	unsigned GetLatencyBytes();

	MMRESULT Flush();

  
	MMRESULT Pause(bool p_state);
	MMRESULT ForcePlay();
	unsigned CanWrite();
	inline bool IsPaused() {return m_paused;}
	
	MMRESULT SetVolume(double p_volume);

	MMRESULT Update();

private:

	

	typedef struct tagHEADER
	{
		tagHEADER * next;
		WAVEHDR hdr;
	} HEADER;
	HEADER * hdr_alloc();
	void hdr_free(HEADER * h);
	static void hdr_free_list(HEADER * h);
	
	HWAVEOUT hWo;
	HEADER *hdrs,*hdrs_free;
	t_pcmspec m_pcmspec;
	char * buffer;
	UINT buf_size,buf_size_used;
	UINT data_written,write_ptr;
	unsigned m_maxblock;

	__int64 total_written,total_played;

	UINT n_playing;
	bool m_paused,m_needplay;
	unsigned m_avgblock;

	double m_volume;


	void killwaveout();
	MMRESULT flush();
	void advance_block();

	void reset_stuff();
	
};
