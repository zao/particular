/*

    This file was generated with gl3w_gen.py, part of gl3w
    (hosted at https://github.com/skaslev/gl3w)

    This is free and unencumbered software released into the public domain.

    Anyone is free to copy, modify, publish, use, compile, sell, or
    distribute this software, either in source code form or as a compiled
    binary, for any purpose, commercial or non-commercial, and by any
    means.

    In jurisdictions that recognize copyright laws, the author or authors
    of this software dedicate any and all copyright interest in the
    software to the public domain. We make this dedication for the benefit
    of the public at large and to the detriment of our heirs and
    successors. We intend this dedication to be an overt act of
    relinquishment in perpetuity of all present and future rights to this
    software under copyright law.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
    OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.

*/

#ifndef __gl3w_h_
#define __gl3w_h_

#include <GL/glcorearb.h>

#ifndef __gl_h_
#define __gl_h_
#endif

#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _GL3WContext {
    int major;
    int minor;
    PFNGLACTIVESHADERPROGRAMPROC                         gl3wActiveShaderProgram;
    PFNGLACTIVETEXTUREPROC                               gl3wActiveTexture;
    PFNGLATTACHSHADERPROC                                gl3wAttachShader;
    PFNGLBEGINCONDITIONALRENDERPROC                      gl3wBeginConditionalRender;
    PFNGLBEGINQUERYPROC                                  gl3wBeginQuery;
    PFNGLBEGINQUERYINDEXEDPROC                           gl3wBeginQueryIndexed;
    PFNGLBEGINTRANSFORMFEEDBACKPROC                      gl3wBeginTransformFeedback;
    PFNGLBINDATTRIBLOCATIONPROC                          gl3wBindAttribLocation;
    PFNGLBINDBUFFERPROC                                  gl3wBindBuffer;
    PFNGLBINDBUFFERBASEPROC                              gl3wBindBufferBase;
    PFNGLBINDBUFFERRANGEPROC                             gl3wBindBufferRange;
    PFNGLBINDBUFFERSBASEPROC                             gl3wBindBuffersBase;
    PFNGLBINDBUFFERSRANGEPROC                            gl3wBindBuffersRange;
    PFNGLBINDFRAGDATALOCATIONPROC                        gl3wBindFragDataLocation;
    PFNGLBINDFRAGDATALOCATIONINDEXEDPROC                 gl3wBindFragDataLocationIndexed;
    PFNGLBINDFRAMEBUFFERPROC                             gl3wBindFramebuffer;
    PFNGLBINDIMAGETEXTUREPROC                            gl3wBindImageTexture;
    PFNGLBINDIMAGETEXTURESPROC                           gl3wBindImageTextures;
    PFNGLBINDPROGRAMPIPELINEPROC                         gl3wBindProgramPipeline;
    PFNGLBINDRENDERBUFFERPROC                            gl3wBindRenderbuffer;
    PFNGLBINDSAMPLERPROC                                 gl3wBindSampler;
    PFNGLBINDSAMPLERSPROC                                gl3wBindSamplers;
    PFNGLBINDTEXTUREPROC                                 gl3wBindTexture;
    PFNGLBINDTEXTUREUNITPROC                             gl3wBindTextureUnit;
    PFNGLBINDTEXTURESPROC                                gl3wBindTextures;
    PFNGLBINDTRANSFORMFEEDBACKPROC                       gl3wBindTransformFeedback;
    PFNGLBINDVERTEXARRAYPROC                             gl3wBindVertexArray;
    PFNGLBINDVERTEXBUFFERPROC                            gl3wBindVertexBuffer;
    PFNGLBINDVERTEXBUFFERSPROC                           gl3wBindVertexBuffers;
    PFNGLBLENDCOLORPROC                                  gl3wBlendColor;
    PFNGLBLENDEQUATIONPROC                               gl3wBlendEquation;
    PFNGLBLENDEQUATIONSEPARATEPROC                       gl3wBlendEquationSeparate;
    PFNGLBLENDEQUATIONSEPARATEIPROC                      gl3wBlendEquationSeparatei;
    PFNGLBLENDEQUATIONSEPARATEIARBPROC                   gl3wBlendEquationSeparateiARB;
    PFNGLBLENDEQUATIONIPROC                              gl3wBlendEquationi;
    PFNGLBLENDEQUATIONIARBPROC                           gl3wBlendEquationiARB;
    PFNGLBLENDFUNCPROC                                   gl3wBlendFunc;
    PFNGLBLENDFUNCSEPARATEPROC                           gl3wBlendFuncSeparate;
    PFNGLBLENDFUNCSEPARATEIPROC                          gl3wBlendFuncSeparatei;
    PFNGLBLENDFUNCSEPARATEIARBPROC                       gl3wBlendFuncSeparateiARB;
    PFNGLBLENDFUNCIPROC                                  gl3wBlendFunci;
    PFNGLBLENDFUNCIARBPROC                               gl3wBlendFunciARB;
    PFNGLBLITFRAMEBUFFERPROC                             gl3wBlitFramebuffer;
    PFNGLBLITNAMEDFRAMEBUFFERPROC                        gl3wBlitNamedFramebuffer;
    PFNGLBUFFERDATAPROC                                  gl3wBufferData;
    PFNGLBUFFERPAGECOMMITMENTARBPROC                     gl3wBufferPageCommitmentARB;
    PFNGLBUFFERSTORAGEPROC                               gl3wBufferStorage;
    PFNGLBUFFERSUBDATAPROC                               gl3wBufferSubData;
    PFNGLCHECKFRAMEBUFFERSTATUSPROC                      gl3wCheckFramebufferStatus;
    PFNGLCHECKNAMEDFRAMEBUFFERSTATUSPROC                 gl3wCheckNamedFramebufferStatus;
    PFNGLCLAMPCOLORPROC                                  gl3wClampColor;
    PFNGLCLEARPROC                                       gl3wClear;
    PFNGLCLEARBUFFERDATAPROC                             gl3wClearBufferData;
    PFNGLCLEARBUFFERSUBDATAPROC                          gl3wClearBufferSubData;
    PFNGLCLEARBUFFERFIPROC                               gl3wClearBufferfi;
    PFNGLCLEARBUFFERFVPROC                               gl3wClearBufferfv;
    PFNGLCLEARBUFFERIVPROC                               gl3wClearBufferiv;
    PFNGLCLEARBUFFERUIVPROC                              gl3wClearBufferuiv;
    PFNGLCLEARCOLORPROC                                  gl3wClearColor;
    PFNGLCLEARDEPTHPROC                                  gl3wClearDepth;
    PFNGLCLEARDEPTHFPROC                                 gl3wClearDepthf;
    PFNGLCLEARNAMEDBUFFERDATAPROC                        gl3wClearNamedBufferData;
    PFNGLCLEARNAMEDBUFFERSUBDATAPROC                     gl3wClearNamedBufferSubData;
    PFNGLCLEARNAMEDFRAMEBUFFERFIPROC                     gl3wClearNamedFramebufferfi;
    PFNGLCLEARNAMEDFRAMEBUFFERFVPROC                     gl3wClearNamedFramebufferfv;
    PFNGLCLEARNAMEDFRAMEBUFFERIVPROC                     gl3wClearNamedFramebufferiv;
    PFNGLCLEARNAMEDFRAMEBUFFERUIVPROC                    gl3wClearNamedFramebufferuiv;
    PFNGLCLEARSTENCILPROC                                gl3wClearStencil;
    PFNGLCLEARTEXIMAGEPROC                               gl3wClearTexImage;
    PFNGLCLEARTEXSUBIMAGEPROC                            gl3wClearTexSubImage;
    PFNGLCLIENTWAITSYNCPROC                              gl3wClientWaitSync;
    PFNGLCLIPCONTROLPROC                                 gl3wClipControl;
    PFNGLCOLORMASKPROC                                   gl3wColorMask;
    PFNGLCOLORMASKIPROC                                  gl3wColorMaski;
    PFNGLCOMPILESHADERPROC                               gl3wCompileShader;
    PFNGLCOMPILESHADERINCLUDEARBPROC                     gl3wCompileShaderIncludeARB;
    PFNGLCOMPRESSEDTEXIMAGE1DPROC                        gl3wCompressedTexImage1D;
    PFNGLCOMPRESSEDTEXIMAGE2DPROC                        gl3wCompressedTexImage2D;
    PFNGLCOMPRESSEDTEXIMAGE3DPROC                        gl3wCompressedTexImage3D;
    PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC                     gl3wCompressedTexSubImage1D;
    PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC                     gl3wCompressedTexSubImage2D;
    PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC                     gl3wCompressedTexSubImage3D;
    PFNGLCOMPRESSEDTEXTURESUBIMAGE1DPROC                 gl3wCompressedTextureSubImage1D;
    PFNGLCOMPRESSEDTEXTURESUBIMAGE2DPROC                 gl3wCompressedTextureSubImage2D;
    PFNGLCOMPRESSEDTEXTURESUBIMAGE3DPROC                 gl3wCompressedTextureSubImage3D;
    PFNGLCOPYBUFFERSUBDATAPROC                           gl3wCopyBufferSubData;
    PFNGLCOPYIMAGESUBDATAPROC                            gl3wCopyImageSubData;
    PFNGLCOPYNAMEDBUFFERSUBDATAPROC                      gl3wCopyNamedBufferSubData;
    PFNGLCOPYTEXIMAGE1DPROC                              gl3wCopyTexImage1D;
    PFNGLCOPYTEXIMAGE2DPROC                              gl3wCopyTexImage2D;
    PFNGLCOPYTEXSUBIMAGE1DPROC                           gl3wCopyTexSubImage1D;
    PFNGLCOPYTEXSUBIMAGE2DPROC                           gl3wCopyTexSubImage2D;
    PFNGLCOPYTEXSUBIMAGE3DPROC                           gl3wCopyTexSubImage3D;
    PFNGLCOPYTEXTURESUBIMAGE1DPROC                       gl3wCopyTextureSubImage1D;
    PFNGLCOPYTEXTURESUBIMAGE2DPROC                       gl3wCopyTextureSubImage2D;
    PFNGLCOPYTEXTURESUBIMAGE3DPROC                       gl3wCopyTextureSubImage3D;
    PFNGLCREATEBUFFERSPROC                               gl3wCreateBuffers;
    PFNGLCREATEFRAMEBUFFERSPROC                          gl3wCreateFramebuffers;
    PFNGLCREATEPROGRAMPROC                               gl3wCreateProgram;
    PFNGLCREATEPROGRAMPIPELINESPROC                      gl3wCreateProgramPipelines;
    PFNGLCREATEQUERIESPROC                               gl3wCreateQueries;
    PFNGLCREATERENDERBUFFERSPROC                         gl3wCreateRenderbuffers;
    PFNGLCREATESAMPLERSPROC                              gl3wCreateSamplers;
    PFNGLCREATESHADERPROC                                gl3wCreateShader;
    PFNGLCREATESHADERPROGRAMVPROC                        gl3wCreateShaderProgramv;
    PFNGLCREATESYNCFROMCLEVENTARBPROC                    gl3wCreateSyncFromCLeventARB;
    PFNGLCREATETEXTURESPROC                              gl3wCreateTextures;
    PFNGLCREATETRANSFORMFEEDBACKSPROC                    gl3wCreateTransformFeedbacks;
    PFNGLCREATEVERTEXARRAYSPROC                          gl3wCreateVertexArrays;
    PFNGLCULLFACEPROC                                    gl3wCullFace;
    PFNGLDEBUGMESSAGECALLBACKPROC                        gl3wDebugMessageCallback;
    PFNGLDEBUGMESSAGECALLBACKARBPROC                     gl3wDebugMessageCallbackARB;
    PFNGLDEBUGMESSAGECONTROLPROC                         gl3wDebugMessageControl;
    PFNGLDEBUGMESSAGECONTROLARBPROC                      gl3wDebugMessageControlARB;
    PFNGLDEBUGMESSAGEINSERTPROC                          gl3wDebugMessageInsert;
    PFNGLDEBUGMESSAGEINSERTARBPROC                       gl3wDebugMessageInsertARB;
    PFNGLDELETEBUFFERSPROC                               gl3wDeleteBuffers;
    PFNGLDELETEFRAMEBUFFERSPROC                          gl3wDeleteFramebuffers;
    PFNGLDELETENAMEDSTRINGARBPROC                        gl3wDeleteNamedStringARB;
    PFNGLDELETEPROGRAMPROC                               gl3wDeleteProgram;
    PFNGLDELETEPROGRAMPIPELINESPROC                      gl3wDeleteProgramPipelines;
    PFNGLDELETEQUERIESPROC                               gl3wDeleteQueries;
    PFNGLDELETERENDERBUFFERSPROC                         gl3wDeleteRenderbuffers;
    PFNGLDELETESAMPLERSPROC                              gl3wDeleteSamplers;
    PFNGLDELETESHADERPROC                                gl3wDeleteShader;
    PFNGLDELETESYNCPROC                                  gl3wDeleteSync;
    PFNGLDELETETEXTURESPROC                              gl3wDeleteTextures;
    PFNGLDELETETRANSFORMFEEDBACKSPROC                    gl3wDeleteTransformFeedbacks;
    PFNGLDELETEVERTEXARRAYSPROC                          gl3wDeleteVertexArrays;
    PFNGLDEPTHFUNCPROC                                   gl3wDepthFunc;
    PFNGLDEPTHMASKPROC                                   gl3wDepthMask;
    PFNGLDEPTHRANGEPROC                                  gl3wDepthRange;
    PFNGLDEPTHRANGEARRAYVPROC                            gl3wDepthRangeArrayv;
    PFNGLDEPTHRANGEINDEXEDPROC                           gl3wDepthRangeIndexed;
    PFNGLDEPTHRANGEFPROC                                 gl3wDepthRangef;
    PFNGLDETACHSHADERPROC                                gl3wDetachShader;
    PFNGLDISABLEPROC                                     gl3wDisable;
    PFNGLDISABLEVERTEXARRAYATTRIBPROC                    gl3wDisableVertexArrayAttrib;
    PFNGLDISABLEVERTEXATTRIBARRAYPROC                    gl3wDisableVertexAttribArray;
    PFNGLDISABLEIPROC                                    gl3wDisablei;
    PFNGLDISPATCHCOMPUTEPROC                             gl3wDispatchCompute;
    PFNGLDISPATCHCOMPUTEGROUPSIZEARBPROC                 gl3wDispatchComputeGroupSizeARB;
    PFNGLDISPATCHCOMPUTEINDIRECTPROC                     gl3wDispatchComputeIndirect;
    PFNGLDRAWARRAYSPROC                                  gl3wDrawArrays;
    PFNGLDRAWARRAYSINDIRECTPROC                          gl3wDrawArraysIndirect;
    PFNGLDRAWARRAYSINSTANCEDPROC                         gl3wDrawArraysInstanced;
    PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC             gl3wDrawArraysInstancedBaseInstance;
    PFNGLDRAWBUFFERPROC                                  gl3wDrawBuffer;
    PFNGLDRAWBUFFERSPROC                                 gl3wDrawBuffers;
    PFNGLDRAWELEMENTSPROC                                gl3wDrawElements;
    PFNGLDRAWELEMENTSBASEVERTEXPROC                      gl3wDrawElementsBaseVertex;
    PFNGLDRAWELEMENTSINDIRECTPROC                        gl3wDrawElementsIndirect;
    PFNGLDRAWELEMENTSINSTANCEDPROC                       gl3wDrawElementsInstanced;
    PFNGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC           gl3wDrawElementsInstancedBaseInstance;
    PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC             gl3wDrawElementsInstancedBaseVertex;
    PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC gl3wDrawElementsInstancedBaseVertexBaseInstance;
    PFNGLDRAWRANGEELEMENTSPROC                           gl3wDrawRangeElements;
    PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC                 gl3wDrawRangeElementsBaseVertex;
    PFNGLDRAWTRANSFORMFEEDBACKPROC                       gl3wDrawTransformFeedback;
    PFNGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC              gl3wDrawTransformFeedbackInstanced;
    PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC                 gl3wDrawTransformFeedbackStream;
    PFNGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC        gl3wDrawTransformFeedbackStreamInstanced;
    PFNGLENABLEPROC                                      gl3wEnable;
    PFNGLENABLEVERTEXARRAYATTRIBPROC                     gl3wEnableVertexArrayAttrib;
    PFNGLENABLEVERTEXATTRIBARRAYPROC                     gl3wEnableVertexAttribArray;
    PFNGLENABLEIPROC                                     gl3wEnablei;
    PFNGLENDCONDITIONALRENDERPROC                        gl3wEndConditionalRender;
    PFNGLENDQUERYPROC                                    gl3wEndQuery;
    PFNGLENDQUERYINDEXEDPROC                             gl3wEndQueryIndexed;
    PFNGLENDTRANSFORMFEEDBACKPROC                        gl3wEndTransformFeedback;
    PFNGLFENCESYNCPROC                                   gl3wFenceSync;
    PFNGLFINISHPROC                                      gl3wFinish;
    PFNGLFLUSHPROC                                       gl3wFlush;
    PFNGLFLUSHMAPPEDBUFFERRANGEPROC                      gl3wFlushMappedBufferRange;
    PFNGLFLUSHMAPPEDNAMEDBUFFERRANGEPROC                 gl3wFlushMappedNamedBufferRange;
    PFNGLFRAMEBUFFERPARAMETERIPROC                       gl3wFramebufferParameteri;
    PFNGLFRAMEBUFFERRENDERBUFFERPROC                     gl3wFramebufferRenderbuffer;
    PFNGLFRAMEBUFFERTEXTUREPROC                          gl3wFramebufferTexture;
    PFNGLFRAMEBUFFERTEXTURE1DPROC                        gl3wFramebufferTexture1D;
    PFNGLFRAMEBUFFERTEXTURE2DPROC                        gl3wFramebufferTexture2D;
    PFNGLFRAMEBUFFERTEXTURE3DPROC                        gl3wFramebufferTexture3D;
    PFNGLFRAMEBUFFERTEXTURELAYERPROC                     gl3wFramebufferTextureLayer;
    PFNGLFRONTFACEPROC                                   gl3wFrontFace;
    PFNGLGENBUFFERSPROC                                  gl3wGenBuffers;
    PFNGLGENFRAMEBUFFERSPROC                             gl3wGenFramebuffers;
    PFNGLGENPROGRAMPIPELINESPROC                         gl3wGenProgramPipelines;
    PFNGLGENQUERIESPROC                                  gl3wGenQueries;
    PFNGLGENRENDERBUFFERSPROC                            gl3wGenRenderbuffers;
    PFNGLGENSAMPLERSPROC                                 gl3wGenSamplers;
    PFNGLGENTEXTURESPROC                                 gl3wGenTextures;
    PFNGLGENTRANSFORMFEEDBACKSPROC                       gl3wGenTransformFeedbacks;
    PFNGLGENVERTEXARRAYSPROC                             gl3wGenVertexArrays;
    PFNGLGENERATEMIPMAPPROC                              gl3wGenerateMipmap;
    PFNGLGENERATETEXTUREMIPMAPPROC                       gl3wGenerateTextureMipmap;
    PFNGLGETACTIVEATOMICCOUNTERBUFFERIVPROC              gl3wGetActiveAtomicCounterBufferiv;
    PFNGLGETACTIVEATTRIBPROC                             gl3wGetActiveAttrib;
    PFNGLGETACTIVESUBROUTINENAMEPROC                     gl3wGetActiveSubroutineName;
    PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC              gl3wGetActiveSubroutineUniformName;
    PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC                gl3wGetActiveSubroutineUniformiv;
    PFNGLGETACTIVEUNIFORMPROC                            gl3wGetActiveUniform;
    PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC                   gl3wGetActiveUniformBlockName;
    PFNGLGETACTIVEUNIFORMBLOCKIVPROC                     gl3wGetActiveUniformBlockiv;
    PFNGLGETACTIVEUNIFORMNAMEPROC                        gl3wGetActiveUniformName;
    PFNGLGETACTIVEUNIFORMSIVPROC                         gl3wGetActiveUniformsiv;
    PFNGLGETATTACHEDSHADERSPROC                          gl3wGetAttachedShaders;
    PFNGLGETATTRIBLOCATIONPROC                           gl3wGetAttribLocation;
    PFNGLGETBOOLEANI_VPROC                               gl3wGetBooleani_v;
    PFNGLGETBOOLEANVPROC                                 gl3wGetBooleanv;
    PFNGLGETBUFFERPARAMETERI64VPROC                      gl3wGetBufferParameteri64v;
    PFNGLGETBUFFERPARAMETERIVPROC                        gl3wGetBufferParameteriv;
    PFNGLGETBUFFERPOINTERVPROC                           gl3wGetBufferPointerv;
    PFNGLGETBUFFERSUBDATAPROC                            gl3wGetBufferSubData;
    PFNGLGETCOMPRESSEDTEXIMAGEPROC                       gl3wGetCompressedTexImage;
    PFNGLGETCOMPRESSEDTEXTUREIMAGEPROC                   gl3wGetCompressedTextureImage;
    PFNGLGETCOMPRESSEDTEXTURESUBIMAGEPROC                gl3wGetCompressedTextureSubImage;
    PFNGLGETDEBUGMESSAGELOGPROC                          gl3wGetDebugMessageLog;
    PFNGLGETDEBUGMESSAGELOGARBPROC                       gl3wGetDebugMessageLogARB;
    PFNGLGETDOUBLEI_VPROC                                gl3wGetDoublei_v;
    PFNGLGETDOUBLEVPROC                                  gl3wGetDoublev;
    PFNGLGETERRORPROC                                    gl3wGetError;
    PFNGLGETFLOATI_VPROC                                 gl3wGetFloati_v;
    PFNGLGETFLOATVPROC                                   gl3wGetFloatv;
    PFNGLGETFRAGDATAINDEXPROC                            gl3wGetFragDataIndex;
    PFNGLGETFRAGDATALOCATIONPROC                         gl3wGetFragDataLocation;
    PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC         gl3wGetFramebufferAttachmentParameteriv;
    PFNGLGETFRAMEBUFFERPARAMETERIVPROC                   gl3wGetFramebufferParameteriv;
    PFNGLGETGRAPHICSRESETSTATUSPROC                      gl3wGetGraphicsResetStatus;
    PFNGLGETGRAPHICSRESETSTATUSARBPROC                   gl3wGetGraphicsResetStatusARB;
    PFNGLGETIMAGEHANDLEARBPROC                           gl3wGetImageHandleARB;
    PFNGLGETINTEGER64I_VPROC                             gl3wGetInteger64i_v;
    PFNGLGETINTEGER64VPROC                               gl3wGetInteger64v;
    PFNGLGETINTEGERI_VPROC                               gl3wGetIntegeri_v;
    PFNGLGETINTEGERVPROC                                 gl3wGetIntegerv;
    PFNGLGETINTERNALFORMATI64VPROC                       gl3wGetInternalformati64v;
    PFNGLGETINTERNALFORMATIVPROC                         gl3wGetInternalformativ;
    PFNGLGETMULTISAMPLEFVPROC                            gl3wGetMultisamplefv;
    PFNGLGETNAMEDBUFFERPARAMETERI64VPROC                 gl3wGetNamedBufferParameteri64v;
    PFNGLGETNAMEDBUFFERPARAMETERIVPROC                   gl3wGetNamedBufferParameteriv;
    PFNGLGETNAMEDBUFFERPOINTERVPROC                      gl3wGetNamedBufferPointerv;
    PFNGLGETNAMEDBUFFERSUBDATAPROC                       gl3wGetNamedBufferSubData;
    PFNGLGETNAMEDFRAMEBUFFERATTACHMENTPARAMETERIVPROC    gl3wGetNamedFramebufferAttachmentParameteriv;
    PFNGLGETNAMEDFRAMEBUFFERPARAMETERIVPROC              gl3wGetNamedFramebufferParameteriv;
    PFNGLGETNAMEDRENDERBUFFERPARAMETERIVPROC             gl3wGetNamedRenderbufferParameteriv;
    PFNGLGETNAMEDSTRINGARBPROC                           gl3wGetNamedStringARB;
    PFNGLGETNAMEDSTRINGIVARBPROC                         gl3wGetNamedStringivARB;
    PFNGLGETOBJECTLABELPROC                              gl3wGetObjectLabel;
    PFNGLGETOBJECTPTRLABELPROC                           gl3wGetObjectPtrLabel;
    PFNGLGETPOINTERVPROC                                 gl3wGetPointerv;
    PFNGLGETPROGRAMBINARYPROC                            gl3wGetProgramBinary;
    PFNGLGETPROGRAMINFOLOGPROC                           gl3wGetProgramInfoLog;
    PFNGLGETPROGRAMINTERFACEIVPROC                       gl3wGetProgramInterfaceiv;
    PFNGLGETPROGRAMPIPELINEINFOLOGPROC                   gl3wGetProgramPipelineInfoLog;
    PFNGLGETPROGRAMPIPELINEIVPROC                        gl3wGetProgramPipelineiv;
    PFNGLGETPROGRAMRESOURCEINDEXPROC                     gl3wGetProgramResourceIndex;
    PFNGLGETPROGRAMRESOURCELOCATIONPROC                  gl3wGetProgramResourceLocation;
    PFNGLGETPROGRAMRESOURCELOCATIONINDEXPROC             gl3wGetProgramResourceLocationIndex;
    PFNGLGETPROGRAMRESOURCENAMEPROC                      gl3wGetProgramResourceName;
    PFNGLGETPROGRAMRESOURCEIVPROC                        gl3wGetProgramResourceiv;
    PFNGLGETPROGRAMSTAGEIVPROC                           gl3wGetProgramStageiv;
    PFNGLGETPROGRAMIVPROC                                gl3wGetProgramiv;
    PFNGLGETQUERYBUFFEROBJECTI64VPROC                    gl3wGetQueryBufferObjecti64v;
    PFNGLGETQUERYBUFFEROBJECTIVPROC                      gl3wGetQueryBufferObjectiv;
    PFNGLGETQUERYBUFFEROBJECTUI64VPROC                   gl3wGetQueryBufferObjectui64v;
    PFNGLGETQUERYBUFFEROBJECTUIVPROC                     gl3wGetQueryBufferObjectuiv;
    PFNGLGETQUERYINDEXEDIVPROC                           gl3wGetQueryIndexediv;
    PFNGLGETQUERYOBJECTI64VPROC                          gl3wGetQueryObjecti64v;
    PFNGLGETQUERYOBJECTIVPROC                            gl3wGetQueryObjectiv;
    PFNGLGETQUERYOBJECTUI64VPROC                         gl3wGetQueryObjectui64v;
    PFNGLGETQUERYOBJECTUIVPROC                           gl3wGetQueryObjectuiv;
    PFNGLGETQUERYIVPROC                                  gl3wGetQueryiv;
    PFNGLGETRENDERBUFFERPARAMETERIVPROC                  gl3wGetRenderbufferParameteriv;
    PFNGLGETSAMPLERPARAMETERIIVPROC                      gl3wGetSamplerParameterIiv;
    PFNGLGETSAMPLERPARAMETERIUIVPROC                     gl3wGetSamplerParameterIuiv;
    PFNGLGETSAMPLERPARAMETERFVPROC                       gl3wGetSamplerParameterfv;
    PFNGLGETSAMPLERPARAMETERIVPROC                       gl3wGetSamplerParameteriv;
    PFNGLGETSHADERINFOLOGPROC                            gl3wGetShaderInfoLog;
    PFNGLGETSHADERPRECISIONFORMATPROC                    gl3wGetShaderPrecisionFormat;
    PFNGLGETSHADERSOURCEPROC                             gl3wGetShaderSource;
    PFNGLGETSHADERIVPROC                                 gl3wGetShaderiv;
    PFNGLGETSTRINGPROC                                   gl3wGetString;
    PFNGLGETSTRINGIPROC                                  gl3wGetStringi;
    PFNGLGETSUBROUTINEINDEXPROC                          gl3wGetSubroutineIndex;
    PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC                gl3wGetSubroutineUniformLocation;
    PFNGLGETSYNCIVPROC                                   gl3wGetSynciv;
    PFNGLGETTEXIMAGEPROC                                 gl3wGetTexImage;
    PFNGLGETTEXLEVELPARAMETERFVPROC                      gl3wGetTexLevelParameterfv;
    PFNGLGETTEXLEVELPARAMETERIVPROC                      gl3wGetTexLevelParameteriv;
    PFNGLGETTEXPARAMETERIIVPROC                          gl3wGetTexParameterIiv;
    PFNGLGETTEXPARAMETERIUIVPROC                         gl3wGetTexParameterIuiv;
    PFNGLGETTEXPARAMETERFVPROC                           gl3wGetTexParameterfv;
    PFNGLGETTEXPARAMETERIVPROC                           gl3wGetTexParameteriv;
    PFNGLGETTEXTUREHANDLEARBPROC                         gl3wGetTextureHandleARB;
    PFNGLGETTEXTUREIMAGEPROC                             gl3wGetTextureImage;
    PFNGLGETTEXTURELEVELPARAMETERFVPROC                  gl3wGetTextureLevelParameterfv;
    PFNGLGETTEXTURELEVELPARAMETERIVPROC                  gl3wGetTextureLevelParameteriv;
    PFNGLGETTEXTUREPARAMETERIIVPROC                      gl3wGetTextureParameterIiv;
    PFNGLGETTEXTUREPARAMETERIUIVPROC                     gl3wGetTextureParameterIuiv;
    PFNGLGETTEXTUREPARAMETERFVPROC                       gl3wGetTextureParameterfv;
    PFNGLGETTEXTUREPARAMETERIVPROC                       gl3wGetTextureParameteriv;
    PFNGLGETTEXTURESAMPLERHANDLEARBPROC                  gl3wGetTextureSamplerHandleARB;
    PFNGLGETTEXTURESUBIMAGEPROC                          gl3wGetTextureSubImage;
    PFNGLGETTRANSFORMFEEDBACKVARYINGPROC                 gl3wGetTransformFeedbackVarying;
    PFNGLGETTRANSFORMFEEDBACKI64_VPROC                   gl3wGetTransformFeedbacki64_v;
    PFNGLGETTRANSFORMFEEDBACKI_VPROC                     gl3wGetTransformFeedbacki_v;
    PFNGLGETTRANSFORMFEEDBACKIVPROC                      gl3wGetTransformFeedbackiv;
    PFNGLGETUNIFORMBLOCKINDEXPROC                        gl3wGetUniformBlockIndex;
    PFNGLGETUNIFORMINDICESPROC                           gl3wGetUniformIndices;
    PFNGLGETUNIFORMLOCATIONPROC                          gl3wGetUniformLocation;
    PFNGLGETUNIFORMSUBROUTINEUIVPROC                     gl3wGetUniformSubroutineuiv;
    PFNGLGETUNIFORMDVPROC                                gl3wGetUniformdv;
    PFNGLGETUNIFORMFVPROC                                gl3wGetUniformfv;
    PFNGLGETUNIFORMIVPROC                                gl3wGetUniformiv;
    PFNGLGETUNIFORMUIVPROC                               gl3wGetUniformuiv;
    PFNGLGETVERTEXARRAYINDEXED64IVPROC                   gl3wGetVertexArrayIndexed64iv;
    PFNGLGETVERTEXARRAYINDEXEDIVPROC                     gl3wGetVertexArrayIndexediv;
    PFNGLGETVERTEXARRAYIVPROC                            gl3wGetVertexArrayiv;
    PFNGLGETVERTEXATTRIBIIVPROC                          gl3wGetVertexAttribIiv;
    PFNGLGETVERTEXATTRIBIUIVPROC                         gl3wGetVertexAttribIuiv;
    PFNGLGETVERTEXATTRIBLDVPROC                          gl3wGetVertexAttribLdv;
    PFNGLGETVERTEXATTRIBLUI64VARBPROC                    gl3wGetVertexAttribLui64vARB;
    PFNGLGETVERTEXATTRIBPOINTERVPROC                     gl3wGetVertexAttribPointerv;
    PFNGLGETVERTEXATTRIBDVPROC                           gl3wGetVertexAttribdv;
    PFNGLGETVERTEXATTRIBFVPROC                           gl3wGetVertexAttribfv;
    PFNGLGETVERTEXATTRIBIVPROC                           gl3wGetVertexAttribiv;
    PFNGLGETNCOMPRESSEDTEXIMAGEPROC                      gl3wGetnCompressedTexImage;
    PFNGLGETNCOMPRESSEDTEXIMAGEARBPROC                   gl3wGetnCompressedTexImageARB;
    PFNGLGETNTEXIMAGEPROC                                gl3wGetnTexImage;
    PFNGLGETNTEXIMAGEARBPROC                             gl3wGetnTexImageARB;
    PFNGLGETNUNIFORMDVPROC                               gl3wGetnUniformdv;
    PFNGLGETNUNIFORMDVARBPROC                            gl3wGetnUniformdvARB;
    PFNGLGETNUNIFORMFVPROC                               gl3wGetnUniformfv;
    PFNGLGETNUNIFORMFVARBPROC                            gl3wGetnUniformfvARB;
    PFNGLGETNUNIFORMIVPROC                               gl3wGetnUniformiv;
    PFNGLGETNUNIFORMIVARBPROC                            gl3wGetnUniformivARB;
    PFNGLGETNUNIFORMUIVPROC                              gl3wGetnUniformuiv;
    PFNGLGETNUNIFORMUIVARBPROC                           gl3wGetnUniformuivARB;
    PFNGLHINTPROC                                        gl3wHint;
    PFNGLINVALIDATEBUFFERDATAPROC                        gl3wInvalidateBufferData;
    PFNGLINVALIDATEBUFFERSUBDATAPROC                     gl3wInvalidateBufferSubData;
    PFNGLINVALIDATEFRAMEBUFFERPROC                       gl3wInvalidateFramebuffer;
    PFNGLINVALIDATENAMEDFRAMEBUFFERDATAPROC              gl3wInvalidateNamedFramebufferData;
    PFNGLINVALIDATENAMEDFRAMEBUFFERSUBDATAPROC           gl3wInvalidateNamedFramebufferSubData;
    PFNGLINVALIDATESUBFRAMEBUFFERPROC                    gl3wInvalidateSubFramebuffer;
    PFNGLINVALIDATETEXIMAGEPROC                          gl3wInvalidateTexImage;
    PFNGLINVALIDATETEXSUBIMAGEPROC                       gl3wInvalidateTexSubImage;
    PFNGLISBUFFERPROC                                    gl3wIsBuffer;
    PFNGLISENABLEDPROC                                   gl3wIsEnabled;
    PFNGLISENABLEDIPROC                                  gl3wIsEnabledi;
    PFNGLISFRAMEBUFFERPROC                               gl3wIsFramebuffer;
    PFNGLISIMAGEHANDLERESIDENTARBPROC                    gl3wIsImageHandleResidentARB;
    PFNGLISNAMEDSTRINGARBPROC                            gl3wIsNamedStringARB;
    PFNGLISPROGRAMPROC                                   gl3wIsProgram;
    PFNGLISPROGRAMPIPELINEPROC                           gl3wIsProgramPipeline;
    PFNGLISQUERYPROC                                     gl3wIsQuery;
    PFNGLISRENDERBUFFERPROC                              gl3wIsRenderbuffer;
    PFNGLISSAMPLERPROC                                   gl3wIsSampler;
    PFNGLISSHADERPROC                                    gl3wIsShader;
    PFNGLISSYNCPROC                                      gl3wIsSync;
    PFNGLISTEXTUREPROC                                   gl3wIsTexture;
    PFNGLISTEXTUREHANDLERESIDENTARBPROC                  gl3wIsTextureHandleResidentARB;
    PFNGLISTRANSFORMFEEDBACKPROC                         gl3wIsTransformFeedback;
    PFNGLISVERTEXARRAYPROC                               gl3wIsVertexArray;
    PFNGLLINEWIDTHPROC                                   gl3wLineWidth;
    PFNGLLINKPROGRAMPROC                                 gl3wLinkProgram;
    PFNGLLOGICOPPROC                                     gl3wLogicOp;
    PFNGLMAKEIMAGEHANDLENONRESIDENTARBPROC               gl3wMakeImageHandleNonResidentARB;
    PFNGLMAKEIMAGEHANDLERESIDENTARBPROC                  gl3wMakeImageHandleResidentARB;
    PFNGLMAKETEXTUREHANDLENONRESIDENTARBPROC             gl3wMakeTextureHandleNonResidentARB;
    PFNGLMAKETEXTUREHANDLERESIDENTARBPROC                gl3wMakeTextureHandleResidentARB;
    PFNGLMAPBUFFERPROC                                   gl3wMapBuffer;
    PFNGLMAPBUFFERRANGEPROC                              gl3wMapBufferRange;
    PFNGLMAPNAMEDBUFFERPROC                              gl3wMapNamedBuffer;
    PFNGLMAPNAMEDBUFFERRANGEPROC                         gl3wMapNamedBufferRange;
    PFNGLMEMORYBARRIERPROC                               gl3wMemoryBarrier;
    PFNGLMEMORYBARRIERBYREGIONPROC                       gl3wMemoryBarrierByRegion;
    PFNGLMINSAMPLESHADINGPROC                            gl3wMinSampleShading;
    PFNGLMINSAMPLESHADINGARBPROC                         gl3wMinSampleShadingARB;
    PFNGLMULTIDRAWARRAYSPROC                             gl3wMultiDrawArrays;
    PFNGLMULTIDRAWARRAYSINDIRECTPROC                     gl3wMultiDrawArraysIndirect;
    PFNGLMULTIDRAWARRAYSINDIRECTCOUNTARBPROC             gl3wMultiDrawArraysIndirectCountARB;
    PFNGLMULTIDRAWELEMENTSPROC                           gl3wMultiDrawElements;
    PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC                 gl3wMultiDrawElementsBaseVertex;
    PFNGLMULTIDRAWELEMENTSINDIRECTPROC                   gl3wMultiDrawElementsIndirect;
    PFNGLMULTIDRAWELEMENTSINDIRECTCOUNTARBPROC           gl3wMultiDrawElementsIndirectCountARB;
    PFNGLNAMEDBUFFERDATAPROC                             gl3wNamedBufferData;
    PFNGLNAMEDBUFFERPAGECOMMITMENTARBPROC                gl3wNamedBufferPageCommitmentARB;
    PFNGLNAMEDBUFFERPAGECOMMITMENTEXTPROC                gl3wNamedBufferPageCommitmentEXT;
    PFNGLNAMEDBUFFERSTORAGEPROC                          gl3wNamedBufferStorage;
    PFNGLNAMEDBUFFERSUBDATAPROC                          gl3wNamedBufferSubData;
    PFNGLNAMEDFRAMEBUFFERDRAWBUFFERPROC                  gl3wNamedFramebufferDrawBuffer;
    PFNGLNAMEDFRAMEBUFFERDRAWBUFFERSPROC                 gl3wNamedFramebufferDrawBuffers;
    PFNGLNAMEDFRAMEBUFFERPARAMETERIPROC                  gl3wNamedFramebufferParameteri;
    PFNGLNAMEDFRAMEBUFFERREADBUFFERPROC                  gl3wNamedFramebufferReadBuffer;
    PFNGLNAMEDFRAMEBUFFERRENDERBUFFERPROC                gl3wNamedFramebufferRenderbuffer;
    PFNGLNAMEDFRAMEBUFFERTEXTUREPROC                     gl3wNamedFramebufferTexture;
    PFNGLNAMEDFRAMEBUFFERTEXTURELAYERPROC                gl3wNamedFramebufferTextureLayer;
    PFNGLNAMEDRENDERBUFFERSTORAGEPROC                    gl3wNamedRenderbufferStorage;
    PFNGLNAMEDRENDERBUFFERSTORAGEMULTISAMPLEPROC         gl3wNamedRenderbufferStorageMultisample;
    PFNGLNAMEDSTRINGARBPROC                              gl3wNamedStringARB;
    PFNGLOBJECTLABELPROC                                 gl3wObjectLabel;
    PFNGLOBJECTPTRLABELPROC                              gl3wObjectPtrLabel;
    PFNGLPATCHPARAMETERFVPROC                            gl3wPatchParameterfv;
    PFNGLPATCHPARAMETERIPROC                             gl3wPatchParameteri;
    PFNGLPAUSETRANSFORMFEEDBACKPROC                      gl3wPauseTransformFeedback;
    PFNGLPIXELSTOREFPROC                                 gl3wPixelStoref;
    PFNGLPIXELSTOREIPROC                                 gl3wPixelStorei;
    PFNGLPOINTPARAMETERFPROC                             gl3wPointParameterf;
    PFNGLPOINTPARAMETERFVPROC                            gl3wPointParameterfv;
    PFNGLPOINTPARAMETERIPROC                             gl3wPointParameteri;
    PFNGLPOINTPARAMETERIVPROC                            gl3wPointParameteriv;
    PFNGLPOINTSIZEPROC                                   gl3wPointSize;
    PFNGLPOLYGONMODEPROC                                 gl3wPolygonMode;
    PFNGLPOLYGONOFFSETPROC                               gl3wPolygonOffset;
    PFNGLPOPDEBUGGROUPPROC                               gl3wPopDebugGroup;
    PFNGLPRIMITIVERESTARTINDEXPROC                       gl3wPrimitiveRestartIndex;
    PFNGLPROGRAMBINARYPROC                               gl3wProgramBinary;
    PFNGLPROGRAMPARAMETERIPROC                           gl3wProgramParameteri;
    PFNGLPROGRAMUNIFORM1DPROC                            gl3wProgramUniform1d;
    PFNGLPROGRAMUNIFORM1DVPROC                           gl3wProgramUniform1dv;
    PFNGLPROGRAMUNIFORM1FPROC                            gl3wProgramUniform1f;
    PFNGLPROGRAMUNIFORM1FVPROC                           gl3wProgramUniform1fv;
    PFNGLPROGRAMUNIFORM1IPROC                            gl3wProgramUniform1i;
    PFNGLPROGRAMUNIFORM1IVPROC                           gl3wProgramUniform1iv;
    PFNGLPROGRAMUNIFORM1UIPROC                           gl3wProgramUniform1ui;
    PFNGLPROGRAMUNIFORM1UIVPROC                          gl3wProgramUniform1uiv;
    PFNGLPROGRAMUNIFORM2DPROC                            gl3wProgramUniform2d;
    PFNGLPROGRAMUNIFORM2DVPROC                           gl3wProgramUniform2dv;
    PFNGLPROGRAMUNIFORM2FPROC                            gl3wProgramUniform2f;
    PFNGLPROGRAMUNIFORM2FVPROC                           gl3wProgramUniform2fv;
    PFNGLPROGRAMUNIFORM2IPROC                            gl3wProgramUniform2i;
    PFNGLPROGRAMUNIFORM2IVPROC                           gl3wProgramUniform2iv;
    PFNGLPROGRAMUNIFORM2UIPROC                           gl3wProgramUniform2ui;
    PFNGLPROGRAMUNIFORM2UIVPROC                          gl3wProgramUniform2uiv;
    PFNGLPROGRAMUNIFORM3DPROC                            gl3wProgramUniform3d;
    PFNGLPROGRAMUNIFORM3DVPROC                           gl3wProgramUniform3dv;
    PFNGLPROGRAMUNIFORM3FPROC                            gl3wProgramUniform3f;
    PFNGLPROGRAMUNIFORM3FVPROC                           gl3wProgramUniform3fv;
    PFNGLPROGRAMUNIFORM3IPROC                            gl3wProgramUniform3i;
    PFNGLPROGRAMUNIFORM3IVPROC                           gl3wProgramUniform3iv;
    PFNGLPROGRAMUNIFORM3UIPROC                           gl3wProgramUniform3ui;
    PFNGLPROGRAMUNIFORM3UIVPROC                          gl3wProgramUniform3uiv;
    PFNGLPROGRAMUNIFORM4DPROC                            gl3wProgramUniform4d;
    PFNGLPROGRAMUNIFORM4DVPROC                           gl3wProgramUniform4dv;
    PFNGLPROGRAMUNIFORM4FPROC                            gl3wProgramUniform4f;
    PFNGLPROGRAMUNIFORM4FVPROC                           gl3wProgramUniform4fv;
    PFNGLPROGRAMUNIFORM4IPROC                            gl3wProgramUniform4i;
    PFNGLPROGRAMUNIFORM4IVPROC                           gl3wProgramUniform4iv;
    PFNGLPROGRAMUNIFORM4UIPROC                           gl3wProgramUniform4ui;
    PFNGLPROGRAMUNIFORM4UIVPROC                          gl3wProgramUniform4uiv;
    PFNGLPROGRAMUNIFORMHANDLEUI64ARBPROC                 gl3wProgramUniformHandleui64ARB;
    PFNGLPROGRAMUNIFORMHANDLEUI64VARBPROC                gl3wProgramUniformHandleui64vARB;
    PFNGLPROGRAMUNIFORMMATRIX2DVPROC                     gl3wProgramUniformMatrix2dv;
    PFNGLPROGRAMUNIFORMMATRIX2FVPROC                     gl3wProgramUniformMatrix2fv;
    PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC                   gl3wProgramUniformMatrix2x3dv;
    PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC                   gl3wProgramUniformMatrix2x3fv;
    PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC                   gl3wProgramUniformMatrix2x4dv;
    PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC                   gl3wProgramUniformMatrix2x4fv;
    PFNGLPROGRAMUNIFORMMATRIX3DVPROC                     gl3wProgramUniformMatrix3dv;
    PFNGLPROGRAMUNIFORMMATRIX3FVPROC                     gl3wProgramUniformMatrix3fv;
    PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC                   gl3wProgramUniformMatrix3x2dv;
    PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC                   gl3wProgramUniformMatrix3x2fv;
    PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC                   gl3wProgramUniformMatrix3x4dv;
    PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC                   gl3wProgramUniformMatrix3x4fv;
    PFNGLPROGRAMUNIFORMMATRIX4DVPROC                     gl3wProgramUniformMatrix4dv;
    PFNGLPROGRAMUNIFORMMATRIX4FVPROC                     gl3wProgramUniformMatrix4fv;
    PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC                   gl3wProgramUniformMatrix4x2dv;
    PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC                   gl3wProgramUniformMatrix4x2fv;
    PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC                   gl3wProgramUniformMatrix4x3dv;
    PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC                   gl3wProgramUniformMatrix4x3fv;
    PFNGLPROVOKINGVERTEXPROC                             gl3wProvokingVertex;
    PFNGLPUSHDEBUGGROUPPROC                              gl3wPushDebugGroup;
    PFNGLQUERYCOUNTERPROC                                gl3wQueryCounter;
    PFNGLREADBUFFERPROC                                  gl3wReadBuffer;
    PFNGLREADPIXELSPROC                                  gl3wReadPixels;
    PFNGLREADNPIXELSPROC                                 gl3wReadnPixels;
    PFNGLREADNPIXELSARBPROC                              gl3wReadnPixelsARB;
    PFNGLRELEASESHADERCOMPILERPROC                       gl3wReleaseShaderCompiler;
    PFNGLRENDERBUFFERSTORAGEPROC                         gl3wRenderbufferStorage;
    PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC              gl3wRenderbufferStorageMultisample;
    PFNGLRESUMETRANSFORMFEEDBACKPROC                     gl3wResumeTransformFeedback;
    PFNGLSAMPLECOVERAGEPROC                              gl3wSampleCoverage;
    PFNGLSAMPLEMASKIPROC                                 gl3wSampleMaski;
    PFNGLSAMPLERPARAMETERIIVPROC                         gl3wSamplerParameterIiv;
    PFNGLSAMPLERPARAMETERIUIVPROC                        gl3wSamplerParameterIuiv;
    PFNGLSAMPLERPARAMETERFPROC                           gl3wSamplerParameterf;
    PFNGLSAMPLERPARAMETERFVPROC                          gl3wSamplerParameterfv;
    PFNGLSAMPLERPARAMETERIPROC                           gl3wSamplerParameteri;
    PFNGLSAMPLERPARAMETERIVPROC                          gl3wSamplerParameteriv;
    PFNGLSCISSORPROC                                     gl3wScissor;
    PFNGLSCISSORARRAYVPROC                               gl3wScissorArrayv;
    PFNGLSCISSORINDEXEDPROC                              gl3wScissorIndexed;
    PFNGLSCISSORINDEXEDVPROC                             gl3wScissorIndexedv;
    PFNGLSHADERBINARYPROC                                gl3wShaderBinary;
    PFNGLSHADERSOURCEPROC                                gl3wShaderSource;
    PFNGLSHADERSTORAGEBLOCKBINDINGPROC                   gl3wShaderStorageBlockBinding;
    PFNGLSTENCILFUNCPROC                                 gl3wStencilFunc;
    PFNGLSTENCILFUNCSEPARATEPROC                         gl3wStencilFuncSeparate;
    PFNGLSTENCILMASKPROC                                 gl3wStencilMask;
    PFNGLSTENCILMASKSEPARATEPROC                         gl3wStencilMaskSeparate;
    PFNGLSTENCILOPPROC                                   gl3wStencilOp;
    PFNGLSTENCILOPSEPARATEPROC                           gl3wStencilOpSeparate;
    PFNGLTEXBUFFERPROC                                   gl3wTexBuffer;
    PFNGLTEXBUFFERRANGEPROC                              gl3wTexBufferRange;
    PFNGLTEXIMAGE1DPROC                                  gl3wTexImage1D;
    PFNGLTEXIMAGE2DPROC                                  gl3wTexImage2D;
    PFNGLTEXIMAGE2DMULTISAMPLEPROC                       gl3wTexImage2DMultisample;
    PFNGLTEXIMAGE3DPROC                                  gl3wTexImage3D;
    PFNGLTEXIMAGE3DMULTISAMPLEPROC                       gl3wTexImage3DMultisample;
    PFNGLTEXPAGECOMMITMENTARBPROC                        gl3wTexPageCommitmentARB;
    PFNGLTEXPARAMETERIIVPROC                             gl3wTexParameterIiv;
    PFNGLTEXPARAMETERIUIVPROC                            gl3wTexParameterIuiv;
    PFNGLTEXPARAMETERFPROC                               gl3wTexParameterf;
    PFNGLTEXPARAMETERFVPROC                              gl3wTexParameterfv;
    PFNGLTEXPARAMETERIPROC                               gl3wTexParameteri;
    PFNGLTEXPARAMETERIVPROC                              gl3wTexParameteriv;
    PFNGLTEXSTORAGE1DPROC                                gl3wTexStorage1D;
    PFNGLTEXSTORAGE2DPROC                                gl3wTexStorage2D;
    PFNGLTEXSTORAGE2DMULTISAMPLEPROC                     gl3wTexStorage2DMultisample;
    PFNGLTEXSTORAGE3DPROC                                gl3wTexStorage3D;
    PFNGLTEXSTORAGE3DMULTISAMPLEPROC                     gl3wTexStorage3DMultisample;
    PFNGLTEXSUBIMAGE1DPROC                               gl3wTexSubImage1D;
    PFNGLTEXSUBIMAGE2DPROC                               gl3wTexSubImage2D;
    PFNGLTEXSUBIMAGE3DPROC                               gl3wTexSubImage3D;
    PFNGLTEXTUREBARRIERPROC                              gl3wTextureBarrier;
    PFNGLTEXTUREBUFFERPROC                               gl3wTextureBuffer;
    PFNGLTEXTUREBUFFERRANGEPROC                          gl3wTextureBufferRange;
    PFNGLTEXTUREPARAMETERIIVPROC                         gl3wTextureParameterIiv;
    PFNGLTEXTUREPARAMETERIUIVPROC                        gl3wTextureParameterIuiv;
    PFNGLTEXTUREPARAMETERFPROC                           gl3wTextureParameterf;
    PFNGLTEXTUREPARAMETERFVPROC                          gl3wTextureParameterfv;
    PFNGLTEXTUREPARAMETERIPROC                           gl3wTextureParameteri;
    PFNGLTEXTUREPARAMETERIVPROC                          gl3wTextureParameteriv;
    PFNGLTEXTURESTORAGE1DPROC                            gl3wTextureStorage1D;
    PFNGLTEXTURESTORAGE2DPROC                            gl3wTextureStorage2D;
    PFNGLTEXTURESTORAGE2DMULTISAMPLEPROC                 gl3wTextureStorage2DMultisample;
    PFNGLTEXTURESTORAGE3DPROC                            gl3wTextureStorage3D;
    PFNGLTEXTURESTORAGE3DMULTISAMPLEPROC                 gl3wTextureStorage3DMultisample;
    PFNGLTEXTURESUBIMAGE1DPROC                           gl3wTextureSubImage1D;
    PFNGLTEXTURESUBIMAGE2DPROC                           gl3wTextureSubImage2D;
    PFNGLTEXTURESUBIMAGE3DPROC                           gl3wTextureSubImage3D;
    PFNGLTEXTUREVIEWPROC                                 gl3wTextureView;
    PFNGLTRANSFORMFEEDBACKBUFFERBASEPROC                 gl3wTransformFeedbackBufferBase;
    PFNGLTRANSFORMFEEDBACKBUFFERRANGEPROC                gl3wTransformFeedbackBufferRange;
    PFNGLTRANSFORMFEEDBACKVARYINGSPROC                   gl3wTransformFeedbackVaryings;
    PFNGLUNIFORM1DPROC                                   gl3wUniform1d;
    PFNGLUNIFORM1DVPROC                                  gl3wUniform1dv;
    PFNGLUNIFORM1FPROC                                   gl3wUniform1f;
    PFNGLUNIFORM1FVPROC                                  gl3wUniform1fv;
    PFNGLUNIFORM1IPROC                                   gl3wUniform1i;
    PFNGLUNIFORM1IVPROC                                  gl3wUniform1iv;
    PFNGLUNIFORM1UIPROC                                  gl3wUniform1ui;
    PFNGLUNIFORM1UIVPROC                                 gl3wUniform1uiv;
    PFNGLUNIFORM2DPROC                                   gl3wUniform2d;
    PFNGLUNIFORM2DVPROC                                  gl3wUniform2dv;
    PFNGLUNIFORM2FPROC                                   gl3wUniform2f;
    PFNGLUNIFORM2FVPROC                                  gl3wUniform2fv;
    PFNGLUNIFORM2IPROC                                   gl3wUniform2i;
    PFNGLUNIFORM2IVPROC                                  gl3wUniform2iv;
    PFNGLUNIFORM2UIPROC                                  gl3wUniform2ui;
    PFNGLUNIFORM2UIVPROC                                 gl3wUniform2uiv;
    PFNGLUNIFORM3DPROC                                   gl3wUniform3d;
    PFNGLUNIFORM3DVPROC                                  gl3wUniform3dv;
    PFNGLUNIFORM3FPROC                                   gl3wUniform3f;
    PFNGLUNIFORM3FVPROC                                  gl3wUniform3fv;
    PFNGLUNIFORM3IPROC                                   gl3wUniform3i;
    PFNGLUNIFORM3IVPROC                                  gl3wUniform3iv;
    PFNGLUNIFORM3UIPROC                                  gl3wUniform3ui;
    PFNGLUNIFORM3UIVPROC                                 gl3wUniform3uiv;
    PFNGLUNIFORM4DPROC                                   gl3wUniform4d;
    PFNGLUNIFORM4DVPROC                                  gl3wUniform4dv;
    PFNGLUNIFORM4FPROC                                   gl3wUniform4f;
    PFNGLUNIFORM4FVPROC                                  gl3wUniform4fv;
    PFNGLUNIFORM4IPROC                                   gl3wUniform4i;
    PFNGLUNIFORM4IVPROC                                  gl3wUniform4iv;
    PFNGLUNIFORM4UIPROC                                  gl3wUniform4ui;
    PFNGLUNIFORM4UIVPROC                                 gl3wUniform4uiv;
    PFNGLUNIFORMBLOCKBINDINGPROC                         gl3wUniformBlockBinding;
    PFNGLUNIFORMHANDLEUI64ARBPROC                        gl3wUniformHandleui64ARB;
    PFNGLUNIFORMHANDLEUI64VARBPROC                       gl3wUniformHandleui64vARB;
    PFNGLUNIFORMMATRIX2DVPROC                            gl3wUniformMatrix2dv;
    PFNGLUNIFORMMATRIX2FVPROC                            gl3wUniformMatrix2fv;
    PFNGLUNIFORMMATRIX2X3DVPROC                          gl3wUniformMatrix2x3dv;
    PFNGLUNIFORMMATRIX2X3FVPROC                          gl3wUniformMatrix2x3fv;
    PFNGLUNIFORMMATRIX2X4DVPROC                          gl3wUniformMatrix2x4dv;
    PFNGLUNIFORMMATRIX2X4FVPROC                          gl3wUniformMatrix2x4fv;
    PFNGLUNIFORMMATRIX3DVPROC                            gl3wUniformMatrix3dv;
    PFNGLUNIFORMMATRIX3FVPROC                            gl3wUniformMatrix3fv;
    PFNGLUNIFORMMATRIX3X2DVPROC                          gl3wUniformMatrix3x2dv;
    PFNGLUNIFORMMATRIX3X2FVPROC                          gl3wUniformMatrix3x2fv;
    PFNGLUNIFORMMATRIX3X4DVPROC                          gl3wUniformMatrix3x4dv;
    PFNGLUNIFORMMATRIX3X4FVPROC                          gl3wUniformMatrix3x4fv;
    PFNGLUNIFORMMATRIX4DVPROC                            gl3wUniformMatrix4dv;
    PFNGLUNIFORMMATRIX4FVPROC                            gl3wUniformMatrix4fv;
    PFNGLUNIFORMMATRIX4X2DVPROC                          gl3wUniformMatrix4x2dv;
    PFNGLUNIFORMMATRIX4X2FVPROC                          gl3wUniformMatrix4x2fv;
    PFNGLUNIFORMMATRIX4X3DVPROC                          gl3wUniformMatrix4x3dv;
    PFNGLUNIFORMMATRIX4X3FVPROC                          gl3wUniformMatrix4x3fv;
    PFNGLUNIFORMSUBROUTINESUIVPROC                       gl3wUniformSubroutinesuiv;
    PFNGLUNMAPBUFFERPROC                                 gl3wUnmapBuffer;
    PFNGLUNMAPNAMEDBUFFERPROC                            gl3wUnmapNamedBuffer;
    PFNGLUSEPROGRAMPROC                                  gl3wUseProgram;
    PFNGLUSEPROGRAMSTAGESPROC                            gl3wUseProgramStages;
    PFNGLVALIDATEPROGRAMPROC                             gl3wValidateProgram;
    PFNGLVALIDATEPROGRAMPIPELINEPROC                     gl3wValidateProgramPipeline;
    PFNGLVERTEXARRAYATTRIBBINDINGPROC                    gl3wVertexArrayAttribBinding;
    PFNGLVERTEXARRAYATTRIBFORMATPROC                     gl3wVertexArrayAttribFormat;
    PFNGLVERTEXARRAYATTRIBIFORMATPROC                    gl3wVertexArrayAttribIFormat;
    PFNGLVERTEXARRAYATTRIBLFORMATPROC                    gl3wVertexArrayAttribLFormat;
    PFNGLVERTEXARRAYBINDINGDIVISORPROC                   gl3wVertexArrayBindingDivisor;
    PFNGLVERTEXARRAYELEMENTBUFFERPROC                    gl3wVertexArrayElementBuffer;
    PFNGLVERTEXARRAYVERTEXBUFFERPROC                     gl3wVertexArrayVertexBuffer;
    PFNGLVERTEXARRAYVERTEXBUFFERSPROC                    gl3wVertexArrayVertexBuffers;
    PFNGLVERTEXATTRIB1DPROC                              gl3wVertexAttrib1d;
    PFNGLVERTEXATTRIB1DVPROC                             gl3wVertexAttrib1dv;
    PFNGLVERTEXATTRIB1FPROC                              gl3wVertexAttrib1f;
    PFNGLVERTEXATTRIB1FVPROC                             gl3wVertexAttrib1fv;
    PFNGLVERTEXATTRIB1SPROC                              gl3wVertexAttrib1s;
    PFNGLVERTEXATTRIB1SVPROC                             gl3wVertexAttrib1sv;
    PFNGLVERTEXATTRIB2DPROC                              gl3wVertexAttrib2d;
    PFNGLVERTEXATTRIB2DVPROC                             gl3wVertexAttrib2dv;
    PFNGLVERTEXATTRIB2FPROC                              gl3wVertexAttrib2f;
    PFNGLVERTEXATTRIB2FVPROC                             gl3wVertexAttrib2fv;
    PFNGLVERTEXATTRIB2SPROC                              gl3wVertexAttrib2s;
    PFNGLVERTEXATTRIB2SVPROC                             gl3wVertexAttrib2sv;
    PFNGLVERTEXATTRIB3DPROC                              gl3wVertexAttrib3d;
    PFNGLVERTEXATTRIB3DVPROC                             gl3wVertexAttrib3dv;
    PFNGLVERTEXATTRIB3FPROC                              gl3wVertexAttrib3f;
    PFNGLVERTEXATTRIB3FVPROC                             gl3wVertexAttrib3fv;
    PFNGLVERTEXATTRIB3SPROC                              gl3wVertexAttrib3s;
    PFNGLVERTEXATTRIB3SVPROC                             gl3wVertexAttrib3sv;
    PFNGLVERTEXATTRIB4NBVPROC                            gl3wVertexAttrib4Nbv;
    PFNGLVERTEXATTRIB4NIVPROC                            gl3wVertexAttrib4Niv;
    PFNGLVERTEXATTRIB4NSVPROC                            gl3wVertexAttrib4Nsv;
    PFNGLVERTEXATTRIB4NUBPROC                            gl3wVertexAttrib4Nub;
    PFNGLVERTEXATTRIB4NUBVPROC                           gl3wVertexAttrib4Nubv;
    PFNGLVERTEXATTRIB4NUIVPROC                           gl3wVertexAttrib4Nuiv;
    PFNGLVERTEXATTRIB4NUSVPROC                           gl3wVertexAttrib4Nusv;
    PFNGLVERTEXATTRIB4BVPROC                             gl3wVertexAttrib4bv;
    PFNGLVERTEXATTRIB4DPROC                              gl3wVertexAttrib4d;
    PFNGLVERTEXATTRIB4DVPROC                             gl3wVertexAttrib4dv;
    PFNGLVERTEXATTRIB4FPROC                              gl3wVertexAttrib4f;
    PFNGLVERTEXATTRIB4FVPROC                             gl3wVertexAttrib4fv;
    PFNGLVERTEXATTRIB4IVPROC                             gl3wVertexAttrib4iv;
    PFNGLVERTEXATTRIB4SPROC                              gl3wVertexAttrib4s;
    PFNGLVERTEXATTRIB4SVPROC                             gl3wVertexAttrib4sv;
    PFNGLVERTEXATTRIB4UBVPROC                            gl3wVertexAttrib4ubv;
    PFNGLVERTEXATTRIB4UIVPROC                            gl3wVertexAttrib4uiv;
    PFNGLVERTEXATTRIB4USVPROC                            gl3wVertexAttrib4usv;
    PFNGLVERTEXATTRIBBINDINGPROC                         gl3wVertexAttribBinding;
    PFNGLVERTEXATTRIBDIVISORPROC                         gl3wVertexAttribDivisor;
    PFNGLVERTEXATTRIBFORMATPROC                          gl3wVertexAttribFormat;
    PFNGLVERTEXATTRIBI1IPROC                             gl3wVertexAttribI1i;
    PFNGLVERTEXATTRIBI1IVPROC                            gl3wVertexAttribI1iv;
    PFNGLVERTEXATTRIBI1UIPROC                            gl3wVertexAttribI1ui;
    PFNGLVERTEXATTRIBI1UIVPROC                           gl3wVertexAttribI1uiv;
    PFNGLVERTEXATTRIBI2IPROC                             gl3wVertexAttribI2i;
    PFNGLVERTEXATTRIBI2IVPROC                            gl3wVertexAttribI2iv;
    PFNGLVERTEXATTRIBI2UIPROC                            gl3wVertexAttribI2ui;
    PFNGLVERTEXATTRIBI2UIVPROC                           gl3wVertexAttribI2uiv;
    PFNGLVERTEXATTRIBI3IPROC                             gl3wVertexAttribI3i;
    PFNGLVERTEXATTRIBI3IVPROC                            gl3wVertexAttribI3iv;
    PFNGLVERTEXATTRIBI3UIPROC                            gl3wVertexAttribI3ui;
    PFNGLVERTEXATTRIBI3UIVPROC                           gl3wVertexAttribI3uiv;
    PFNGLVERTEXATTRIBI4BVPROC                            gl3wVertexAttribI4bv;
    PFNGLVERTEXATTRIBI4IPROC                             gl3wVertexAttribI4i;
    PFNGLVERTEXATTRIBI4IVPROC                            gl3wVertexAttribI4iv;
    PFNGLVERTEXATTRIBI4SVPROC                            gl3wVertexAttribI4sv;
    PFNGLVERTEXATTRIBI4UBVPROC                           gl3wVertexAttribI4ubv;
    PFNGLVERTEXATTRIBI4UIPROC                            gl3wVertexAttribI4ui;
    PFNGLVERTEXATTRIBI4UIVPROC                           gl3wVertexAttribI4uiv;
    PFNGLVERTEXATTRIBI4USVPROC                           gl3wVertexAttribI4usv;
    PFNGLVERTEXATTRIBIFORMATPROC                         gl3wVertexAttribIFormat;
    PFNGLVERTEXATTRIBIPOINTERPROC                        gl3wVertexAttribIPointer;
    PFNGLVERTEXATTRIBL1DPROC                             gl3wVertexAttribL1d;
    PFNGLVERTEXATTRIBL1DVPROC                            gl3wVertexAttribL1dv;
    PFNGLVERTEXATTRIBL1UI64ARBPROC                       gl3wVertexAttribL1ui64ARB;
    PFNGLVERTEXATTRIBL1UI64VARBPROC                      gl3wVertexAttribL1ui64vARB;
    PFNGLVERTEXATTRIBL2DPROC                             gl3wVertexAttribL2d;
    PFNGLVERTEXATTRIBL2DVPROC                            gl3wVertexAttribL2dv;
    PFNGLVERTEXATTRIBL3DPROC                             gl3wVertexAttribL3d;
    PFNGLVERTEXATTRIBL3DVPROC                            gl3wVertexAttribL3dv;
    PFNGLVERTEXATTRIBL4DPROC                             gl3wVertexAttribL4d;
    PFNGLVERTEXATTRIBL4DVPROC                            gl3wVertexAttribL4dv;
    PFNGLVERTEXATTRIBLFORMATPROC                         gl3wVertexAttribLFormat;
    PFNGLVERTEXATTRIBLPOINTERPROC                        gl3wVertexAttribLPointer;
    PFNGLVERTEXATTRIBP1UIPROC                            gl3wVertexAttribP1ui;
    PFNGLVERTEXATTRIBP1UIVPROC                           gl3wVertexAttribP1uiv;
    PFNGLVERTEXATTRIBP2UIPROC                            gl3wVertexAttribP2ui;
    PFNGLVERTEXATTRIBP2UIVPROC                           gl3wVertexAttribP2uiv;
    PFNGLVERTEXATTRIBP3UIPROC                            gl3wVertexAttribP3ui;
    PFNGLVERTEXATTRIBP3UIVPROC                           gl3wVertexAttribP3uiv;
    PFNGLVERTEXATTRIBP4UIPROC                            gl3wVertexAttribP4ui;
    PFNGLVERTEXATTRIBP4UIVPROC                           gl3wVertexAttribP4uiv;
    PFNGLVERTEXATTRIBPOINTERPROC                         gl3wVertexAttribPointer;
    PFNGLVERTEXBINDINGDIVISORPROC                        gl3wVertexBindingDivisor;
    PFNGLVIEWPORTPROC                                    gl3wViewport;
    PFNGLVIEWPORTARRAYVPROC                              gl3wViewportArrayv;
    PFNGLVIEWPORTINDEXEDFPROC                            gl3wViewportIndexedf;
    PFNGLVIEWPORTINDEXEDFVPROC                           gl3wViewportIndexedfv;
    PFNGLWAITSYNCPROC                                    gl3wWaitSync;

} GL3WContext;

#define glActiveShaderProgram                         ((GL3W_CONTEXT_METHOD)->gl3wActiveShaderProgram)
#define glActiveTexture                               ((GL3W_CONTEXT_METHOD)->gl3wActiveTexture)
#define glAttachShader                                ((GL3W_CONTEXT_METHOD)->gl3wAttachShader)
#define glBeginConditionalRender                      ((GL3W_CONTEXT_METHOD)->gl3wBeginConditionalRender)
#define glBeginQuery                                  ((GL3W_CONTEXT_METHOD)->gl3wBeginQuery)
#define glBeginQueryIndexed                           ((GL3W_CONTEXT_METHOD)->gl3wBeginQueryIndexed)
#define glBeginTransformFeedback                      ((GL3W_CONTEXT_METHOD)->gl3wBeginTransformFeedback)
#define glBindAttribLocation                          ((GL3W_CONTEXT_METHOD)->gl3wBindAttribLocation)
#define glBindBuffer                                  ((GL3W_CONTEXT_METHOD)->gl3wBindBuffer)
#define glBindBufferBase                              ((GL3W_CONTEXT_METHOD)->gl3wBindBufferBase)
#define glBindBufferRange                             ((GL3W_CONTEXT_METHOD)->gl3wBindBufferRange)
#define glBindBuffersBase                             ((GL3W_CONTEXT_METHOD)->gl3wBindBuffersBase)
#define glBindBuffersRange                            ((GL3W_CONTEXT_METHOD)->gl3wBindBuffersRange)
#define glBindFragDataLocation                        ((GL3W_CONTEXT_METHOD)->gl3wBindFragDataLocation)
#define glBindFragDataLocationIndexed                 ((GL3W_CONTEXT_METHOD)->gl3wBindFragDataLocationIndexed)
#define glBindFramebuffer                             ((GL3W_CONTEXT_METHOD)->gl3wBindFramebuffer)
#define glBindImageTexture                            ((GL3W_CONTEXT_METHOD)->gl3wBindImageTexture)
#define glBindImageTextures                           ((GL3W_CONTEXT_METHOD)->gl3wBindImageTextures)
#define glBindProgramPipeline                         ((GL3W_CONTEXT_METHOD)->gl3wBindProgramPipeline)
#define glBindRenderbuffer                            ((GL3W_CONTEXT_METHOD)->gl3wBindRenderbuffer)
#define glBindSampler                                 ((GL3W_CONTEXT_METHOD)->gl3wBindSampler)
#define glBindSamplers                                ((GL3W_CONTEXT_METHOD)->gl3wBindSamplers)
#define glBindTexture                                 ((GL3W_CONTEXT_METHOD)->gl3wBindTexture)
#define glBindTextureUnit                             ((GL3W_CONTEXT_METHOD)->gl3wBindTextureUnit)
#define glBindTextures                                ((GL3W_CONTEXT_METHOD)->gl3wBindTextures)
#define glBindTransformFeedback                       ((GL3W_CONTEXT_METHOD)->gl3wBindTransformFeedback)
#define glBindVertexArray                             ((GL3W_CONTEXT_METHOD)->gl3wBindVertexArray)
#define glBindVertexBuffer                            ((GL3W_CONTEXT_METHOD)->gl3wBindVertexBuffer)
#define glBindVertexBuffers                           ((GL3W_CONTEXT_METHOD)->gl3wBindVertexBuffers)
#define glBlendColor                                  ((GL3W_CONTEXT_METHOD)->gl3wBlendColor)
#define glBlendEquation                               ((GL3W_CONTEXT_METHOD)->gl3wBlendEquation)
#define glBlendEquationSeparate                       ((GL3W_CONTEXT_METHOD)->gl3wBlendEquationSeparate)
#define glBlendEquationSeparatei                      ((GL3W_CONTEXT_METHOD)->gl3wBlendEquationSeparatei)
#define glBlendEquationSeparateiARB                   ((GL3W_CONTEXT_METHOD)->gl3wBlendEquationSeparateiARB)
#define glBlendEquationi                              ((GL3W_CONTEXT_METHOD)->gl3wBlendEquationi)
#define glBlendEquationiARB                           ((GL3W_CONTEXT_METHOD)->gl3wBlendEquationiARB)
#define glBlendFunc                                   ((GL3W_CONTEXT_METHOD)->gl3wBlendFunc)
#define glBlendFuncSeparate                           ((GL3W_CONTEXT_METHOD)->gl3wBlendFuncSeparate)
#define glBlendFuncSeparatei                          ((GL3W_CONTEXT_METHOD)->gl3wBlendFuncSeparatei)
#define glBlendFuncSeparateiARB                       ((GL3W_CONTEXT_METHOD)->gl3wBlendFuncSeparateiARB)
#define glBlendFunci                                  ((GL3W_CONTEXT_METHOD)->gl3wBlendFunci)
#define glBlendFunciARB                               ((GL3W_CONTEXT_METHOD)->gl3wBlendFunciARB)
#define glBlitFramebuffer                             ((GL3W_CONTEXT_METHOD)->gl3wBlitFramebuffer)
#define glBlitNamedFramebuffer                        ((GL3W_CONTEXT_METHOD)->gl3wBlitNamedFramebuffer)
#define glBufferData                                  ((GL3W_CONTEXT_METHOD)->gl3wBufferData)
#define glBufferPageCommitmentARB                     ((GL3W_CONTEXT_METHOD)->gl3wBufferPageCommitmentARB)
#define glBufferStorage                               ((GL3W_CONTEXT_METHOD)->gl3wBufferStorage)
#define glBufferSubData                               ((GL3W_CONTEXT_METHOD)->gl3wBufferSubData)
#define glCheckFramebufferStatus                      ((GL3W_CONTEXT_METHOD)->gl3wCheckFramebufferStatus)
#define glCheckNamedFramebufferStatus                 ((GL3W_CONTEXT_METHOD)->gl3wCheckNamedFramebufferStatus)
#define glClampColor                                  ((GL3W_CONTEXT_METHOD)->gl3wClampColor)
#define glClear                                       ((GL3W_CONTEXT_METHOD)->gl3wClear)
#define glClearBufferData                             ((GL3W_CONTEXT_METHOD)->gl3wClearBufferData)
#define glClearBufferSubData                          ((GL3W_CONTEXT_METHOD)->gl3wClearBufferSubData)
#define glClearBufferfi                               ((GL3W_CONTEXT_METHOD)->gl3wClearBufferfi)
#define glClearBufferfv                               ((GL3W_CONTEXT_METHOD)->gl3wClearBufferfv)
#define glClearBufferiv                               ((GL3W_CONTEXT_METHOD)->gl3wClearBufferiv)
#define glClearBufferuiv                              ((GL3W_CONTEXT_METHOD)->gl3wClearBufferuiv)
#define glClearColor                                  ((GL3W_CONTEXT_METHOD)->gl3wClearColor)
#define glClearDepth                                  ((GL3W_CONTEXT_METHOD)->gl3wClearDepth)
#define glClearDepthf                                 ((GL3W_CONTEXT_METHOD)->gl3wClearDepthf)
#define glClearNamedBufferData                        ((GL3W_CONTEXT_METHOD)->gl3wClearNamedBufferData)
#define glClearNamedBufferSubData                     ((GL3W_CONTEXT_METHOD)->gl3wClearNamedBufferSubData)
#define glClearNamedFramebufferfi                     ((GL3W_CONTEXT_METHOD)->gl3wClearNamedFramebufferfi)
#define glClearNamedFramebufferfv                     ((GL3W_CONTEXT_METHOD)->gl3wClearNamedFramebufferfv)
#define glClearNamedFramebufferiv                     ((GL3W_CONTEXT_METHOD)->gl3wClearNamedFramebufferiv)
#define glClearNamedFramebufferuiv                    ((GL3W_CONTEXT_METHOD)->gl3wClearNamedFramebufferuiv)
#define glClearStencil                                ((GL3W_CONTEXT_METHOD)->gl3wClearStencil)
#define glClearTexImage                               ((GL3W_CONTEXT_METHOD)->gl3wClearTexImage)
#define glClearTexSubImage                            ((GL3W_CONTEXT_METHOD)->gl3wClearTexSubImage)
#define glClientWaitSync                              ((GL3W_CONTEXT_METHOD)->gl3wClientWaitSync)
#define glClipControl                                 ((GL3W_CONTEXT_METHOD)->gl3wClipControl)
#define glColorMask                                   ((GL3W_CONTEXT_METHOD)->gl3wColorMask)
#define glColorMaski                                  ((GL3W_CONTEXT_METHOD)->gl3wColorMaski)
#define glCompileShader                               ((GL3W_CONTEXT_METHOD)->gl3wCompileShader)
#define glCompileShaderIncludeARB                     ((GL3W_CONTEXT_METHOD)->gl3wCompileShaderIncludeARB)
#define glCompressedTexImage1D                        ((GL3W_CONTEXT_METHOD)->gl3wCompressedTexImage1D)
#define glCompressedTexImage2D                        ((GL3W_CONTEXT_METHOD)->gl3wCompressedTexImage2D)
#define glCompressedTexImage3D                        ((GL3W_CONTEXT_METHOD)->gl3wCompressedTexImage3D)
#define glCompressedTexSubImage1D                     ((GL3W_CONTEXT_METHOD)->gl3wCompressedTexSubImage1D)
#define glCompressedTexSubImage2D                     ((GL3W_CONTEXT_METHOD)->gl3wCompressedTexSubImage2D)
#define glCompressedTexSubImage3D                     ((GL3W_CONTEXT_METHOD)->gl3wCompressedTexSubImage3D)
#define glCompressedTextureSubImage1D                 ((GL3W_CONTEXT_METHOD)->gl3wCompressedTextureSubImage1D)
#define glCompressedTextureSubImage2D                 ((GL3W_CONTEXT_METHOD)->gl3wCompressedTextureSubImage2D)
#define glCompressedTextureSubImage3D                 ((GL3W_CONTEXT_METHOD)->gl3wCompressedTextureSubImage3D)
#define glCopyBufferSubData                           ((GL3W_CONTEXT_METHOD)->gl3wCopyBufferSubData)
#define glCopyImageSubData                            ((GL3W_CONTEXT_METHOD)->gl3wCopyImageSubData)
#define glCopyNamedBufferSubData                      ((GL3W_CONTEXT_METHOD)->gl3wCopyNamedBufferSubData)
#define glCopyTexImage1D                              ((GL3W_CONTEXT_METHOD)->gl3wCopyTexImage1D)
#define glCopyTexImage2D                              ((GL3W_CONTEXT_METHOD)->gl3wCopyTexImage2D)
#define glCopyTexSubImage1D                           ((GL3W_CONTEXT_METHOD)->gl3wCopyTexSubImage1D)
#define glCopyTexSubImage2D                           ((GL3W_CONTEXT_METHOD)->gl3wCopyTexSubImage2D)
#define glCopyTexSubImage3D                           ((GL3W_CONTEXT_METHOD)->gl3wCopyTexSubImage3D)
#define glCopyTextureSubImage1D                       ((GL3W_CONTEXT_METHOD)->gl3wCopyTextureSubImage1D)
#define glCopyTextureSubImage2D                       ((GL3W_CONTEXT_METHOD)->gl3wCopyTextureSubImage2D)
#define glCopyTextureSubImage3D                       ((GL3W_CONTEXT_METHOD)->gl3wCopyTextureSubImage3D)
#define glCreateBuffers                               ((GL3W_CONTEXT_METHOD)->gl3wCreateBuffers)
#define glCreateFramebuffers                          ((GL3W_CONTEXT_METHOD)->gl3wCreateFramebuffers)
#define glCreateProgram                               ((GL3W_CONTEXT_METHOD)->gl3wCreateProgram)
#define glCreateProgramPipelines                      ((GL3W_CONTEXT_METHOD)->gl3wCreateProgramPipelines)
#define glCreateQueries                               ((GL3W_CONTEXT_METHOD)->gl3wCreateQueries)
#define glCreateRenderbuffers                         ((GL3W_CONTEXT_METHOD)->gl3wCreateRenderbuffers)
#define glCreateSamplers                              ((GL3W_CONTEXT_METHOD)->gl3wCreateSamplers)
#define glCreateShader                                ((GL3W_CONTEXT_METHOD)->gl3wCreateShader)
#define glCreateShaderProgramv                        ((GL3W_CONTEXT_METHOD)->gl3wCreateShaderProgramv)
#define glCreateSyncFromCLeventARB                    ((GL3W_CONTEXT_METHOD)->gl3wCreateSyncFromCLeventARB)
#define glCreateTextures                              ((GL3W_CONTEXT_METHOD)->gl3wCreateTextures)
#define glCreateTransformFeedbacks                    ((GL3W_CONTEXT_METHOD)->gl3wCreateTransformFeedbacks)
#define glCreateVertexArrays                          ((GL3W_CONTEXT_METHOD)->gl3wCreateVertexArrays)
#define glCullFace                                    ((GL3W_CONTEXT_METHOD)->gl3wCullFace)
#define glDebugMessageCallback                        ((GL3W_CONTEXT_METHOD)->gl3wDebugMessageCallback)
#define glDebugMessageCallbackARB                     ((GL3W_CONTEXT_METHOD)->gl3wDebugMessageCallbackARB)
#define glDebugMessageControl                         ((GL3W_CONTEXT_METHOD)->gl3wDebugMessageControl)
#define glDebugMessageControlARB                      ((GL3W_CONTEXT_METHOD)->gl3wDebugMessageControlARB)
#define glDebugMessageInsert                          ((GL3W_CONTEXT_METHOD)->gl3wDebugMessageInsert)
#define glDebugMessageInsertARB                       ((GL3W_CONTEXT_METHOD)->gl3wDebugMessageInsertARB)
#define glDeleteBuffers                               ((GL3W_CONTEXT_METHOD)->gl3wDeleteBuffers)
#define glDeleteFramebuffers                          ((GL3W_CONTEXT_METHOD)->gl3wDeleteFramebuffers)
#define glDeleteNamedStringARB                        ((GL3W_CONTEXT_METHOD)->gl3wDeleteNamedStringARB)
#define glDeleteProgram                               ((GL3W_CONTEXT_METHOD)->gl3wDeleteProgram)
#define glDeleteProgramPipelines                      ((GL3W_CONTEXT_METHOD)->gl3wDeleteProgramPipelines)
#define glDeleteQueries                               ((GL3W_CONTEXT_METHOD)->gl3wDeleteQueries)
#define glDeleteRenderbuffers                         ((GL3W_CONTEXT_METHOD)->gl3wDeleteRenderbuffers)
#define glDeleteSamplers                              ((GL3W_CONTEXT_METHOD)->gl3wDeleteSamplers)
#define glDeleteShader                                ((GL3W_CONTEXT_METHOD)->gl3wDeleteShader)
#define glDeleteSync                                  ((GL3W_CONTEXT_METHOD)->gl3wDeleteSync)
#define glDeleteTextures                              ((GL3W_CONTEXT_METHOD)->gl3wDeleteTextures)
#define glDeleteTransformFeedbacks                    ((GL3W_CONTEXT_METHOD)->gl3wDeleteTransformFeedbacks)
#define glDeleteVertexArrays                          ((GL3W_CONTEXT_METHOD)->gl3wDeleteVertexArrays)
#define glDepthFunc                                   ((GL3W_CONTEXT_METHOD)->gl3wDepthFunc)
#define glDepthMask                                   ((GL3W_CONTEXT_METHOD)->gl3wDepthMask)
#define glDepthRange                                  ((GL3W_CONTEXT_METHOD)->gl3wDepthRange)
#define glDepthRangeArrayv                            ((GL3W_CONTEXT_METHOD)->gl3wDepthRangeArrayv)
#define glDepthRangeIndexed                           ((GL3W_CONTEXT_METHOD)->gl3wDepthRangeIndexed)
#define glDepthRangef                                 ((GL3W_CONTEXT_METHOD)->gl3wDepthRangef)
#define glDetachShader                                ((GL3W_CONTEXT_METHOD)->gl3wDetachShader)
#define glDisable                                     ((GL3W_CONTEXT_METHOD)->gl3wDisable)
#define glDisableVertexArrayAttrib                    ((GL3W_CONTEXT_METHOD)->gl3wDisableVertexArrayAttrib)
#define glDisableVertexAttribArray                    ((GL3W_CONTEXT_METHOD)->gl3wDisableVertexAttribArray)
#define glDisablei                                    ((GL3W_CONTEXT_METHOD)->gl3wDisablei)
#define glDispatchCompute                             ((GL3W_CONTEXT_METHOD)->gl3wDispatchCompute)
#define glDispatchComputeGroupSizeARB                 ((GL3W_CONTEXT_METHOD)->gl3wDispatchComputeGroupSizeARB)
#define glDispatchComputeIndirect                     ((GL3W_CONTEXT_METHOD)->gl3wDispatchComputeIndirect)
#define glDrawArrays                                  ((GL3W_CONTEXT_METHOD)->gl3wDrawArrays)
#define glDrawArraysIndirect                          ((GL3W_CONTEXT_METHOD)->gl3wDrawArraysIndirect)
#define glDrawArraysInstanced                         ((GL3W_CONTEXT_METHOD)->gl3wDrawArraysInstanced)
#define glDrawArraysInstancedBaseInstance             ((GL3W_CONTEXT_METHOD)->gl3wDrawArraysInstancedBaseInstance)
#define glDrawBuffer                                  ((GL3W_CONTEXT_METHOD)->gl3wDrawBuffer)
#define glDrawBuffers                                 ((GL3W_CONTEXT_METHOD)->gl3wDrawBuffers)
#define glDrawElements                                ((GL3W_CONTEXT_METHOD)->gl3wDrawElements)
#define glDrawElementsBaseVertex                      ((GL3W_CONTEXT_METHOD)->gl3wDrawElementsBaseVertex)
#define glDrawElementsIndirect                        ((GL3W_CONTEXT_METHOD)->gl3wDrawElementsIndirect)
#define glDrawElementsInstanced                       ((GL3W_CONTEXT_METHOD)->gl3wDrawElementsInstanced)
#define glDrawElementsInstancedBaseInstance           ((GL3W_CONTEXT_METHOD)->gl3wDrawElementsInstancedBaseInstance)
#define glDrawElementsInstancedBaseVertex             ((GL3W_CONTEXT_METHOD)->gl3wDrawElementsInstancedBaseVertex)
#define glDrawElementsInstancedBaseVertexBaseInstance ((GL3W_CONTEXT_METHOD)->gl3wDrawElementsInstancedBaseVertexBaseInstance)
#define glDrawRangeElements                           ((GL3W_CONTEXT_METHOD)->gl3wDrawRangeElements)
#define glDrawRangeElementsBaseVertex                 ((GL3W_CONTEXT_METHOD)->gl3wDrawRangeElementsBaseVertex)
#define glDrawTransformFeedback                       ((GL3W_CONTEXT_METHOD)->gl3wDrawTransformFeedback)
#define glDrawTransformFeedbackInstanced              ((GL3W_CONTEXT_METHOD)->gl3wDrawTransformFeedbackInstanced)
#define glDrawTransformFeedbackStream                 ((GL3W_CONTEXT_METHOD)->gl3wDrawTransformFeedbackStream)
#define glDrawTransformFeedbackStreamInstanced        ((GL3W_CONTEXT_METHOD)->gl3wDrawTransformFeedbackStreamInstanced)
#define glEnable                                      ((GL3W_CONTEXT_METHOD)->gl3wEnable)
#define glEnableVertexArrayAttrib                     ((GL3W_CONTEXT_METHOD)->gl3wEnableVertexArrayAttrib)
#define glEnableVertexAttribArray                     ((GL3W_CONTEXT_METHOD)->gl3wEnableVertexAttribArray)
#define glEnablei                                     ((GL3W_CONTEXT_METHOD)->gl3wEnablei)
#define glEndConditionalRender                        ((GL3W_CONTEXT_METHOD)->gl3wEndConditionalRender)
#define glEndQuery                                    ((GL3W_CONTEXT_METHOD)->gl3wEndQuery)
#define glEndQueryIndexed                             ((GL3W_CONTEXT_METHOD)->gl3wEndQueryIndexed)
#define glEndTransformFeedback                        ((GL3W_CONTEXT_METHOD)->gl3wEndTransformFeedback)
#define glFenceSync                                   ((GL3W_CONTEXT_METHOD)->gl3wFenceSync)
#define glFinish                                      ((GL3W_CONTEXT_METHOD)->gl3wFinish)
#define glFlush                                       ((GL3W_CONTEXT_METHOD)->gl3wFlush)
#define glFlushMappedBufferRange                      ((GL3W_CONTEXT_METHOD)->gl3wFlushMappedBufferRange)
#define glFlushMappedNamedBufferRange                 ((GL3W_CONTEXT_METHOD)->gl3wFlushMappedNamedBufferRange)
#define glFramebufferParameteri                       ((GL3W_CONTEXT_METHOD)->gl3wFramebufferParameteri)
#define glFramebufferRenderbuffer                     ((GL3W_CONTEXT_METHOD)->gl3wFramebufferRenderbuffer)
#define glFramebufferTexture                          ((GL3W_CONTEXT_METHOD)->gl3wFramebufferTexture)
#define glFramebufferTexture1D                        ((GL3W_CONTEXT_METHOD)->gl3wFramebufferTexture1D)
#define glFramebufferTexture2D                        ((GL3W_CONTEXT_METHOD)->gl3wFramebufferTexture2D)
#define glFramebufferTexture3D                        ((GL3W_CONTEXT_METHOD)->gl3wFramebufferTexture3D)
#define glFramebufferTextureLayer                     ((GL3W_CONTEXT_METHOD)->gl3wFramebufferTextureLayer)
#define glFrontFace                                   ((GL3W_CONTEXT_METHOD)->gl3wFrontFace)
#define glGenBuffers                                  ((GL3W_CONTEXT_METHOD)->gl3wGenBuffers)
#define glGenFramebuffers                             ((GL3W_CONTEXT_METHOD)->gl3wGenFramebuffers)
#define glGenProgramPipelines                         ((GL3W_CONTEXT_METHOD)->gl3wGenProgramPipelines)
#define glGenQueries                                  ((GL3W_CONTEXT_METHOD)->gl3wGenQueries)
#define glGenRenderbuffers                            ((GL3W_CONTEXT_METHOD)->gl3wGenRenderbuffers)
#define glGenSamplers                                 ((GL3W_CONTEXT_METHOD)->gl3wGenSamplers)
#define glGenTextures                                 ((GL3W_CONTEXT_METHOD)->gl3wGenTextures)
#define glGenTransformFeedbacks                       ((GL3W_CONTEXT_METHOD)->gl3wGenTransformFeedbacks)
#define glGenVertexArrays                             ((GL3W_CONTEXT_METHOD)->gl3wGenVertexArrays)
#define glGenerateMipmap                              ((GL3W_CONTEXT_METHOD)->gl3wGenerateMipmap)
#define glGenerateTextureMipmap                       ((GL3W_CONTEXT_METHOD)->gl3wGenerateTextureMipmap)
#define glGetActiveAtomicCounterBufferiv              ((GL3W_CONTEXT_METHOD)->gl3wGetActiveAtomicCounterBufferiv)
#define glGetActiveAttrib                             ((GL3W_CONTEXT_METHOD)->gl3wGetActiveAttrib)
#define glGetActiveSubroutineName                     ((GL3W_CONTEXT_METHOD)->gl3wGetActiveSubroutineName)
#define glGetActiveSubroutineUniformName              ((GL3W_CONTEXT_METHOD)->gl3wGetActiveSubroutineUniformName)
#define glGetActiveSubroutineUniformiv                ((GL3W_CONTEXT_METHOD)->gl3wGetActiveSubroutineUniformiv)
#define glGetActiveUniform                            ((GL3W_CONTEXT_METHOD)->gl3wGetActiveUniform)
#define glGetActiveUniformBlockName                   ((GL3W_CONTEXT_METHOD)->gl3wGetActiveUniformBlockName)
#define glGetActiveUniformBlockiv                     ((GL3W_CONTEXT_METHOD)->gl3wGetActiveUniformBlockiv)
#define glGetActiveUniformName                        ((GL3W_CONTEXT_METHOD)->gl3wGetActiveUniformName)
#define glGetActiveUniformsiv                         ((GL3W_CONTEXT_METHOD)->gl3wGetActiveUniformsiv)
#define glGetAttachedShaders                          ((GL3W_CONTEXT_METHOD)->gl3wGetAttachedShaders)
#define glGetAttribLocation                           ((GL3W_CONTEXT_METHOD)->gl3wGetAttribLocation)
#define glGetBooleani_v                               ((GL3W_CONTEXT_METHOD)->gl3wGetBooleani_v)
#define glGetBooleanv                                 ((GL3W_CONTEXT_METHOD)->gl3wGetBooleanv)
#define glGetBufferParameteri64v                      ((GL3W_CONTEXT_METHOD)->gl3wGetBufferParameteri64v)
#define glGetBufferParameteriv                        ((GL3W_CONTEXT_METHOD)->gl3wGetBufferParameteriv)
#define glGetBufferPointerv                           ((GL3W_CONTEXT_METHOD)->gl3wGetBufferPointerv)
#define glGetBufferSubData                            ((GL3W_CONTEXT_METHOD)->gl3wGetBufferSubData)
#define glGetCompressedTexImage                       ((GL3W_CONTEXT_METHOD)->gl3wGetCompressedTexImage)
#define glGetCompressedTextureImage                   ((GL3W_CONTEXT_METHOD)->gl3wGetCompressedTextureImage)
#define glGetCompressedTextureSubImage                ((GL3W_CONTEXT_METHOD)->gl3wGetCompressedTextureSubImage)
#define glGetDebugMessageLog                          ((GL3W_CONTEXT_METHOD)->gl3wGetDebugMessageLog)
#define glGetDebugMessageLogARB                       ((GL3W_CONTEXT_METHOD)->gl3wGetDebugMessageLogARB)
#define glGetDoublei_v                                ((GL3W_CONTEXT_METHOD)->gl3wGetDoublei_v)
#define glGetDoublev                                  ((GL3W_CONTEXT_METHOD)->gl3wGetDoublev)
#define glGetError                                    ((GL3W_CONTEXT_METHOD)->gl3wGetError)
#define glGetFloati_v                                 ((GL3W_CONTEXT_METHOD)->gl3wGetFloati_v)
#define glGetFloatv                                   ((GL3W_CONTEXT_METHOD)->gl3wGetFloatv)
#define glGetFragDataIndex                            ((GL3W_CONTEXT_METHOD)->gl3wGetFragDataIndex)
#define glGetFragDataLocation                         ((GL3W_CONTEXT_METHOD)->gl3wGetFragDataLocation)
#define glGetFramebufferAttachmentParameteriv         ((GL3W_CONTEXT_METHOD)->gl3wGetFramebufferAttachmentParameteriv)
#define glGetFramebufferParameteriv                   ((GL3W_CONTEXT_METHOD)->gl3wGetFramebufferParameteriv)
#define glGetGraphicsResetStatus                      ((GL3W_CONTEXT_METHOD)->gl3wGetGraphicsResetStatus)
#define glGetGraphicsResetStatusARB                   ((GL3W_CONTEXT_METHOD)->gl3wGetGraphicsResetStatusARB)
#define glGetImageHandleARB                           ((GL3W_CONTEXT_METHOD)->gl3wGetImageHandleARB)
#define glGetInteger64i_v                             ((GL3W_CONTEXT_METHOD)->gl3wGetInteger64i_v)
#define glGetInteger64v                               ((GL3W_CONTEXT_METHOD)->gl3wGetInteger64v)
#define glGetIntegeri_v                               ((GL3W_CONTEXT_METHOD)->gl3wGetIntegeri_v)
#define glGetIntegerv                                 ((GL3W_CONTEXT_METHOD)->gl3wGetIntegerv)
#define glGetInternalformati64v                       ((GL3W_CONTEXT_METHOD)->gl3wGetInternalformati64v)
#define glGetInternalformativ                         ((GL3W_CONTEXT_METHOD)->gl3wGetInternalformativ)
#define glGetMultisamplefv                            ((GL3W_CONTEXT_METHOD)->gl3wGetMultisamplefv)
#define glGetNamedBufferParameteri64v                 ((GL3W_CONTEXT_METHOD)->gl3wGetNamedBufferParameteri64v)
#define glGetNamedBufferParameteriv                   ((GL3W_CONTEXT_METHOD)->gl3wGetNamedBufferParameteriv)
#define glGetNamedBufferPointerv                      ((GL3W_CONTEXT_METHOD)->gl3wGetNamedBufferPointerv)
#define glGetNamedBufferSubData                       ((GL3W_CONTEXT_METHOD)->gl3wGetNamedBufferSubData)
#define glGetNamedFramebufferAttachmentParameteriv    ((GL3W_CONTEXT_METHOD)->gl3wGetNamedFramebufferAttachmentParameteriv)
#define glGetNamedFramebufferParameteriv              ((GL3W_CONTEXT_METHOD)->gl3wGetNamedFramebufferParameteriv)
#define glGetNamedRenderbufferParameteriv             ((GL3W_CONTEXT_METHOD)->gl3wGetNamedRenderbufferParameteriv)
#define glGetNamedStringARB                           ((GL3W_CONTEXT_METHOD)->gl3wGetNamedStringARB)
#define glGetNamedStringivARB                         ((GL3W_CONTEXT_METHOD)->gl3wGetNamedStringivARB)
#define glGetObjectLabel                              ((GL3W_CONTEXT_METHOD)->gl3wGetObjectLabel)
#define glGetObjectPtrLabel                           ((GL3W_CONTEXT_METHOD)->gl3wGetObjectPtrLabel)
#define glGetPointerv                                 ((GL3W_CONTEXT_METHOD)->gl3wGetPointerv)
#define glGetProgramBinary                            ((GL3W_CONTEXT_METHOD)->gl3wGetProgramBinary)
#define glGetProgramInfoLog                           ((GL3W_CONTEXT_METHOD)->gl3wGetProgramInfoLog)
#define glGetProgramInterfaceiv                       ((GL3W_CONTEXT_METHOD)->gl3wGetProgramInterfaceiv)
#define glGetProgramPipelineInfoLog                   ((GL3W_CONTEXT_METHOD)->gl3wGetProgramPipelineInfoLog)
#define glGetProgramPipelineiv                        ((GL3W_CONTEXT_METHOD)->gl3wGetProgramPipelineiv)
#define glGetProgramResourceIndex                     ((GL3W_CONTEXT_METHOD)->gl3wGetProgramResourceIndex)
#define glGetProgramResourceLocation                  ((GL3W_CONTEXT_METHOD)->gl3wGetProgramResourceLocation)
#define glGetProgramResourceLocationIndex             ((GL3W_CONTEXT_METHOD)->gl3wGetProgramResourceLocationIndex)
#define glGetProgramResourceName                      ((GL3W_CONTEXT_METHOD)->gl3wGetProgramResourceName)
#define glGetProgramResourceiv                        ((GL3W_CONTEXT_METHOD)->gl3wGetProgramResourceiv)
#define glGetProgramStageiv                           ((GL3W_CONTEXT_METHOD)->gl3wGetProgramStageiv)
#define glGetProgramiv                                ((GL3W_CONTEXT_METHOD)->gl3wGetProgramiv)
#define glGetQueryBufferObjecti64v                    ((GL3W_CONTEXT_METHOD)->gl3wGetQueryBufferObjecti64v)
#define glGetQueryBufferObjectiv                      ((GL3W_CONTEXT_METHOD)->gl3wGetQueryBufferObjectiv)
#define glGetQueryBufferObjectui64v                   ((GL3W_CONTEXT_METHOD)->gl3wGetQueryBufferObjectui64v)
#define glGetQueryBufferObjectuiv                     ((GL3W_CONTEXT_METHOD)->gl3wGetQueryBufferObjectuiv)
#define glGetQueryIndexediv                           ((GL3W_CONTEXT_METHOD)->gl3wGetQueryIndexediv)
#define glGetQueryObjecti64v                          ((GL3W_CONTEXT_METHOD)->gl3wGetQueryObjecti64v)
#define glGetQueryObjectiv                            ((GL3W_CONTEXT_METHOD)->gl3wGetQueryObjectiv)
#define glGetQueryObjectui64v                         ((GL3W_CONTEXT_METHOD)->gl3wGetQueryObjectui64v)
#define glGetQueryObjectuiv                           ((GL3W_CONTEXT_METHOD)->gl3wGetQueryObjectuiv)
#define glGetQueryiv                                  ((GL3W_CONTEXT_METHOD)->gl3wGetQueryiv)
#define glGetRenderbufferParameteriv                  ((GL3W_CONTEXT_METHOD)->gl3wGetRenderbufferParameteriv)
#define glGetSamplerParameterIiv                      ((GL3W_CONTEXT_METHOD)->gl3wGetSamplerParameterIiv)
#define glGetSamplerParameterIuiv                     ((GL3W_CONTEXT_METHOD)->gl3wGetSamplerParameterIuiv)
#define glGetSamplerParameterfv                       ((GL3W_CONTEXT_METHOD)->gl3wGetSamplerParameterfv)
#define glGetSamplerParameteriv                       ((GL3W_CONTEXT_METHOD)->gl3wGetSamplerParameteriv)
#define glGetShaderInfoLog                            ((GL3W_CONTEXT_METHOD)->gl3wGetShaderInfoLog)
#define glGetShaderPrecisionFormat                    ((GL3W_CONTEXT_METHOD)->gl3wGetShaderPrecisionFormat)
#define glGetShaderSource                             ((GL3W_CONTEXT_METHOD)->gl3wGetShaderSource)
#define glGetShaderiv                                 ((GL3W_CONTEXT_METHOD)->gl3wGetShaderiv)
#define glGetString                                   ((GL3W_CONTEXT_METHOD)->gl3wGetString)
#define glGetStringi                                  ((GL3W_CONTEXT_METHOD)->gl3wGetStringi)
#define glGetSubroutineIndex                          ((GL3W_CONTEXT_METHOD)->gl3wGetSubroutineIndex)
#define glGetSubroutineUniformLocation                ((GL3W_CONTEXT_METHOD)->gl3wGetSubroutineUniformLocation)
#define glGetSynciv                                   ((GL3W_CONTEXT_METHOD)->gl3wGetSynciv)
#define glGetTexImage                                 ((GL3W_CONTEXT_METHOD)->gl3wGetTexImage)
#define glGetTexLevelParameterfv                      ((GL3W_CONTEXT_METHOD)->gl3wGetTexLevelParameterfv)
#define glGetTexLevelParameteriv                      ((GL3W_CONTEXT_METHOD)->gl3wGetTexLevelParameteriv)
#define glGetTexParameterIiv                          ((GL3W_CONTEXT_METHOD)->gl3wGetTexParameterIiv)
#define glGetTexParameterIuiv                         ((GL3W_CONTEXT_METHOD)->gl3wGetTexParameterIuiv)
#define glGetTexParameterfv                           ((GL3W_CONTEXT_METHOD)->gl3wGetTexParameterfv)
#define glGetTexParameteriv                           ((GL3W_CONTEXT_METHOD)->gl3wGetTexParameteriv)
#define glGetTextureHandleARB                         ((GL3W_CONTEXT_METHOD)->gl3wGetTextureHandleARB)
#define glGetTextureImage                             ((GL3W_CONTEXT_METHOD)->gl3wGetTextureImage)
#define glGetTextureLevelParameterfv                  ((GL3W_CONTEXT_METHOD)->gl3wGetTextureLevelParameterfv)
#define glGetTextureLevelParameteriv                  ((GL3W_CONTEXT_METHOD)->gl3wGetTextureLevelParameteriv)
#define glGetTextureParameterIiv                      ((GL3W_CONTEXT_METHOD)->gl3wGetTextureParameterIiv)
#define glGetTextureParameterIuiv                     ((GL3W_CONTEXT_METHOD)->gl3wGetTextureParameterIuiv)
#define glGetTextureParameterfv                       ((GL3W_CONTEXT_METHOD)->gl3wGetTextureParameterfv)
#define glGetTextureParameteriv                       ((GL3W_CONTEXT_METHOD)->gl3wGetTextureParameteriv)
#define glGetTextureSamplerHandleARB                  ((GL3W_CONTEXT_METHOD)->gl3wGetTextureSamplerHandleARB)
#define glGetTextureSubImage                          ((GL3W_CONTEXT_METHOD)->gl3wGetTextureSubImage)
#define glGetTransformFeedbackVarying                 ((GL3W_CONTEXT_METHOD)->gl3wGetTransformFeedbackVarying)
#define glGetTransformFeedbacki64_v                   ((GL3W_CONTEXT_METHOD)->gl3wGetTransformFeedbacki64_v)
#define glGetTransformFeedbacki_v                     ((GL3W_CONTEXT_METHOD)->gl3wGetTransformFeedbacki_v)
#define glGetTransformFeedbackiv                      ((GL3W_CONTEXT_METHOD)->gl3wGetTransformFeedbackiv)
#define glGetUniformBlockIndex                        ((GL3W_CONTEXT_METHOD)->gl3wGetUniformBlockIndex)
#define glGetUniformIndices                           ((GL3W_CONTEXT_METHOD)->gl3wGetUniformIndices)
#define glGetUniformLocation                          ((GL3W_CONTEXT_METHOD)->gl3wGetUniformLocation)
#define glGetUniformSubroutineuiv                     ((GL3W_CONTEXT_METHOD)->gl3wGetUniformSubroutineuiv)
#define glGetUniformdv                                ((GL3W_CONTEXT_METHOD)->gl3wGetUniformdv)
#define glGetUniformfv                                ((GL3W_CONTEXT_METHOD)->gl3wGetUniformfv)
#define glGetUniformiv                                ((GL3W_CONTEXT_METHOD)->gl3wGetUniformiv)
#define glGetUniformuiv                               ((GL3W_CONTEXT_METHOD)->gl3wGetUniformuiv)
#define glGetVertexArrayIndexed64iv                   ((GL3W_CONTEXT_METHOD)->gl3wGetVertexArrayIndexed64iv)
#define glGetVertexArrayIndexediv                     ((GL3W_CONTEXT_METHOD)->gl3wGetVertexArrayIndexediv)
#define glGetVertexArrayiv                            ((GL3W_CONTEXT_METHOD)->gl3wGetVertexArrayiv)
#define glGetVertexAttribIiv                          ((GL3W_CONTEXT_METHOD)->gl3wGetVertexAttribIiv)
#define glGetVertexAttribIuiv                         ((GL3W_CONTEXT_METHOD)->gl3wGetVertexAttribIuiv)
#define glGetVertexAttribLdv                          ((GL3W_CONTEXT_METHOD)->gl3wGetVertexAttribLdv)
#define glGetVertexAttribLui64vARB                    ((GL3W_CONTEXT_METHOD)->gl3wGetVertexAttribLui64vARB)
#define glGetVertexAttribPointerv                     ((GL3W_CONTEXT_METHOD)->gl3wGetVertexAttribPointerv)
#define glGetVertexAttribdv                           ((GL3W_CONTEXT_METHOD)->gl3wGetVertexAttribdv)
#define glGetVertexAttribfv                           ((GL3W_CONTEXT_METHOD)->gl3wGetVertexAttribfv)
#define glGetVertexAttribiv                           ((GL3W_CONTEXT_METHOD)->gl3wGetVertexAttribiv)
#define glGetnCompressedTexImage                      ((GL3W_CONTEXT_METHOD)->gl3wGetnCompressedTexImage)
#define glGetnCompressedTexImageARB                   ((GL3W_CONTEXT_METHOD)->gl3wGetnCompressedTexImageARB)
#define glGetnTexImage                                ((GL3W_CONTEXT_METHOD)->gl3wGetnTexImage)
#define glGetnTexImageARB                             ((GL3W_CONTEXT_METHOD)->gl3wGetnTexImageARB)
#define glGetnUniformdv                               ((GL3W_CONTEXT_METHOD)->gl3wGetnUniformdv)
#define glGetnUniformdvARB                            ((GL3W_CONTEXT_METHOD)->gl3wGetnUniformdvARB)
#define glGetnUniformfv                               ((GL3W_CONTEXT_METHOD)->gl3wGetnUniformfv)
#define glGetnUniformfvARB                            ((GL3W_CONTEXT_METHOD)->gl3wGetnUniformfvARB)
#define glGetnUniformiv                               ((GL3W_CONTEXT_METHOD)->gl3wGetnUniformiv)
#define glGetnUniformivARB                            ((GL3W_CONTEXT_METHOD)->gl3wGetnUniformivARB)
#define glGetnUniformuiv                              ((GL3W_CONTEXT_METHOD)->gl3wGetnUniformuiv)
#define glGetnUniformuivARB                           ((GL3W_CONTEXT_METHOD)->gl3wGetnUniformuivARB)
#define glHint                                        ((GL3W_CONTEXT_METHOD)->gl3wHint)
#define glInvalidateBufferData                        ((GL3W_CONTEXT_METHOD)->gl3wInvalidateBufferData)
#define glInvalidateBufferSubData                     ((GL3W_CONTEXT_METHOD)->gl3wInvalidateBufferSubData)
#define glInvalidateFramebuffer                       ((GL3W_CONTEXT_METHOD)->gl3wInvalidateFramebuffer)
#define glInvalidateNamedFramebufferData              ((GL3W_CONTEXT_METHOD)->gl3wInvalidateNamedFramebufferData)
#define glInvalidateNamedFramebufferSubData           ((GL3W_CONTEXT_METHOD)->gl3wInvalidateNamedFramebufferSubData)
#define glInvalidateSubFramebuffer                    ((GL3W_CONTEXT_METHOD)->gl3wInvalidateSubFramebuffer)
#define glInvalidateTexImage                          ((GL3W_CONTEXT_METHOD)->gl3wInvalidateTexImage)
#define glInvalidateTexSubImage                       ((GL3W_CONTEXT_METHOD)->gl3wInvalidateTexSubImage)
#define glIsBuffer                                    ((GL3W_CONTEXT_METHOD)->gl3wIsBuffer)
#define glIsEnabled                                   ((GL3W_CONTEXT_METHOD)->gl3wIsEnabled)
#define glIsEnabledi                                  ((GL3W_CONTEXT_METHOD)->gl3wIsEnabledi)
#define glIsFramebuffer                               ((GL3W_CONTEXT_METHOD)->gl3wIsFramebuffer)
#define glIsImageHandleResidentARB                    ((GL3W_CONTEXT_METHOD)->gl3wIsImageHandleResidentARB)
#define glIsNamedStringARB                            ((GL3W_CONTEXT_METHOD)->gl3wIsNamedStringARB)
#define glIsProgram                                   ((GL3W_CONTEXT_METHOD)->gl3wIsProgram)
#define glIsProgramPipeline                           ((GL3W_CONTEXT_METHOD)->gl3wIsProgramPipeline)
#define glIsQuery                                     ((GL3W_CONTEXT_METHOD)->gl3wIsQuery)
#define glIsRenderbuffer                              ((GL3W_CONTEXT_METHOD)->gl3wIsRenderbuffer)
#define glIsSampler                                   ((GL3W_CONTEXT_METHOD)->gl3wIsSampler)
#define glIsShader                                    ((GL3W_CONTEXT_METHOD)->gl3wIsShader)
#define glIsSync                                      ((GL3W_CONTEXT_METHOD)->gl3wIsSync)
#define glIsTexture                                   ((GL3W_CONTEXT_METHOD)->gl3wIsTexture)
#define glIsTextureHandleResidentARB                  ((GL3W_CONTEXT_METHOD)->gl3wIsTextureHandleResidentARB)
#define glIsTransformFeedback                         ((GL3W_CONTEXT_METHOD)->gl3wIsTransformFeedback)
#define glIsVertexArray                               ((GL3W_CONTEXT_METHOD)->gl3wIsVertexArray)
#define glLineWidth                                   ((GL3W_CONTEXT_METHOD)->gl3wLineWidth)
#define glLinkProgram                                 ((GL3W_CONTEXT_METHOD)->gl3wLinkProgram)
#define glLogicOp                                     ((GL3W_CONTEXT_METHOD)->gl3wLogicOp)
#define glMakeImageHandleNonResidentARB               ((GL3W_CONTEXT_METHOD)->gl3wMakeImageHandleNonResidentARB)
#define glMakeImageHandleResidentARB                  ((GL3W_CONTEXT_METHOD)->gl3wMakeImageHandleResidentARB)
#define glMakeTextureHandleNonResidentARB             ((GL3W_CONTEXT_METHOD)->gl3wMakeTextureHandleNonResidentARB)
#define glMakeTextureHandleResidentARB                ((GL3W_CONTEXT_METHOD)->gl3wMakeTextureHandleResidentARB)
#define glMapBuffer                                   ((GL3W_CONTEXT_METHOD)->gl3wMapBuffer)
#define glMapBufferRange                              ((GL3W_CONTEXT_METHOD)->gl3wMapBufferRange)
#define glMapNamedBuffer                              ((GL3W_CONTEXT_METHOD)->gl3wMapNamedBuffer)
#define glMapNamedBufferRange                         ((GL3W_CONTEXT_METHOD)->gl3wMapNamedBufferRange)
#define glMemoryBarrier                               ((GL3W_CONTEXT_METHOD)->gl3wMemoryBarrier)
#define glMemoryBarrierByRegion                       ((GL3W_CONTEXT_METHOD)->gl3wMemoryBarrierByRegion)
#define glMinSampleShading                            ((GL3W_CONTEXT_METHOD)->gl3wMinSampleShading)
#define glMinSampleShadingARB                         ((GL3W_CONTEXT_METHOD)->gl3wMinSampleShadingARB)
#define glMultiDrawArrays                             ((GL3W_CONTEXT_METHOD)->gl3wMultiDrawArrays)
#define glMultiDrawArraysIndirect                     ((GL3W_CONTEXT_METHOD)->gl3wMultiDrawArraysIndirect)
#define glMultiDrawArraysIndirectCountARB             ((GL3W_CONTEXT_METHOD)->gl3wMultiDrawArraysIndirectCountARB)
#define glMultiDrawElements                           ((GL3W_CONTEXT_METHOD)->gl3wMultiDrawElements)
#define glMultiDrawElementsBaseVertex                 ((GL3W_CONTEXT_METHOD)->gl3wMultiDrawElementsBaseVertex)
#define glMultiDrawElementsIndirect                   ((GL3W_CONTEXT_METHOD)->gl3wMultiDrawElementsIndirect)
#define glMultiDrawElementsIndirectCountARB           ((GL3W_CONTEXT_METHOD)->gl3wMultiDrawElementsIndirectCountARB)
#define glNamedBufferData                             ((GL3W_CONTEXT_METHOD)->gl3wNamedBufferData)
#define glNamedBufferPageCommitmentARB                ((GL3W_CONTEXT_METHOD)->gl3wNamedBufferPageCommitmentARB)
#define glNamedBufferPageCommitmentEXT                ((GL3W_CONTEXT_METHOD)->gl3wNamedBufferPageCommitmentEXT)
#define glNamedBufferStorage                          ((GL3W_CONTEXT_METHOD)->gl3wNamedBufferStorage)
#define glNamedBufferSubData                          ((GL3W_CONTEXT_METHOD)->gl3wNamedBufferSubData)
#define glNamedFramebufferDrawBuffer                  ((GL3W_CONTEXT_METHOD)->gl3wNamedFramebufferDrawBuffer)
#define glNamedFramebufferDrawBuffers                 ((GL3W_CONTEXT_METHOD)->gl3wNamedFramebufferDrawBuffers)
#define glNamedFramebufferParameteri                  ((GL3W_CONTEXT_METHOD)->gl3wNamedFramebufferParameteri)
#define glNamedFramebufferReadBuffer                  ((GL3W_CONTEXT_METHOD)->gl3wNamedFramebufferReadBuffer)
#define glNamedFramebufferRenderbuffer                ((GL3W_CONTEXT_METHOD)->gl3wNamedFramebufferRenderbuffer)
#define glNamedFramebufferTexture                     ((GL3W_CONTEXT_METHOD)->gl3wNamedFramebufferTexture)
#define glNamedFramebufferTextureLayer                ((GL3W_CONTEXT_METHOD)->gl3wNamedFramebufferTextureLayer)
#define glNamedRenderbufferStorage                    ((GL3W_CONTEXT_METHOD)->gl3wNamedRenderbufferStorage)
#define glNamedRenderbufferStorageMultisample         ((GL3W_CONTEXT_METHOD)->gl3wNamedRenderbufferStorageMultisample)
#define glNamedStringARB                              ((GL3W_CONTEXT_METHOD)->gl3wNamedStringARB)
#define glObjectLabel                                 ((GL3W_CONTEXT_METHOD)->gl3wObjectLabel)
#define glObjectPtrLabel                              ((GL3W_CONTEXT_METHOD)->gl3wObjectPtrLabel)
#define glPatchParameterfv                            ((GL3W_CONTEXT_METHOD)->gl3wPatchParameterfv)
#define glPatchParameteri                             ((GL3W_CONTEXT_METHOD)->gl3wPatchParameteri)
#define glPauseTransformFeedback                      ((GL3W_CONTEXT_METHOD)->gl3wPauseTransformFeedback)
#define glPixelStoref                                 ((GL3W_CONTEXT_METHOD)->gl3wPixelStoref)
#define glPixelStorei                                 ((GL3W_CONTEXT_METHOD)->gl3wPixelStorei)
#define glPointParameterf                             ((GL3W_CONTEXT_METHOD)->gl3wPointParameterf)
#define glPointParameterfv                            ((GL3W_CONTEXT_METHOD)->gl3wPointParameterfv)
#define glPointParameteri                             ((GL3W_CONTEXT_METHOD)->gl3wPointParameteri)
#define glPointParameteriv                            ((GL3W_CONTEXT_METHOD)->gl3wPointParameteriv)
#define glPointSize                                   ((GL3W_CONTEXT_METHOD)->gl3wPointSize)
#define glPolygonMode                                 ((GL3W_CONTEXT_METHOD)->gl3wPolygonMode)
#define glPolygonOffset                               ((GL3W_CONTEXT_METHOD)->gl3wPolygonOffset)
#define glPopDebugGroup                               ((GL3W_CONTEXT_METHOD)->gl3wPopDebugGroup)
#define glPrimitiveRestartIndex                       ((GL3W_CONTEXT_METHOD)->gl3wPrimitiveRestartIndex)
#define glProgramBinary                               ((GL3W_CONTEXT_METHOD)->gl3wProgramBinary)
#define glProgramParameteri                           ((GL3W_CONTEXT_METHOD)->gl3wProgramParameteri)
#define glProgramUniform1d                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform1d)
#define glProgramUniform1dv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform1dv)
#define glProgramUniform1f                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform1f)
#define glProgramUniform1fv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform1fv)
#define glProgramUniform1i                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform1i)
#define glProgramUniform1iv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform1iv)
#define glProgramUniform1ui                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform1ui)
#define glProgramUniform1uiv                          ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform1uiv)
#define glProgramUniform2d                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform2d)
#define glProgramUniform2dv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform2dv)
#define glProgramUniform2f                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform2f)
#define glProgramUniform2fv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform2fv)
#define glProgramUniform2i                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform2i)
#define glProgramUniform2iv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform2iv)
#define glProgramUniform2ui                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform2ui)
#define glProgramUniform2uiv                          ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform2uiv)
#define glProgramUniform3d                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform3d)
#define glProgramUniform3dv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform3dv)
#define glProgramUniform3f                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform3f)
#define glProgramUniform3fv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform3fv)
#define glProgramUniform3i                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform3i)
#define glProgramUniform3iv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform3iv)
#define glProgramUniform3ui                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform3ui)
#define glProgramUniform3uiv                          ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform3uiv)
#define glProgramUniform4d                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform4d)
#define glProgramUniform4dv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform4dv)
#define glProgramUniform4f                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform4f)
#define glProgramUniform4fv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform4fv)
#define glProgramUniform4i                            ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform4i)
#define glProgramUniform4iv                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform4iv)
#define glProgramUniform4ui                           ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform4ui)
#define glProgramUniform4uiv                          ((GL3W_CONTEXT_METHOD)->gl3wProgramUniform4uiv)
#define glProgramUniformHandleui64ARB                 ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformHandleui64ARB)
#define glProgramUniformHandleui64vARB                ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformHandleui64vARB)
#define glProgramUniformMatrix2dv                     ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix2dv)
#define glProgramUniformMatrix2fv                     ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix2fv)
#define glProgramUniformMatrix2x3dv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix2x3dv)
#define glProgramUniformMatrix2x3fv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix2x3fv)
#define glProgramUniformMatrix2x4dv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix2x4dv)
#define glProgramUniformMatrix2x4fv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix2x4fv)
#define glProgramUniformMatrix3dv                     ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix3dv)
#define glProgramUniformMatrix3fv                     ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix3fv)
#define glProgramUniformMatrix3x2dv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix3x2dv)
#define glProgramUniformMatrix3x2fv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix3x2fv)
#define glProgramUniformMatrix3x4dv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix3x4dv)
#define glProgramUniformMatrix3x4fv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix3x4fv)
#define glProgramUniformMatrix4dv                     ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix4dv)
#define glProgramUniformMatrix4fv                     ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix4fv)
#define glProgramUniformMatrix4x2dv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix4x2dv)
#define glProgramUniformMatrix4x2fv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix4x2fv)
#define glProgramUniformMatrix4x3dv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix4x3dv)
#define glProgramUniformMatrix4x3fv                   ((GL3W_CONTEXT_METHOD)->gl3wProgramUniformMatrix4x3fv)
#define glProvokingVertex                             ((GL3W_CONTEXT_METHOD)->gl3wProvokingVertex)
#define glPushDebugGroup                              ((GL3W_CONTEXT_METHOD)->gl3wPushDebugGroup)
#define glQueryCounter                                ((GL3W_CONTEXT_METHOD)->gl3wQueryCounter)
#define glReadBuffer                                  ((GL3W_CONTEXT_METHOD)->gl3wReadBuffer)
#define glReadPixels                                  ((GL3W_CONTEXT_METHOD)->gl3wReadPixels)
#define glReadnPixels                                 ((GL3W_CONTEXT_METHOD)->gl3wReadnPixels)
#define glReadnPixelsARB                              ((GL3W_CONTEXT_METHOD)->gl3wReadnPixelsARB)
#define glReleaseShaderCompiler                       ((GL3W_CONTEXT_METHOD)->gl3wReleaseShaderCompiler)
#define glRenderbufferStorage                         ((GL3W_CONTEXT_METHOD)->gl3wRenderbufferStorage)
#define glRenderbufferStorageMultisample              ((GL3W_CONTEXT_METHOD)->gl3wRenderbufferStorageMultisample)
#define glResumeTransformFeedback                     ((GL3W_CONTEXT_METHOD)->gl3wResumeTransformFeedback)
#define glSampleCoverage                              ((GL3W_CONTEXT_METHOD)->gl3wSampleCoverage)
#define glSampleMaski                                 ((GL3W_CONTEXT_METHOD)->gl3wSampleMaski)
#define glSamplerParameterIiv                         ((GL3W_CONTEXT_METHOD)->gl3wSamplerParameterIiv)
#define glSamplerParameterIuiv                        ((GL3W_CONTEXT_METHOD)->gl3wSamplerParameterIuiv)
#define glSamplerParameterf                           ((GL3W_CONTEXT_METHOD)->gl3wSamplerParameterf)
#define glSamplerParameterfv                          ((GL3W_CONTEXT_METHOD)->gl3wSamplerParameterfv)
#define glSamplerParameteri                           ((GL3W_CONTEXT_METHOD)->gl3wSamplerParameteri)
#define glSamplerParameteriv                          ((GL3W_CONTEXT_METHOD)->gl3wSamplerParameteriv)
#define glScissor                                     ((GL3W_CONTEXT_METHOD)->gl3wScissor)
#define glScissorArrayv                               ((GL3W_CONTEXT_METHOD)->gl3wScissorArrayv)
#define glScissorIndexed                              ((GL3W_CONTEXT_METHOD)->gl3wScissorIndexed)
#define glScissorIndexedv                             ((GL3W_CONTEXT_METHOD)->gl3wScissorIndexedv)
#define glShaderBinary                                ((GL3W_CONTEXT_METHOD)->gl3wShaderBinary)
#define glShaderSource                                ((GL3W_CONTEXT_METHOD)->gl3wShaderSource)
#define glShaderStorageBlockBinding                   ((GL3W_CONTEXT_METHOD)->gl3wShaderStorageBlockBinding)
#define glStencilFunc                                 ((GL3W_CONTEXT_METHOD)->gl3wStencilFunc)
#define glStencilFuncSeparate                         ((GL3W_CONTEXT_METHOD)->gl3wStencilFuncSeparate)
#define glStencilMask                                 ((GL3W_CONTEXT_METHOD)->gl3wStencilMask)
#define glStencilMaskSeparate                         ((GL3W_CONTEXT_METHOD)->gl3wStencilMaskSeparate)
#define glStencilOp                                   ((GL3W_CONTEXT_METHOD)->gl3wStencilOp)
#define glStencilOpSeparate                           ((GL3W_CONTEXT_METHOD)->gl3wStencilOpSeparate)
#define glTexBuffer                                   ((GL3W_CONTEXT_METHOD)->gl3wTexBuffer)
#define glTexBufferRange                              ((GL3W_CONTEXT_METHOD)->gl3wTexBufferRange)
#define glTexImage1D                                  ((GL3W_CONTEXT_METHOD)->gl3wTexImage1D)
#define glTexImage2D                                  ((GL3W_CONTEXT_METHOD)->gl3wTexImage2D)
#define glTexImage2DMultisample                       ((GL3W_CONTEXT_METHOD)->gl3wTexImage2DMultisample)
#define glTexImage3D                                  ((GL3W_CONTEXT_METHOD)->gl3wTexImage3D)
#define glTexImage3DMultisample                       ((GL3W_CONTEXT_METHOD)->gl3wTexImage3DMultisample)
#define glTexPageCommitmentARB                        ((GL3W_CONTEXT_METHOD)->gl3wTexPageCommitmentARB)
#define glTexParameterIiv                             ((GL3W_CONTEXT_METHOD)->gl3wTexParameterIiv)
#define glTexParameterIuiv                            ((GL3W_CONTEXT_METHOD)->gl3wTexParameterIuiv)
#define glTexParameterf                               ((GL3W_CONTEXT_METHOD)->gl3wTexParameterf)
#define glTexParameterfv                              ((GL3W_CONTEXT_METHOD)->gl3wTexParameterfv)
#define glTexParameteri                               ((GL3W_CONTEXT_METHOD)->gl3wTexParameteri)
#define glTexParameteriv                              ((GL3W_CONTEXT_METHOD)->gl3wTexParameteriv)
#define glTexStorage1D                                ((GL3W_CONTEXT_METHOD)->gl3wTexStorage1D)
#define glTexStorage2D                                ((GL3W_CONTEXT_METHOD)->gl3wTexStorage2D)
#define glTexStorage2DMultisample                     ((GL3W_CONTEXT_METHOD)->gl3wTexStorage2DMultisample)
#define glTexStorage3D                                ((GL3W_CONTEXT_METHOD)->gl3wTexStorage3D)
#define glTexStorage3DMultisample                     ((GL3W_CONTEXT_METHOD)->gl3wTexStorage3DMultisample)
#define glTexSubImage1D                               ((GL3W_CONTEXT_METHOD)->gl3wTexSubImage1D)
#define glTexSubImage2D                               ((GL3W_CONTEXT_METHOD)->gl3wTexSubImage2D)
#define glTexSubImage3D                               ((GL3W_CONTEXT_METHOD)->gl3wTexSubImage3D)
#define glTextureBarrier                              ((GL3W_CONTEXT_METHOD)->gl3wTextureBarrier)
#define glTextureBuffer                               ((GL3W_CONTEXT_METHOD)->gl3wTextureBuffer)
#define glTextureBufferRange                          ((GL3W_CONTEXT_METHOD)->gl3wTextureBufferRange)
#define glTextureParameterIiv                         ((GL3W_CONTEXT_METHOD)->gl3wTextureParameterIiv)
#define glTextureParameterIuiv                        ((GL3W_CONTEXT_METHOD)->gl3wTextureParameterIuiv)
#define glTextureParameterf                           ((GL3W_CONTEXT_METHOD)->gl3wTextureParameterf)
#define glTextureParameterfv                          ((GL3W_CONTEXT_METHOD)->gl3wTextureParameterfv)
#define glTextureParameteri                           ((GL3W_CONTEXT_METHOD)->gl3wTextureParameteri)
#define glTextureParameteriv                          ((GL3W_CONTEXT_METHOD)->gl3wTextureParameteriv)
#define glTextureStorage1D                            ((GL3W_CONTEXT_METHOD)->gl3wTextureStorage1D)
#define glTextureStorage2D                            ((GL3W_CONTEXT_METHOD)->gl3wTextureStorage2D)
#define glTextureStorage2DMultisample                 ((GL3W_CONTEXT_METHOD)->gl3wTextureStorage2DMultisample)
#define glTextureStorage3D                            ((GL3W_CONTEXT_METHOD)->gl3wTextureStorage3D)
#define glTextureStorage3DMultisample                 ((GL3W_CONTEXT_METHOD)->gl3wTextureStorage3DMultisample)
#define glTextureSubImage1D                           ((GL3W_CONTEXT_METHOD)->gl3wTextureSubImage1D)
#define glTextureSubImage2D                           ((GL3W_CONTEXT_METHOD)->gl3wTextureSubImage2D)
#define glTextureSubImage3D                           ((GL3W_CONTEXT_METHOD)->gl3wTextureSubImage3D)
#define glTextureView                                 ((GL3W_CONTEXT_METHOD)->gl3wTextureView)
#define glTransformFeedbackBufferBase                 ((GL3W_CONTEXT_METHOD)->gl3wTransformFeedbackBufferBase)
#define glTransformFeedbackBufferRange                ((GL3W_CONTEXT_METHOD)->gl3wTransformFeedbackBufferRange)
#define glTransformFeedbackVaryings                   ((GL3W_CONTEXT_METHOD)->gl3wTransformFeedbackVaryings)
#define glUniform1d                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform1d)
#define glUniform1dv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform1dv)
#define glUniform1f                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform1f)
#define glUniform1fv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform1fv)
#define glUniform1i                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform1i)
#define glUniform1iv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform1iv)
#define glUniform1ui                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform1ui)
#define glUniform1uiv                                 ((GL3W_CONTEXT_METHOD)->gl3wUniform1uiv)
#define glUniform2d                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform2d)
#define glUniform2dv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform2dv)
#define glUniform2f                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform2f)
#define glUniform2fv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform2fv)
#define glUniform2i                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform2i)
#define glUniform2iv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform2iv)
#define glUniform2ui                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform2ui)
#define glUniform2uiv                                 ((GL3W_CONTEXT_METHOD)->gl3wUniform2uiv)
#define glUniform3d                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform3d)
#define glUniform3dv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform3dv)
#define glUniform3f                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform3f)
#define glUniform3fv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform3fv)
#define glUniform3i                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform3i)
#define glUniform3iv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform3iv)
#define glUniform3ui                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform3ui)
#define glUniform3uiv                                 ((GL3W_CONTEXT_METHOD)->gl3wUniform3uiv)
#define glUniform4d                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform4d)
#define glUniform4dv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform4dv)
#define glUniform4f                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform4f)
#define glUniform4fv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform4fv)
#define glUniform4i                                   ((GL3W_CONTEXT_METHOD)->gl3wUniform4i)
#define glUniform4iv                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform4iv)
#define glUniform4ui                                  ((GL3W_CONTEXT_METHOD)->gl3wUniform4ui)
#define glUniform4uiv                                 ((GL3W_CONTEXT_METHOD)->gl3wUniform4uiv)
#define glUniformBlockBinding                         ((GL3W_CONTEXT_METHOD)->gl3wUniformBlockBinding)
#define glUniformHandleui64ARB                        ((GL3W_CONTEXT_METHOD)->gl3wUniformHandleui64ARB)
#define glUniformHandleui64vARB                       ((GL3W_CONTEXT_METHOD)->gl3wUniformHandleui64vARB)
#define glUniformMatrix2dv                            ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix2dv)
#define glUniformMatrix2fv                            ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix2fv)
#define glUniformMatrix2x3dv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix2x3dv)
#define glUniformMatrix2x3fv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix2x3fv)
#define glUniformMatrix2x4dv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix2x4dv)
#define glUniformMatrix2x4fv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix2x4fv)
#define glUniformMatrix3dv                            ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix3dv)
#define glUniformMatrix3fv                            ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix3fv)
#define glUniformMatrix3x2dv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix3x2dv)
#define glUniformMatrix3x2fv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix3x2fv)
#define glUniformMatrix3x4dv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix3x4dv)
#define glUniformMatrix3x4fv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix3x4fv)
#define glUniformMatrix4dv                            ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix4dv)
#define glUniformMatrix4fv                            ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix4fv)
#define glUniformMatrix4x2dv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix4x2dv)
#define glUniformMatrix4x2fv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix4x2fv)
#define glUniformMatrix4x3dv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix4x3dv)
#define glUniformMatrix4x3fv                          ((GL3W_CONTEXT_METHOD)->gl3wUniformMatrix4x3fv)
#define glUniformSubroutinesuiv                       ((GL3W_CONTEXT_METHOD)->gl3wUniformSubroutinesuiv)
#define glUnmapBuffer                                 ((GL3W_CONTEXT_METHOD)->gl3wUnmapBuffer)
#define glUnmapNamedBuffer                            ((GL3W_CONTEXT_METHOD)->gl3wUnmapNamedBuffer)
#define glUseProgram                                  ((GL3W_CONTEXT_METHOD)->gl3wUseProgram)
#define glUseProgramStages                            ((GL3W_CONTEXT_METHOD)->gl3wUseProgramStages)
#define glValidateProgram                             ((GL3W_CONTEXT_METHOD)->gl3wValidateProgram)
#define glValidateProgramPipeline                     ((GL3W_CONTEXT_METHOD)->gl3wValidateProgramPipeline)
#define glVertexArrayAttribBinding                    ((GL3W_CONTEXT_METHOD)->gl3wVertexArrayAttribBinding)
#define glVertexArrayAttribFormat                     ((GL3W_CONTEXT_METHOD)->gl3wVertexArrayAttribFormat)
#define glVertexArrayAttribIFormat                    ((GL3W_CONTEXT_METHOD)->gl3wVertexArrayAttribIFormat)
#define glVertexArrayAttribLFormat                    ((GL3W_CONTEXT_METHOD)->gl3wVertexArrayAttribLFormat)
#define glVertexArrayBindingDivisor                   ((GL3W_CONTEXT_METHOD)->gl3wVertexArrayBindingDivisor)
#define glVertexArrayElementBuffer                    ((GL3W_CONTEXT_METHOD)->gl3wVertexArrayElementBuffer)
#define glVertexArrayVertexBuffer                     ((GL3W_CONTEXT_METHOD)->gl3wVertexArrayVertexBuffer)
#define glVertexArrayVertexBuffers                    ((GL3W_CONTEXT_METHOD)->gl3wVertexArrayVertexBuffers)
#define glVertexAttrib1d                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib1d)
#define glVertexAttrib1dv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib1dv)
#define glVertexAttrib1f                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib1f)
#define glVertexAttrib1fv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib1fv)
#define glVertexAttrib1s                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib1s)
#define glVertexAttrib1sv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib1sv)
#define glVertexAttrib2d                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib2d)
#define glVertexAttrib2dv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib2dv)
#define glVertexAttrib2f                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib2f)
#define glVertexAttrib2fv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib2fv)
#define glVertexAttrib2s                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib2s)
#define glVertexAttrib2sv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib2sv)
#define glVertexAttrib3d                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib3d)
#define glVertexAttrib3dv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib3dv)
#define glVertexAttrib3f                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib3f)
#define glVertexAttrib3fv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib3fv)
#define glVertexAttrib3s                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib3s)
#define glVertexAttrib3sv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib3sv)
#define glVertexAttrib4Nbv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4Nbv)
#define glVertexAttrib4Niv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4Niv)
#define glVertexAttrib4Nsv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4Nsv)
#define glVertexAttrib4Nub                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4Nub)
#define glVertexAttrib4Nubv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4Nubv)
#define glVertexAttrib4Nuiv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4Nuiv)
#define glVertexAttrib4Nusv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4Nusv)
#define glVertexAttrib4bv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4bv)
#define glVertexAttrib4d                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4d)
#define glVertexAttrib4dv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4dv)
#define glVertexAttrib4f                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4f)
#define glVertexAttrib4fv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4fv)
#define glVertexAttrib4iv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4iv)
#define glVertexAttrib4s                              ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4s)
#define glVertexAttrib4sv                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4sv)
#define glVertexAttrib4ubv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4ubv)
#define glVertexAttrib4uiv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4uiv)
#define glVertexAttrib4usv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttrib4usv)
#define glVertexAttribBinding                         ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribBinding)
#define glVertexAttribDivisor                         ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribDivisor)
#define glVertexAttribFormat                          ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribFormat)
#define glVertexAttribI1i                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI1i)
#define glVertexAttribI1iv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI1iv)
#define glVertexAttribI1ui                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI1ui)
#define glVertexAttribI1uiv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI1uiv)
#define glVertexAttribI2i                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI2i)
#define glVertexAttribI2iv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI2iv)
#define glVertexAttribI2ui                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI2ui)
#define glVertexAttribI2uiv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI2uiv)
#define glVertexAttribI3i                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI3i)
#define glVertexAttribI3iv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI3iv)
#define glVertexAttribI3ui                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI3ui)
#define glVertexAttribI3uiv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI3uiv)
#define glVertexAttribI4bv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI4bv)
#define glVertexAttribI4i                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI4i)
#define glVertexAttribI4iv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI4iv)
#define glVertexAttribI4sv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI4sv)
#define glVertexAttribI4ubv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI4ubv)
#define glVertexAttribI4ui                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI4ui)
#define glVertexAttribI4uiv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI4uiv)
#define glVertexAttribI4usv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribI4usv)
#define glVertexAttribIFormat                         ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribIFormat)
#define glVertexAttribIPointer                        ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribIPointer)
#define glVertexAttribL1d                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL1d)
#define glVertexAttribL1dv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL1dv)
#define glVertexAttribL1ui64ARB                       ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL1ui64ARB)
#define glVertexAttribL1ui64vARB                      ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL1ui64vARB)
#define glVertexAttribL2d                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL2d)
#define glVertexAttribL2dv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL2dv)
#define glVertexAttribL3d                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL3d)
#define glVertexAttribL3dv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL3dv)
#define glVertexAttribL4d                             ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL4d)
#define glVertexAttribL4dv                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribL4dv)
#define glVertexAttribLFormat                         ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribLFormat)
#define glVertexAttribLPointer                        ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribLPointer)
#define glVertexAttribP1ui                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribP1ui)
#define glVertexAttribP1uiv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribP1uiv)
#define glVertexAttribP2ui                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribP2ui)
#define glVertexAttribP2uiv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribP2uiv)
#define glVertexAttribP3ui                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribP3ui)
#define glVertexAttribP3uiv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribP3uiv)
#define glVertexAttribP4ui                            ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribP4ui)
#define glVertexAttribP4uiv                           ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribP4uiv)
#define glVertexAttribPointer                         ((GL3W_CONTEXT_METHOD)->gl3wVertexAttribPointer)
#define glVertexBindingDivisor                        ((GL3W_CONTEXT_METHOD)->gl3wVertexBindingDivisor)
#define glViewport                                    ((GL3W_CONTEXT_METHOD)->gl3wViewport)
#define glViewportArrayv                              ((GL3W_CONTEXT_METHOD)->gl3wViewportArrayv)
#define glViewportIndexedf                            ((GL3W_CONTEXT_METHOD)->gl3wViewportIndexedf)
#define glViewportIndexedfv                           ((GL3W_CONTEXT_METHOD)->gl3wViewportIndexedfv)
#define glWaitSync                                    ((GL3W_CONTEXT_METHOD)->gl3wWaitSync)


typedef void (*GL3WglProc)(void);

/* gl3w api */
int gl3wInit(GL3WContext *context);
void gl3wShutdown(GL3WContext *context);
int gl3wIsSupported(const GL3WContext *context, int major, int minor);
GL3WglProc gl3wGetProcAddress(const char *proc);

#ifdef __cplusplus
}
#endif

#endif
