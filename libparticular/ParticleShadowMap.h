#pragma once

#include "util/OpenGLHelpers.h"
#include <MathGeoLib/MathGeoLib.h>

struct ParticleShadowMap {
    ParticleShadowMap(size_t width, size_t height, size_t depth);
    void Clear();
    void BindAsTarget();
    void BindAsTexture(GLint base);
    void SaveTiled(char const* filename);
    void Propagate();

    size_t width, height, depth;

    util::ResourcePtr propagateProgram;
    util::ResourcePtr volumeTexture;
    util::ResourcePtr fbo;
};