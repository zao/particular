#pragma once

#include <MathGeoLib/MathGeoLib.h>
#include "util/OpenGLHelpers.h"

struct DebugLineSet {
    struct Vertex {
        float3 position;
        float4 color;
    };
    DebugLineSet();
    void Add(float3 vA, float3 vB, float4 cA, float4 cB);
    void Clear();
    void Draw(Frustum const& frustum);

    util::ResourcePtr program;
    std::vector<Vertex> vertices;
};