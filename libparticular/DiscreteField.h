#pragma once

#include <MathGeoLib/MathGeoLib.h>
#include <stddef.h>
#include <vector>

struct DiscreteField {
    DiscreteField(float3 cellSize, float3 cellCount);

    float3 Sample(float3 tc) const;
    float3 Load(float3 wholes) const;
    size_t MakeIndex(size_t x, size_t y, size_t z) const;

    LCG lcg;
    float3 cellSize;
    float3 cellCount;
    std::vector<float3> cells;
};