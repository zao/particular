#include <Windows.h>
#pragma comment(lib,"winmm.lib")
#include "particular/Particular.h"
#include "particular/OpenGLIncludes.h"
#include "util/OpenGLHelpers.h"
#include "util/RTC.h"
#include "util/SlurpFiles.h"
#include "util/Threading.h"
#include "ParticleShadowMap.h"
#include "DebugLineSet.h"

#include <array>
#include <atomic>
#include <complex>
#include <future>
#include <memory>
#include <numeric>
#include <random>
#include <set>
#include <stack>
#include <thread>

#include <MathGeoLib/MathGeoLib.h>
#include <kiss_fft.h>
#include <waveout.h>
#include <ks.h>
#include <ksmedia.h>

#include "stb_image.h"
#include "stb_image_resize.h"

float SpawnRateLowBound = 20000.0f;
float SpawnRateHighBound = 20000.0f;

thread_local GL3WContext* g_gl3wContext;

GL3WContext* GetCurrentGL3WContext() {
	return g_gl3wContext;
}

void MakeGL3WContextCurrent(GL3WContext* ctx) {
	g_gl3wContext = ctx;
}

namespace particular {
	struct Particles {
		std::vector<float3> positions;
		std::vector<float3> velocities;
		std::vector<float> ages;
		std::vector<float4> colors;

		size_t Add(float3 position, float3 velocity, float age, float4 color) {
			positions.push_back(position);
			velocities.push_back(velocity);
			ages.push_back(age);
			colors.push_back(color);
			return positions.size();
		}

		float createAccum{0.0f};
	};

    struct ParticleBuffers {
        util::ResourcePtr position, velocity, color, age;
    };

	struct InstanceImpl : Instance {
		explicit InstanceImpl(Host* host);
		~InstanceImpl();

        void RunFrame() override;
		void RunAudio(float* samples, size_t count) override;

	private:
		void RenderMain();
		void InitGL();
		void TerminateGL();

		void MakeParticle();
		void StepParticles(std::vector<kiss_fft_cpx> const& fftData);
        void InstanceImpl::SortParticlesIntoBuffers(Frustum const& frustum, ParticleBuffers& buffers);

		Host* host;

		float const dt = 1.0f / 120.0f;
		LCG particleLCG{9001u};
		Particles particles;

		std::vector<std::shared_ptr<threading::Worker>> workers;
		threading::WorkQueue workQueue;

        struct Resources {
            util::ResourcePtr vao;
            std::unique_ptr<ParticleShadowMap> psm;
            ParticleBuffers particleBuffers;

            util::ResourcePtr pointProgram, psmProgram, resolveProgram;
            util::ResourcePtr particleDiffuseTexture, particleNormalTexture;

            util::ResourcePtr hdrFrameBuffer, hdrColorTexture, hdrDepthTexture;
        } resources;

        struct PCMSource {
            std::vector<float2> samples;
        } pcm;

		struct PlaybackState {
			size_t nextSample = 0;
		} playbackState;

        struct Spectrum {
            explicit Spectrum(size_t width)
                : fftWidth(width)
                , fftConfig(kiss_fft_alloc(fftWidth, 0, nullptr, nullptr), &kiss_fft_free)
                , hannWindow(width)
                , fftData(width)
            {
                for (size_t i = 0; i < fftWidth; ++i) {
                    hannWindow[i] = (1.0f - std::cos((2.0f * pi * static_cast<float>(i)) / (fftWidth - 1.0f))) / 2.0f;
                };
            }

            size_t fftWidth;
            std::vector<float> hannWindow;
            std::shared_ptr<kiss_fft_state> fftConfig;
            std::vector<kiss_fft_cpx> fftData;
        };
        std::unique_ptr<Spectrum> spectrum;

        std::unique_ptr<DebugLineSet> debugLines;

        double baseTime;
        double lastTime;
		int lastClientWidth{}, lastClientHeight{};

		std::shared_ptr<void> destroyGL;
        GL3WContext gl3wContext;
	};

	std::unique_ptr<Instance> CreateInstance(Host* host) {
		return std::make_unique<InstanceImpl>(host);
	}

	InstanceImpl::InstanceImpl(Host* host)
		: host(host)
	{
        InitGL();
		destroyGL.reset((void*)nullptr, [this](void*){ TerminateGL(); });
        threading::SetThreadName(GetCurrentThreadId(), "RenderMain");

        for (size_t i = 0; i < std::thread::hardware_concurrency(); ++i) {
            auto worker = std::make_shared<threading::Worker>();
            worker->handle = std::make_unique<std::thread>([&, worker] {
                while (!worker->terminate) {
                    threading::WorkQueue::Fun op;
                    {
                        std::unique_lock<std::mutex> lk(workQueue.mutex);
                        workQueue.wake.wait(lk, [&]{ return worker->terminate || workQueue.work.size(); });
                        if (workQueue.work.size()) {
                            op = std::move(workQueue.work.front());
                            workQueue.work.pop_front();
                        }
                    }
                    if (op) {
                        op();
                    }
                }
            });
            workers.push_back(worker);
        }
        resources.vao = util::CreateVertexArray();
        glBindVertexArray(*resources.vao);

        {
            HANDLE fh = CreateFile(L"C:\\Temp\\track.dat", GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
            DWORD _{};
            DWORD cb = GetFileSize(fh, &_);
            DWORD offset = sizeof(float2) * 44100 * 20;
            cb -= offset;
            pcm.samples.resize(cb / sizeof(float2));
            SetFilePointer(fh, offset, nullptr, FILE_BEGIN);
            ReadFile(fh, pcm.samples.data(), cb, &_, nullptr);
            CloseHandle(fh);
        }

        resources.psm = std::make_unique<ParticleShadowMap>(256, 256, 256);
        spectrum = std::make_unique<Spectrum>(8192);

        for (size_t i = 0; i < 100; ++i) {
            MakeParticle();
        }

        auto MakeArrayBuffer = []{
            auto ret = util::CreateBuffer();
            glBindBuffer(GL_ARRAY_BUFFER, *ret); // need to bind to classify as array buffer, it seems
            return ret;
        };

        resources.particleBuffers.position = MakeArrayBuffer();
        resources.particleBuffers.velocity = MakeArrayBuffer();
        resources.particleBuffers.age = MakeArrayBuffer();
        resources.particleBuffers.color = MakeArrayBuffer();

        while (1) {
            auto pointVSSource = util::SlurpTextFile("assets\\pointVS.glsl");
            auto pointFSSource = util::SlurpTextFile("assets\\pointFS.glsl");
            auto pointVS = util::CompileShader(GL_VERTEX_SHADER, pointVSSource);
            auto pointFS = util::CompileShader(GL_FRAGMENT_SHADER, pointFSSource);
            auto pointLink = util::LinkProgram({pointVS.id, pointFS.id});
            OutputDebugStringA("VS:\n"); OutputDebugStringA(pointVS.infoLog.c_str());
            OutputDebugStringA("FS:\n"); OutputDebugStringA(pointFS.infoLog.c_str());
            OutputDebugStringA("Link:\n"); OutputDebugStringA(pointLink.infoLog.c_str());
            resources.pointProgram = pointLink.id;
            if (pointVS.success && pointFS.success && pointLink.success)
                break;
            DebugBreak();
        }

        while (1) {
            auto psmVSSource = util::SlurpTextFile("assets\\psmVS.glsl");
            auto psmGSSource = util::SlurpTextFile("assets\\psmGS.glsl");
            auto psmFSSource = util::SlurpTextFile("assets\\psmFS.glsl");
            auto vs = util::CompileShader(GL_VERTEX_SHADER, psmVSSource);
            auto gs = util::CompileShader(GL_GEOMETRY_SHADER, psmGSSource);
            auto fs = util::CompileShader(GL_FRAGMENT_SHADER, psmFSSource);
            auto link = util::LinkProgram({vs.id, gs.id, fs.id}, util::LinkFlags_EnumerateUniforms);
            OutputDebugStringA("VS:\n"); OutputDebugStringA(vs.infoLog.c_str());
            OutputDebugStringA("GS:\n"); OutputDebugStringA(gs.infoLog.c_str());
            OutputDebugStringA("FS:\n"); OutputDebugStringA(fs.infoLog.c_str());
            OutputDebugStringA("Link:\n"); OutputDebugStringA(link.infoLog.c_str());
            resources.psmProgram = link.id;
            if (vs.success && gs.success && fs.success && link.success)
                break;
            DebugBreak();
        }

        while (1) {
            auto fullTriVSSource = util::SlurpTextFile("assets\\fullTriVS.glsl");
            auto resolveFSSource = util::SlurpTextFile("assets\\resolveFS.glsl");
            auto vsCompile = util::CompileShader(GL_VERTEX_SHADER, fullTriVSSource);
            auto fsCompile = util::CompileShader(GL_FRAGMENT_SHADER, resolveFSSource);
            auto link = util::LinkProgram({vsCompile.id, fsCompile.id});
            OutputDebugStringA("VS:\n"); OutputDebugStringA(vsCompile.infoLog.c_str());
            OutputDebugStringA("FS:\n"); OutputDebugStringA(fsCompile.infoLog.c_str());
            OutputDebugStringA("Link:\n"); OutputDebugStringA(link.infoLog.c_str());
            resources.resolveProgram = link.id;
            if (vsCompile.success && fsCompile.success && link.success)
                break;
            DebugBreak();
        }

        {
            util::ResourcePtr diffuseTexture = util::CreateTexture(GL_TEXTURE_2D);
            util::ResourcePtr normalTexture = util::CreateTexture(GL_TEXTURE_2D);
            int w{}, h{}, comp{};
            int normalW{}, normalH{}, normalComp{};
            auto* data = stbi_load("assets\\aqLq8mu.png", &w, &h, &comp, 4);
            auto* normalData = stbi_load("assets\\lSl59ob.png", &normalW, &normalH, &normalComp, 3);
            assert(w == normalW && h == normalH);
#define PC_SYNTHESISE_ALPHA 1
#if PC_SYNTHESISE_ALPHA
            for (int row = 0; row < h; ++row) {
                size_t rowBase = row*w*4;
                for (int col = 0; col < w; ++col) {
                    auto* p = data + rowBase + col*4;
                    p[3] = p[0];
                }
            }
#endif
            DWORD mipCount{};
            _BitScanReverse(&mipCount, static_cast<DWORD>(w));
            mipCount += 1;

            GLint unpackAlignment{};
            glGetIntegerv(GL_UNPACK_ALIGNMENT, &unpackAlignment);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glTextureStorage2D(*diffuseTexture, mipCount, GL_RGBA8, w, h);
            glTextureStorage2D(*normalTexture, mipCount, GL_RGB8, w, h);
            glTextureSubImage2D(*diffuseTexture, 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, data);
            glTextureSubImage2D(*normalTexture, 0, 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, normalData);
            glPixelStorei(GL_UNPACK_ALIGNMENT, unpackAlignment);
            stbi_image_free(data);
            stbi_image_free(normalData);
            glGenerateTextureMipmap(*diffuseTexture);
            glGenerateTextureMipmap(*normalTexture);
            glTextureParameteri(*diffuseTexture, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTextureParameteri(*diffuseTexture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTextureParameteri(*diffuseTexture, GL_TEXTURE_MAX_LEVEL, mipCount);
            glTextureParameteri(*normalTexture, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTextureParameteri(*normalTexture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTextureParameteri(*normalTexture, GL_TEXTURE_MAX_LEVEL, mipCount);

            resources.particleDiffuseTexture = diffuseTexture;
            resources.particleNormalTexture = normalTexture;
        }

        resources.hdrFrameBuffer = util::CreateFramebuffer();

        debugLines = std::make_unique<DebugLineSet>();

        baseTime = host->GetWallTime();
        lastTime = baseTime;
	}

	InstanceImpl::~InstanceImpl() {
        for (auto& worker : workers) {
            worker->terminate = true;
        }
        {
            std::unique_lock<std::mutex> lk(workQueue.mutex);
            workQueue.wake.notify_all();
        }
        for (auto& worker : workers) {
            worker->handle->join();
        }
		resources = {};
		debugLines.reset();
	}

	void InstanceImpl::InitGL() {
		gl3wInit(&gl3wContext);
		MakeGL3WContextCurrent(&gl3wContext);
	}

	void InstanceImpl::TerminateGL() {
		MakeGL3WContextCurrent(nullptr);
	}

	struct EraseBatch {
		EraseBatch(Particles& particles)
			: particles(particles)
		{}

		~EraseBatch() {
			Commit();
		}

		EraseBatch(EraseBatch const&) = delete;
		EraseBatch& operator = (EraseBatch const&) = delete;

		void Commit() {
			assert(particles.positions.size() == particles.velocities.size() &&
				particles.positions.size() == particles.ages.size() &&
				particles.positions.size() == particles.colors.size());
			if (eraseIndices.empty()) return;

			size_t last = particles.positions.size() - 1;
			for (size_t i : eraseIndices) {
				particles.positions[i] = particles.positions[last];
				particles.velocities[i] = particles.velocities[last];
				particles.ages[i] = particles.ages[last];
				particles.colors[i] = particles.colors[last];
				--last;
			}
			particles.positions.resize(last+1);
			particles.velocities.resize(last+1);
			particles.ages.resize(last+1);
			particles.colors.resize(last+1);
			eraseIndices.clear();
		}

		void Erase(size_t i) {
			eraseIndices.insert(eraseIndices.end(), i);
		}

		Particles& particles;
		std::set<size_t, std::greater<size_t>> eraseIndices;
	};

	void InstanceImpl::MakeParticle() {
#define PC_GENERATE_EXPLOSION 1
#define PC_GENERATE_SPHERE 0
#define PC_GENERATE_LUMPS 0
#define PC_GENERATE_PLANE 0
        float3 p = float3::zero;
        float3 v = float3::zero;

#if PC_GENERATE_EXPLOSION
        p = float3::RandomSphere(particleLCG, float3::zero, 1.0f) * 0.1f;
        v = float3::RandomDir(particleLCG, particleLCG.FloatIncl(0.2f, 0.4f));
#endif
#if PC_GENERATE_SPHERE
        while (1) {
            float3 candidate = float3::RandomBox(particleLCG, -1.0, 1.0);
            if (candidate.LengthSq() <= 0.3f) {
                p = candidate;
                break;
            }
        }
#endif
#if PC_GENERATE_LUMPS
        float z = Lerp(0.0f, 1.0f, particleLCG.Int(0,3)/4.0f);
        p = float3::RandomBox(particleLCG, -0.3f, 0.3f);
        p[2] = z;
#endif
#if PC_GENERATE_PLANE
		p = float3::RandomBox(particleLCG, -1.0f, 1.0f);
		p[1] = 0.0f;
#endif
		float age = 0.0f;
		float4 color = float4::zero;
		particles.Add(p, v, age, color);
	}

	void InstanceImpl::StepParticles(std::vector<kiss_fft_cpx> const& fftData) {
		// Sim particles
		float3 gravity{0.0f, -9.82f, 0.0f};
		size_t const n = particles.positions.size();
		size_t partitionSize = n / workers.size();
        threading::WorkSet workSet(workers.size());
		for (size_t workId = 0; workId < workers.size(); ++workId) {
			size_t lowBound = workId * partitionSize;
			size_t highBound = (workId+1) * partitionSize;
			if (workId == workers.size() - 1) {
				highBound = n;
			}
            threading::WorkQueue::Fun op = [this, &gravity, &fftData, &workSet, lowBound, highBound, workId]{
				for (size_t i = lowBound; i < highBound; ++i) {
					auto& p = particles.positions[i];
					auto& v = particles.velocities[i];
					auto& age = particles.ages[i];
					auto& color = particles.colors[i];

					float3 center = float3::zero;
					float3 a = float3::zero;
#define PC_BOUNCE_PARTICLES 0
#define PC_CLAMP_PARTICLES_TO_ZERO 0
#define PC_SIMULATE_GRAVITY 0
#if PC_BOUNCE_PARTICLES
					if (p[1] <= 0.0f) {
                        if (particleLCG.Float01Incl() < 0.05f) {
						    auto idx = static_cast<size_t>(Lerp(fftWidth / 128.0f, fftWidth / 2.0f - 1.0f, Log2(1.0f + Clamp01((p[0] + 1.0f) / 2.0f))));
						    float2 vec(fftData[idx].r, fftData[idx].i);
						    vec.ScaleToLength(std::log(1.0f + vec.Length()) * 100.0f);
						    a += float3::unitY * vec.Length();
                            float2 fuzz = float2::RandomDir(particleLCG);
                            a += float3(fuzz[0], 0.0f, fuzz[1]);
						    p[1] = 0.0f;
						    v[1] = 0.0f;
                        }
					}
#endif
#if PC_SIMULATE_GRAVITY
					a += gravity;
#endif
					v += a * dt;
					p += v * dt;
#if PC_CLAMP_PARTICLES_TO_ZERO
					p[1] = Max(p[1], 0.0f);
#endif
					age += dt;
					color = float4(float3(0.1f, 0.0f, 0.0f) + p.Mul(float3(0.0f, 5.0f, 5.0f)).Abs(), 1.0f);
                    color = float4(float3(0.1f, 1.0f, 1.0f), 0.1f);
				}
				workSet.Complete(workId);
			};
            workQueue.Enqueue(std::move(op));
		}
        workSet.Wait();

		// Kill particles
		{
			EraseBatch batch(particles);
			for (size_t i = 0; i < particles.positions.size(); ++i) {
				if (particles.ages[i] > 5.0f) {
					batch.Erase(i);
				}
			}
			batch.Commit();
		}

		// Spawn particles
		particles.createAccum += particleLCG.FloatIncl(SpawnRateLowBound * dt, SpawnRateHighBound * dt);
		float newCount{};
		particles.createAccum = std::modf(particles.createAccum, &newCount);
		for (float i = 0.0f; i < newCount; ++i) {
			MakeParticle();
		}
	}

    void InstanceImpl::SortParticlesIntoBuffers(Frustum const& frustum, ParticleBuffers& buffers) {
        size_t const particleCount = particles.positions.size();
        float3x4 V = frustum.ComputeViewMatrix();
        std::vector<size_t> indices(particleCount);
        std::iota(indices.begin(), indices.end(), 0u);
#define PC_SORT_PARTICLES 1
#if PC_SORT_PARTICLES
        std::vector<float> depths(particleCount);
        for (size_t i = 0; i < particleCount; ++i) {
            depths[i] = math::Dot(V.Row(2), float4(particles.positions[i], 1.0));
        }
        std::sort(indices.begin(), indices.end(), [&](size_t a, size_t b) {
            return depths[a] > depths[b];
        });
#endif
        size_t cb1 = particleCount * sizeof(float);
        size_t cb3 = particleCount * sizeof(float3);
        size_t cb4 = particleCount * sizeof(float4);
        std::vector<char> posBuf(cb3);
        std::vector<char> velBuf(cb3);
        std::vector<char> ageBuf(cb1);
        std::vector<char> colBuf(cb4);

        threading::WorkSet workSet(4);
        workQueue.Enqueue([&]{
            char* p = posBuf.data();
            for (auto idx : indices) {
                memcpy(p, particles.positions[idx].ptr(), sizeof(float3));
                p += sizeof(float3);
            }
            workSet.Complete(0);
        });
        workQueue.Enqueue([&]{
            char* p = velBuf.data();
            for (auto idx : indices) {
                memcpy(p, particles.velocities[idx].ptr(), sizeof(float3));
                p += sizeof(float3);
            }
            workSet.Complete(1);
        });
        workQueue.Enqueue([&]{
            char* p = ageBuf.data();
            for (auto idx : indices) {
                memcpy(p, &particles.ages[idx], sizeof(float));
                p += sizeof(float);
            }
            workSet.Complete(2);
        });
        workQueue.Enqueue([&]{
            char* p = colBuf.data();
            for (auto idx : indices) {
                memcpy(p, particles.colors[idx].ptr(), sizeof(float4));
                p += sizeof(float4);
            }
            workSet.Complete(3);
        });
        workSet.Wait();

        glNamedBufferData(*buffers.position, cb3, posBuf.data(), GL_STATIC_DRAW);
        glNamedBufferData(*buffers.velocity, cb3, velBuf.data(), GL_STATIC_DRAW);
        glNamedBufferData(*buffers.age, cb1, ageBuf.data(), GL_STATIC_DRAW);
        glNamedBufferData(*buffers.color, cb4, colBuf.data(), GL_STATIC_DRAW);
    }


	void InstanceImpl::RunAudio(float* samples, size_t count) {
#define PC_AUDIO 1
#if PC_AUDIO
		auto& nextSample = playbackState.nextSample;
		size_t wantedCount = count;
		size_t firstSlice = Min(wantedCount, pcm.samples.size() - nextSample);
		size_t secondSlice = wantedCount - firstSlice;
		memcpy(samples, pcm.samples[nextSample].ptr(), firstSlice * sizeof(float2));
		nextSample += firstSlice;
		if (secondSlice) {
			memcpy(samples + firstSlice * 2, pcm.samples[0].ptr(), secondSlice * sizeof(float2));
			nextSample = secondSlice;
		}
#else
		memset(samples, 0, sizeof(float) * count * 2);
#endif
	}

    void InstanceImpl::RunFrame() {
        debugLines->Clear();
        double currentTime = host->GetWallTime();
        // Update phase
        while (lastTime + dt <= currentTime) {
            {
                // Update spectrum
                size_t nSamples = pcm.samples.size();
                size_t currentSample = static_cast<size_t>(currentTime * 44100.0f) % nSamples;
                for (size_t dstIdx = 0; dstIdx < spectrum->fftWidth; ++dstIdx) {
                    size_t srcIdx = (currentSample + dstIdx + nSamples - spectrum->fftWidth/2) % nSamples;
                    spectrum->fftData[dstIdx].r = pcm.samples[srcIdx].x * spectrum->hannWindow[dstIdx];
                    spectrum->fftData[dstIdx].i = 0.0f;
                }
                kiss_fft(spectrum->fftConfig.get(), spectrum->fftData.data(), spectrum->fftData.data());

                StepParticles(spectrum->fftData);
            }
            lastTime += dt;
        }

        // Draw phase
		int clientWidth{}, clientHeight{};
		host->GetSize(&clientWidth, &clientHeight);
        do {
			if (clientWidth == 0 || clientHeight == 0) return;
			if (clientWidth != lastClientWidth || clientHeight != lastClientHeight) {
                {
                    GLint prevFBO{};
                    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &prevFBO);
                    resources.hdrColorTexture = util::CreateTexture(GL_TEXTURE_2D);
                    resources.hdrDepthTexture = util::CreateTexture(GL_TEXTURE_2D);
                    glBindFramebuffer(GL_FRAMEBUFFER, *resources.hdrFrameBuffer);
                    glBindFramebuffer(GL_FRAMEBUFFER, prevFBO);
                    glTextureParameteri(*resources.hdrColorTexture, GL_TEXTURE_MAX_LEVEL, 0);
                    glTextureParameteri(*resources.hdrColorTexture, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                    glTextureParameteri(*resources.hdrColorTexture, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                    glTextureParameteri(*resources.hdrDepthTexture, GL_TEXTURE_MAX_LEVEL, 0);
                    glTextureParameteri(*resources.hdrDepthTexture, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                    glTextureParameteri(*resources.hdrDepthTexture, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                    glTextureStorage2D(*resources.hdrColorTexture, 1, GL_RGBA16F, clientWidth, clientHeight);
                    glTextureStorage2D(*resources.hdrDepthTexture, 1, GL_DEPTH_COMPONENT32F, clientWidth, clientHeight);
                    glNamedFramebufferTexture(*resources.hdrFrameBuffer, GL_COLOR_ATTACHMENT0, *resources.hdrColorTexture, 0);
                    glNamedFramebufferTexture(*resources.hdrFrameBuffer, GL_DEPTH_ATTACHMENT, *resources.hdrDepthTexture, 0);
                    auto status = glCheckNamedFramebufferStatus(*resources.hdrFrameBuffer, GL_DRAW_FRAMEBUFFER);
                    GLenum err{};
                    switch (status) {
                    case GL_FRAMEBUFFER_COMPLETE: break;
                    case 0: err = glGetError(); break;
                    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                    case GL_FRAMEBUFFER_UNSUPPORTED:
                    case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
                    case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
                    default:
                        abort(); break;
                    }
                }
                glViewport(0, 0, clientWidth, clientHeight);
				lastClientWidth = clientWidth;
				lastClientHeight = clientHeight;
            }
        } while (0);
        GLint prevFBO{};
        glGetIntegerv(GL_FRAMEBUFFER_BINDING, &prevFBO);

        // Prepare camera
        Frustum frustum;
        float aspectRatio = static_cast<float>(clientWidth) / static_cast<float>(clientHeight);
        frustum.SetHorizontalFovAndAspectRatio(pi/1.4f, aspectRatio);
        frustum.SetKind(math::FrustumSpaceGL, math::FrustumLeftHanded);
        frustum.SetViewPlaneDistances(0.1f, 1000.0f);
        float3x4 camWorld = float3x4::RotateY(static_cast<float>(0.3 * currentTime)) *
            float3x4::Translate(0.0f, 0.4f, -2.6f);
        frustum.SetWorldMatrix(camWorld);

        // Prepare light
        Frustum spot;
        float spotAngle = pi/4.4f;
        spot.SetHorizontalFovAndAspectRatio(spotAngle, 1.0f);
        spot.SetKind(math::FrustumSpaceGL, math::FrustumLeftHanded);
        spot.SetViewPlaneDistances(0.1f, 3.0f);
        float3x4 spotWorld = float3x4::LookAt(
            float3(0.5f, 0.5f, -1.5f),
            //float3(0.0f, 0.0f, -1.0f),
            float3::zero,
            +float3::unitZ,
            +float3::unitY,
            +float3::unitY);
        spot.SetWorldMatrix(spotWorld);

        // Update display particles
        size_t const particleCount = particles.positions.size();

        // Sort particles into buffers
        SortParticlesIntoBuffers(frustum, resources.particleBuffers);

        auto DrawParticles = [&](size_t particleCount, GLuint program, Frustum view) {
            auto& vao = resources.vao;
            auto& particleBuffers = resources.particleBuffers;
            auto& particleDiffuseTexture = resources.particleDiffuseTexture;
            auto& particleNormalTexture = resources.particleNormalTexture;
            glUseProgram(program);
            float4x4 V = view.ComputeViewMatrix();
            float4x4 P = view.ComputeProjectionMatrix();
            float4x4 VP = P*V;
            float4x4 MV = V;
            float4x4 MVP = VP;
            glUniformMatrix4fv(glGetUniformLocation(program, "u_MVP"), 1, GL_FALSE, MVP.Transposed().ptr());
            glUniformMatrix4fv(glGetUniformLocation(program, "u_MV"), 1, GL_FALSE, MV.Transposed().ptr());
            glUniformMatrix4fv(glGetUniformLocation(program, "u_V"), 1, GL_FALSE, V.Transposed().ptr());
            glUniformMatrix4fv(glGetUniformLocation(program, "u_P"), 1, GL_FALSE, P.Transposed().ptr());
            glVertexArrayVertexBuffer(*vao, 0, *particleBuffers.position, 0, sizeof(float3));
            glVertexArrayVertexBuffer(*vao, 1, *particleBuffers.velocity, 0, sizeof(float3));
            glVertexArrayVertexBuffer(*vao, 2, *particleBuffers.age, 0, sizeof(float));
            glVertexArrayVertexBuffer(*vao, 3, *particleBuffers.color, 0, sizeof(float4));
            glVertexArrayAttribBinding(*vao, 0, 0);
            glVertexArrayAttribBinding(*vao, 1, 1);
            glVertexArrayAttribBinding(*vao, 2, 2);
            glVertexArrayAttribBinding(*vao, 3, 3);
            glVertexArrayAttribFormat(*vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
            glVertexArrayAttribFormat(*vao, 1, 3, GL_FLOAT, GL_FALSE, 0);
            glVertexArrayAttribFormat(*vao, 2, 1, GL_FLOAT, GL_FALSE, 0);
            glVertexArrayAttribFormat(*vao, 3, 4, GL_FLOAT, GL_FALSE, 0);
            glVertexArrayBindingDivisor(*vao, 0, 1);
            glVertexArrayBindingDivisor(*vao, 1, 1);
            glVertexArrayBindingDivisor(*vao, 2, 1);
            glVertexArrayBindingDivisor(*vao, 3, 1);
            glEnableVertexArrayAttrib(*vao, 0);
            glEnableVertexArrayAttrib(*vao, 1);
            glEnableVertexArrayAttrib(*vao, 2);
            glEnableVertexArrayAttrib(*vao, 3);
            GLuint textures[] = { *particleDiffuseTexture, *particleNormalTexture };
            glBindTextures(0, 2, textures);
            glUniform1i(glGetUniformLocation(program, "u_DiffuseTex"), 0);
            glUniform1i(glGetUniformLocation(program, "u_NormalTex"), 1);
            glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, 6, particleCount);
        };

        // Generate PSM
        {
            auto& psm = resources.psm;
            auto& psmProgram = resources.psmProgram;
            psm->Clear();
            psm->BindAsTarget();
            glViewport(0, 0, psm->width, psm->height);
            glDisable(GL_DEPTH_TEST);
            glEnable(GL_BLEND);
            glBlendFuncSeparate(GL_DST_COLOR, GL_ZERO, GL_DST_ALPHA, GL_ZERO);
            glProgramUniform1i(*psmProgram, glGetUniformLocation(*psmProgram, "u_SliceCount"), psm->depth);
            float nearZ = spot.NearPlaneDistance();
            float farZ = spot.FarPlaneDistance();
            glProgramUniform1f(*psmProgram, glGetUniformLocation(*psmProgram, "u_NearZ"), nearZ);
            glProgramUniform1f(*psmProgram, glGetUniformLocation(*psmProgram, "u_FarZ"), farZ);
            DrawParticles(particleCount, *psmProgram, spot);
#define PC_SAVE_TILES 0
#if PC_SAVE_TILES
            psm->SaveTiled("C:\\Temp\\psm\\volume");
#endif
            psm->Propagate();
#if PC_SAVE_TILES
            psm->SaveTiled("C:\\Temp\\psm\\map");
#endif
        }

        // Draw to HDR target
        glBindFramebuffer(GL_FRAMEBUFFER, *resources.hdrFrameBuffer);
        glViewport(0, 0, clientWidth, clientHeight);
        float4 clearColor(0.1f, 0.2f, 0.3f, 1.0f);
		glClearBufferfv(GL_COLOR, 0, clearColor.ptr());
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0u);
        {
            auto& psm = resources.psm;
            auto& pointProgram = resources.pointProgram;
            glDisable(GL_DEPTH_TEST);
            glEnable(GL_BLEND);
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
            glProgramUniform4fv(*pointProgram, glGetUniformLocation(*pointProgram, "u_LightPositionW"), 1, float4(spot.Pos(), 1.0f).ptr());
            float4x4 spotV = spot.ComputeViewMatrix();
            glProgramUniformMatrix4fv(*pointProgram, glGetUniformLocation(*pointProgram, "u_SpotV"), 1, GL_FALSE, spotV.Transposed().ptr());
            float4x4 spotP = spot.ComputeProjectionMatrix();
            glProgramUniformMatrix4fv(*pointProgram, glGetUniformLocation(*pointProgram, "u_SpotP"), 1, GL_FALSE, spotP.Transposed().ptr());
            float4x4 invV = frustum.WorldMatrix();
            glProgramUniformMatrix4fv(*pointProgram, glGetUniformLocation(*pointProgram, "u_InvV"), 1, GL_FALSE, invV.Transposed().ptr());
            psm->BindAsTexture(2);
            glProgramUniform1i(*pointProgram, glGetUniformLocation(*pointProgram, "u_PSMTex"), 2);
            float nearZ = spot.NearPlaneDistance();
            float farZ = spot.FarPlaneDistance();
            glProgramUniform1f(*pointProgram, glGetUniformLocation(*pointProgram, "u_PSMNearZ"), nearZ);
            glProgramUniform1f(*pointProgram, glGetUniformLocation(*pointProgram, "u_PSMFarZ"), farZ);
            float spotCos = cosf(spot.HorizontalFov() / 2.0f);
            glProgramUniform1f(*pointProgram, glGetUniformLocation(*pointProgram, "u_SpotCos"), spotCos);
            glProgramUniform1f(*pointProgram, glGetUniformLocation(*pointProgram, "u_SpotConstantF"), 0.1f);
            glProgramUniform1f(*pointProgram, glGetUniformLocation(*pointProgram, "u_SpotLinearF"), 0.0f);
            glProgramUniform1f(*pointProgram, glGetUniformLocation(*pointProgram, "u_SpotQuadraticF"), 1.0f);
            float u_Meh = Lerp(0.0f, 1.0f, fmodf(float(currentTime), 1.0f));
            glProgramUniform1f(*pointProgram, glGetUniformLocation(*pointProgram, "u_Meh"), u_Meh);
            debugLines->Add(float3::unitY, float3::unitY + float3::unitX * u_Meh, float4::one, float4::one);
            DrawParticles(particleCount, *pointProgram, frustum);
        }

        {
            auto& resolveProgram = resources.resolveProgram;
            // Resolve HDR target to framebuffer
            glBindFramebuffer(GL_FRAMEBUFFER, prevFBO);
            clearColor = float4::zero;
            glDisable(GL_BLEND);
            glClearBufferfv(GL_COLOR, 0, clearColor.ptr());
            glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0u);
            glUseProgram(*resources.resolveProgram);
            glBindTextures(0, 1, resources.hdrColorTexture.get());
            glUniform1i(glGetUniformLocation(*resolveProgram, "u_ColorTex"), 0);
            glDrawArrays(GL_TRIANGLES, 0, 3);
        }

        // Draw debug lines for spot frustum
        debugLines->Add(float3::zero, float3::unitX, float4(1.0f, 0.0f, 0.0f, 1.0f), float4(1.0f, 0.0f, 0.0f, 1.0f));
        debugLines->Add(float3::zero, float3::unitY, float4(0.0f, 1.0f, 0.0f, 1.0f), float4(0.0f, 1.0f, 0.0f, 1.0f));
        debugLines->Add(float3::zero, float3::unitZ, float4(0.0f, 0.0f, 1.0f, 1.0f), float4(0.0f, 0.0f, 1.0f, 1.0f));
        for (size_t i = 0; i < 12; ++i) {
            math::LineSegment edge = spot.Edge(i);
            debugLines->Add(edge.a, edge.b, float4(float3::one / 2.0f, 1.0f), float4::one);
        }
        //debugLines->Draw(frustum);
    }

	void InstanceImpl::RenderMain() {
	}
}