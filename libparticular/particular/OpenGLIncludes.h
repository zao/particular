#pragma once

#define GL3W_CONTEXT_METHOD ::GetCurrentGL3WContext()
#include <GL/gl3w.h>

GL3WContext* GetCurrentGL3WContext();