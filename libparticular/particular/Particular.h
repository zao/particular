#pragma once

#pragma comment(lib,"opengl32.lib")
#if defined(_DEBUG)
#pragma comment(lib,"MathGeoLibd.lib")
#else
#pragma comment(lib,"MathGeoLib.lib")
#endif


#include <memory>

namespace particular {
	struct Instance {
		virtual ~Instance() {}
        virtual void RunFrame() = 0;
		virtual void RunAudio(float* samples, size_t count) = 0;
	};

	struct Host {
		void* wnd;
		void GetSize(int* width, int* height);
		double GetWallTime();
	};

	std::unique_ptr<Instance> CreateInstance(Host* host);
}