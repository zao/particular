/*

    This file was generated with gl3w_gen.py, part of gl3w
    (hosted at https://github.com/skaslev/gl3w)

    This is free and unencumbered software released into the public domain.

    Anyone is free to copy, modify, publish, use, compile, sell, or
    distribute this software, either in source code form or as a compiled
    binary, for any purpose, commercial or non-commercial, and by any
    means.

    In jurisdictions that recognize copyright laws, the author or authors
    of this software dedicate any and all copyright interest in the
    software to the public domain. We make this dedication for the benefit
    of the public at large and to the detriment of our heirs and
    successors. We intend this dedication to be an overt act of
    relinquishment in perpetuity of all present and future rights to this
    software under copyright law.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
    OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.

*/

#include <GL/gl3w.h>

#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

static HMODULE libgl;

static void libgl_open(void) {
    libgl = LoadLibraryA("opengl32.dll");
}

static void libgl_close(void) {
    FreeLibrary(libgl);
}

static GL3WglProc libgl_sym(const char *proc) {
    GL3WglProc res;

    res = (GL3WglProc)wglGetProcAddress(proc);
    if (!res)
        res = (GL3WglProc)GetProcAddress(libgl, proc);
    return res;
}

#elif defined(__APPLE__) || defined(__APPLE_CC__)

#include <Carbon/Carbon.h>

static CFBundleRef bundle;
static CFURLRef bundleURL;

static void libgl_open(void) {
    bundleURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault,
        CFSTR("/System/Library/Frameworks/OpenGL.framework"),
        kCFURLPOSIXPathStyle, true);

    bundle = CFBundleCreate(kCFAllocatorDefault, bundleURL);
    assert(bundle != NULL);
}

static void libgl_close(void) {
    CFRelease(bundle);
    CFRelease(bundleURL);
}

static GL3WglProc libgl_sym(const char *proc) {
    GL3WglProc res;

    CFStringRef procName = CFStringCreateWithCString(kCFAllocatorDefault, proc,
        kCFStringEncodingASCII);
    res = (GL3WglProc)CFBundleGetFunctionPointerForName(bundle, procName);
    CFRelease(procName);
    return res;
}

#else

#include <dlfcn.h>
#include <GL/glx.h>

static void *libgl;

static void libgl_open(void) {
    libgl = dlopen("libGL.so.1", RTLD_LAZY | RTLD_GLOBAL);
}

static void libgl_close(void) {
    dlclose(libgl);
}

static GL3WglProc libgl_sym(const char *proc) {
    GL3WglProc res;

    res = (GL3WglProc)glXGetProcAddress((const GLubyte *)proc);
    if (!res)
        res = (GL3WglProc)dlsym(libgl, proc);
    return res;
}

#endif


static int setup_context(GL3WContext *p) {
    p->gl3wActiveShaderProgram = (PFNGLACTIVESHADERPROGRAMPROC)libgl_sym("glActiveShaderProgram");
    p->gl3wActiveTexture = (PFNGLACTIVETEXTUREPROC)libgl_sym("glActiveTexture");
    p->gl3wAttachShader = (PFNGLATTACHSHADERPROC)libgl_sym("glAttachShader");
    p->gl3wBeginConditionalRender = (PFNGLBEGINCONDITIONALRENDERPROC)libgl_sym("glBeginConditionalRender");
    p->gl3wBeginQuery = (PFNGLBEGINQUERYPROC)libgl_sym("glBeginQuery");
    p->gl3wBeginQueryIndexed = (PFNGLBEGINQUERYINDEXEDPROC)libgl_sym("glBeginQueryIndexed");
    p->gl3wBeginTransformFeedback = (PFNGLBEGINTRANSFORMFEEDBACKPROC)libgl_sym("glBeginTransformFeedback");
    p->gl3wBindAttribLocation = (PFNGLBINDATTRIBLOCATIONPROC)libgl_sym("glBindAttribLocation");
    p->gl3wBindBuffer = (PFNGLBINDBUFFERPROC)libgl_sym("glBindBuffer");
    p->gl3wBindBufferBase = (PFNGLBINDBUFFERBASEPROC)libgl_sym("glBindBufferBase");
    p->gl3wBindBufferRange = (PFNGLBINDBUFFERRANGEPROC)libgl_sym("glBindBufferRange");
    p->gl3wBindBuffersBase = (PFNGLBINDBUFFERSBASEPROC)libgl_sym("glBindBuffersBase");
    p->gl3wBindBuffersRange = (PFNGLBINDBUFFERSRANGEPROC)libgl_sym("glBindBuffersRange");
    p->gl3wBindFragDataLocation = (PFNGLBINDFRAGDATALOCATIONPROC)libgl_sym("glBindFragDataLocation");
    p->gl3wBindFragDataLocationIndexed = (PFNGLBINDFRAGDATALOCATIONINDEXEDPROC)libgl_sym("glBindFragDataLocationIndexed");
    p->gl3wBindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC)libgl_sym("glBindFramebuffer");
    p->gl3wBindImageTexture = (PFNGLBINDIMAGETEXTUREPROC)libgl_sym("glBindImageTexture");
    p->gl3wBindImageTextures = (PFNGLBINDIMAGETEXTURESPROC)libgl_sym("glBindImageTextures");
    p->gl3wBindProgramPipeline = (PFNGLBINDPROGRAMPIPELINEPROC)libgl_sym("glBindProgramPipeline");
    p->gl3wBindRenderbuffer = (PFNGLBINDRENDERBUFFERPROC)libgl_sym("glBindRenderbuffer");
    p->gl3wBindSampler = (PFNGLBINDSAMPLERPROC)libgl_sym("glBindSampler");
    p->gl3wBindSamplers = (PFNGLBINDSAMPLERSPROC)libgl_sym("glBindSamplers");
    p->gl3wBindTexture = (PFNGLBINDTEXTUREPROC)libgl_sym("glBindTexture");
    p->gl3wBindTextureUnit = (PFNGLBINDTEXTUREUNITPROC)libgl_sym("glBindTextureUnit");
    p->gl3wBindTextures = (PFNGLBINDTEXTURESPROC)libgl_sym("glBindTextures");
    p->gl3wBindTransformFeedback = (PFNGLBINDTRANSFORMFEEDBACKPROC)libgl_sym("glBindTransformFeedback");
    p->gl3wBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)libgl_sym("glBindVertexArray");
    p->gl3wBindVertexBuffer = (PFNGLBINDVERTEXBUFFERPROC)libgl_sym("glBindVertexBuffer");
    p->gl3wBindVertexBuffers = (PFNGLBINDVERTEXBUFFERSPROC)libgl_sym("glBindVertexBuffers");
    p->gl3wBlendColor = (PFNGLBLENDCOLORPROC)libgl_sym("glBlendColor");
    p->gl3wBlendEquation = (PFNGLBLENDEQUATIONPROC)libgl_sym("glBlendEquation");
    p->gl3wBlendEquationSeparate = (PFNGLBLENDEQUATIONSEPARATEPROC)libgl_sym("glBlendEquationSeparate");
    p->gl3wBlendEquationSeparatei = (PFNGLBLENDEQUATIONSEPARATEIPROC)libgl_sym("glBlendEquationSeparatei");
    p->gl3wBlendEquationSeparateiARB = (PFNGLBLENDEQUATIONSEPARATEIARBPROC)libgl_sym("glBlendEquationSeparateiARB");
    p->gl3wBlendEquationi = (PFNGLBLENDEQUATIONIPROC)libgl_sym("glBlendEquationi");
    p->gl3wBlendEquationiARB = (PFNGLBLENDEQUATIONIARBPROC)libgl_sym("glBlendEquationiARB");
    p->gl3wBlendFunc = (PFNGLBLENDFUNCPROC)libgl_sym("glBlendFunc");
    p->gl3wBlendFuncSeparate = (PFNGLBLENDFUNCSEPARATEPROC)libgl_sym("glBlendFuncSeparate");
    p->gl3wBlendFuncSeparatei = (PFNGLBLENDFUNCSEPARATEIPROC)libgl_sym("glBlendFuncSeparatei");
    p->gl3wBlendFuncSeparateiARB = (PFNGLBLENDFUNCSEPARATEIARBPROC)libgl_sym("glBlendFuncSeparateiARB");
    p->gl3wBlendFunci = (PFNGLBLENDFUNCIPROC)libgl_sym("glBlendFunci");
    p->gl3wBlendFunciARB = (PFNGLBLENDFUNCIARBPROC)libgl_sym("glBlendFunciARB");
    p->gl3wBlitFramebuffer = (PFNGLBLITFRAMEBUFFERPROC)libgl_sym("glBlitFramebuffer");
    p->gl3wBlitNamedFramebuffer = (PFNGLBLITNAMEDFRAMEBUFFERPROC)libgl_sym("glBlitNamedFramebuffer");
    p->gl3wBufferData = (PFNGLBUFFERDATAPROC)libgl_sym("glBufferData");
    p->gl3wBufferPageCommitmentARB = (PFNGLBUFFERPAGECOMMITMENTARBPROC)libgl_sym("glBufferPageCommitmentARB");
    p->gl3wBufferStorage = (PFNGLBUFFERSTORAGEPROC)libgl_sym("glBufferStorage");
    p->gl3wBufferSubData = (PFNGLBUFFERSUBDATAPROC)libgl_sym("glBufferSubData");
    p->gl3wCheckFramebufferStatus = (PFNGLCHECKFRAMEBUFFERSTATUSPROC)libgl_sym("glCheckFramebufferStatus");
    p->gl3wCheckNamedFramebufferStatus = (PFNGLCHECKNAMEDFRAMEBUFFERSTATUSPROC)libgl_sym("glCheckNamedFramebufferStatus");
    p->gl3wClampColor = (PFNGLCLAMPCOLORPROC)libgl_sym("glClampColor");
    p->gl3wClear = (PFNGLCLEARPROC)libgl_sym("glClear");
    p->gl3wClearBufferData = (PFNGLCLEARBUFFERDATAPROC)libgl_sym("glClearBufferData");
    p->gl3wClearBufferSubData = (PFNGLCLEARBUFFERSUBDATAPROC)libgl_sym("glClearBufferSubData");
    p->gl3wClearBufferfi = (PFNGLCLEARBUFFERFIPROC)libgl_sym("glClearBufferfi");
    p->gl3wClearBufferfv = (PFNGLCLEARBUFFERFVPROC)libgl_sym("glClearBufferfv");
    p->gl3wClearBufferiv = (PFNGLCLEARBUFFERIVPROC)libgl_sym("glClearBufferiv");
    p->gl3wClearBufferuiv = (PFNGLCLEARBUFFERUIVPROC)libgl_sym("glClearBufferuiv");
    p->gl3wClearColor = (PFNGLCLEARCOLORPROC)libgl_sym("glClearColor");
    p->gl3wClearDepth = (PFNGLCLEARDEPTHPROC)libgl_sym("glClearDepth");
    p->gl3wClearDepthf = (PFNGLCLEARDEPTHFPROC)libgl_sym("glClearDepthf");
    p->gl3wClearNamedBufferData = (PFNGLCLEARNAMEDBUFFERDATAPROC)libgl_sym("glClearNamedBufferData");
    p->gl3wClearNamedBufferSubData = (PFNGLCLEARNAMEDBUFFERSUBDATAPROC)libgl_sym("glClearNamedBufferSubData");
    p->gl3wClearNamedFramebufferfi = (PFNGLCLEARNAMEDFRAMEBUFFERFIPROC)libgl_sym("glClearNamedFramebufferfi");
    p->gl3wClearNamedFramebufferfv = (PFNGLCLEARNAMEDFRAMEBUFFERFVPROC)libgl_sym("glClearNamedFramebufferfv");
    p->gl3wClearNamedFramebufferiv = (PFNGLCLEARNAMEDFRAMEBUFFERIVPROC)libgl_sym("glClearNamedFramebufferiv");
    p->gl3wClearNamedFramebufferuiv = (PFNGLCLEARNAMEDFRAMEBUFFERUIVPROC)libgl_sym("glClearNamedFramebufferuiv");
    p->gl3wClearStencil = (PFNGLCLEARSTENCILPROC)libgl_sym("glClearStencil");
    p->gl3wClearTexImage = (PFNGLCLEARTEXIMAGEPROC)libgl_sym("glClearTexImage");
    p->gl3wClearTexSubImage = (PFNGLCLEARTEXSUBIMAGEPROC)libgl_sym("glClearTexSubImage");
    p->gl3wClientWaitSync = (PFNGLCLIENTWAITSYNCPROC)libgl_sym("glClientWaitSync");
    p->gl3wClipControl = (PFNGLCLIPCONTROLPROC)libgl_sym("glClipControl");
    p->gl3wColorMask = (PFNGLCOLORMASKPROC)libgl_sym("glColorMask");
    p->gl3wColorMaski = (PFNGLCOLORMASKIPROC)libgl_sym("glColorMaski");
    p->gl3wCompileShader = (PFNGLCOMPILESHADERPROC)libgl_sym("glCompileShader");
    p->gl3wCompileShaderIncludeARB = (PFNGLCOMPILESHADERINCLUDEARBPROC)libgl_sym("glCompileShaderIncludeARB");
    p->gl3wCompressedTexImage1D = (PFNGLCOMPRESSEDTEXIMAGE1DPROC)libgl_sym("glCompressedTexImage1D");
    p->gl3wCompressedTexImage2D = (PFNGLCOMPRESSEDTEXIMAGE2DPROC)libgl_sym("glCompressedTexImage2D");
    p->gl3wCompressedTexImage3D = (PFNGLCOMPRESSEDTEXIMAGE3DPROC)libgl_sym("glCompressedTexImage3D");
    p->gl3wCompressedTexSubImage1D = (PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC)libgl_sym("glCompressedTexSubImage1D");
    p->gl3wCompressedTexSubImage2D = (PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC)libgl_sym("glCompressedTexSubImage2D");
    p->gl3wCompressedTexSubImage3D = (PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC)libgl_sym("glCompressedTexSubImage3D");
    p->gl3wCompressedTextureSubImage1D = (PFNGLCOMPRESSEDTEXTURESUBIMAGE1DPROC)libgl_sym("glCompressedTextureSubImage1D");
    p->gl3wCompressedTextureSubImage2D = (PFNGLCOMPRESSEDTEXTURESUBIMAGE2DPROC)libgl_sym("glCompressedTextureSubImage2D");
    p->gl3wCompressedTextureSubImage3D = (PFNGLCOMPRESSEDTEXTURESUBIMAGE3DPROC)libgl_sym("glCompressedTextureSubImage3D");
    p->gl3wCopyBufferSubData = (PFNGLCOPYBUFFERSUBDATAPROC)libgl_sym("glCopyBufferSubData");
    p->gl3wCopyImageSubData = (PFNGLCOPYIMAGESUBDATAPROC)libgl_sym("glCopyImageSubData");
    p->gl3wCopyNamedBufferSubData = (PFNGLCOPYNAMEDBUFFERSUBDATAPROC)libgl_sym("glCopyNamedBufferSubData");
    p->gl3wCopyTexImage1D = (PFNGLCOPYTEXIMAGE1DPROC)libgl_sym("glCopyTexImage1D");
    p->gl3wCopyTexImage2D = (PFNGLCOPYTEXIMAGE2DPROC)libgl_sym("glCopyTexImage2D");
    p->gl3wCopyTexSubImage1D = (PFNGLCOPYTEXSUBIMAGE1DPROC)libgl_sym("glCopyTexSubImage1D");
    p->gl3wCopyTexSubImage2D = (PFNGLCOPYTEXSUBIMAGE2DPROC)libgl_sym("glCopyTexSubImage2D");
    p->gl3wCopyTexSubImage3D = (PFNGLCOPYTEXSUBIMAGE3DPROC)libgl_sym("glCopyTexSubImage3D");
    p->gl3wCopyTextureSubImage1D = (PFNGLCOPYTEXTURESUBIMAGE1DPROC)libgl_sym("glCopyTextureSubImage1D");
    p->gl3wCopyTextureSubImage2D = (PFNGLCOPYTEXTURESUBIMAGE2DPROC)libgl_sym("glCopyTextureSubImage2D");
    p->gl3wCopyTextureSubImage3D = (PFNGLCOPYTEXTURESUBIMAGE3DPROC)libgl_sym("glCopyTextureSubImage3D");
    p->gl3wCreateBuffers = (PFNGLCREATEBUFFERSPROC)libgl_sym("glCreateBuffers");
    p->gl3wCreateFramebuffers = (PFNGLCREATEFRAMEBUFFERSPROC)libgl_sym("glCreateFramebuffers");
    p->gl3wCreateProgram = (PFNGLCREATEPROGRAMPROC)libgl_sym("glCreateProgram");
    p->gl3wCreateProgramPipelines = (PFNGLCREATEPROGRAMPIPELINESPROC)libgl_sym("glCreateProgramPipelines");
    p->gl3wCreateQueries = (PFNGLCREATEQUERIESPROC)libgl_sym("glCreateQueries");
    p->gl3wCreateRenderbuffers = (PFNGLCREATERENDERBUFFERSPROC)libgl_sym("glCreateRenderbuffers");
    p->gl3wCreateSamplers = (PFNGLCREATESAMPLERSPROC)libgl_sym("glCreateSamplers");
    p->gl3wCreateShader = (PFNGLCREATESHADERPROC)libgl_sym("glCreateShader");
    p->gl3wCreateShaderProgramv = (PFNGLCREATESHADERPROGRAMVPROC)libgl_sym("glCreateShaderProgramv");
    p->gl3wCreateSyncFromCLeventARB = (PFNGLCREATESYNCFROMCLEVENTARBPROC)libgl_sym("glCreateSyncFromCLeventARB");
    p->gl3wCreateTextures = (PFNGLCREATETEXTURESPROC)libgl_sym("glCreateTextures");
    p->gl3wCreateTransformFeedbacks = (PFNGLCREATETRANSFORMFEEDBACKSPROC)libgl_sym("glCreateTransformFeedbacks");
    p->gl3wCreateVertexArrays = (PFNGLCREATEVERTEXARRAYSPROC)libgl_sym("glCreateVertexArrays");
    p->gl3wCullFace = (PFNGLCULLFACEPROC)libgl_sym("glCullFace");
    p->gl3wDebugMessageCallback = (PFNGLDEBUGMESSAGECALLBACKPROC)libgl_sym("glDebugMessageCallback");
    p->gl3wDebugMessageCallbackARB = (PFNGLDEBUGMESSAGECALLBACKARBPROC)libgl_sym("glDebugMessageCallbackARB");
    p->gl3wDebugMessageControl = (PFNGLDEBUGMESSAGECONTROLPROC)libgl_sym("glDebugMessageControl");
    p->gl3wDebugMessageControlARB = (PFNGLDEBUGMESSAGECONTROLARBPROC)libgl_sym("glDebugMessageControlARB");
    p->gl3wDebugMessageInsert = (PFNGLDEBUGMESSAGEINSERTPROC)libgl_sym("glDebugMessageInsert");
    p->gl3wDebugMessageInsertARB = (PFNGLDEBUGMESSAGEINSERTARBPROC)libgl_sym("glDebugMessageInsertARB");
    p->gl3wDeleteBuffers = (PFNGLDELETEBUFFERSPROC)libgl_sym("glDeleteBuffers");
    p->gl3wDeleteFramebuffers = (PFNGLDELETEFRAMEBUFFERSPROC)libgl_sym("glDeleteFramebuffers");
    p->gl3wDeleteNamedStringARB = (PFNGLDELETENAMEDSTRINGARBPROC)libgl_sym("glDeleteNamedStringARB");
    p->gl3wDeleteProgram = (PFNGLDELETEPROGRAMPROC)libgl_sym("glDeleteProgram");
    p->gl3wDeleteProgramPipelines = (PFNGLDELETEPROGRAMPIPELINESPROC)libgl_sym("glDeleteProgramPipelines");
    p->gl3wDeleteQueries = (PFNGLDELETEQUERIESPROC)libgl_sym("glDeleteQueries");
    p->gl3wDeleteRenderbuffers = (PFNGLDELETERENDERBUFFERSPROC)libgl_sym("glDeleteRenderbuffers");
    p->gl3wDeleteSamplers = (PFNGLDELETESAMPLERSPROC)libgl_sym("glDeleteSamplers");
    p->gl3wDeleteShader = (PFNGLDELETESHADERPROC)libgl_sym("glDeleteShader");
    p->gl3wDeleteSync = (PFNGLDELETESYNCPROC)libgl_sym("glDeleteSync");
    p->gl3wDeleteTextures = (PFNGLDELETETEXTURESPROC)libgl_sym("glDeleteTextures");
    p->gl3wDeleteTransformFeedbacks = (PFNGLDELETETRANSFORMFEEDBACKSPROC)libgl_sym("glDeleteTransformFeedbacks");
    p->gl3wDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC)libgl_sym("glDeleteVertexArrays");
    p->gl3wDepthFunc = (PFNGLDEPTHFUNCPROC)libgl_sym("glDepthFunc");
    p->gl3wDepthMask = (PFNGLDEPTHMASKPROC)libgl_sym("glDepthMask");
    p->gl3wDepthRange = (PFNGLDEPTHRANGEPROC)libgl_sym("glDepthRange");
    p->gl3wDepthRangeArrayv = (PFNGLDEPTHRANGEARRAYVPROC)libgl_sym("glDepthRangeArrayv");
    p->gl3wDepthRangeIndexed = (PFNGLDEPTHRANGEINDEXEDPROC)libgl_sym("glDepthRangeIndexed");
    p->gl3wDepthRangef = (PFNGLDEPTHRANGEFPROC)libgl_sym("glDepthRangef");
    p->gl3wDetachShader = (PFNGLDETACHSHADERPROC)libgl_sym("glDetachShader");
    p->gl3wDisable = (PFNGLDISABLEPROC)libgl_sym("glDisable");
    p->gl3wDisableVertexArrayAttrib = (PFNGLDISABLEVERTEXARRAYATTRIBPROC)libgl_sym("glDisableVertexArrayAttrib");
    p->gl3wDisableVertexAttribArray = (PFNGLDISABLEVERTEXATTRIBARRAYPROC)libgl_sym("glDisableVertexAttribArray");
    p->gl3wDisablei = (PFNGLDISABLEIPROC)libgl_sym("glDisablei");
    p->gl3wDispatchCompute = (PFNGLDISPATCHCOMPUTEPROC)libgl_sym("glDispatchCompute");
    p->gl3wDispatchComputeGroupSizeARB = (PFNGLDISPATCHCOMPUTEGROUPSIZEARBPROC)libgl_sym("glDispatchComputeGroupSizeARB");
    p->gl3wDispatchComputeIndirect = (PFNGLDISPATCHCOMPUTEINDIRECTPROC)libgl_sym("glDispatchComputeIndirect");
    p->gl3wDrawArrays = (PFNGLDRAWARRAYSPROC)libgl_sym("glDrawArrays");
    p->gl3wDrawArraysIndirect = (PFNGLDRAWARRAYSINDIRECTPROC)libgl_sym("glDrawArraysIndirect");
    p->gl3wDrawArraysInstanced = (PFNGLDRAWARRAYSINSTANCEDPROC)libgl_sym("glDrawArraysInstanced");
    p->gl3wDrawArraysInstancedBaseInstance = (PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC)libgl_sym("glDrawArraysInstancedBaseInstance");
    p->gl3wDrawBuffer = (PFNGLDRAWBUFFERPROC)libgl_sym("glDrawBuffer");
    p->gl3wDrawBuffers = (PFNGLDRAWBUFFERSPROC)libgl_sym("glDrawBuffers");
    p->gl3wDrawElements = (PFNGLDRAWELEMENTSPROC)libgl_sym("glDrawElements");
    p->gl3wDrawElementsBaseVertex = (PFNGLDRAWELEMENTSBASEVERTEXPROC)libgl_sym("glDrawElementsBaseVertex");
    p->gl3wDrawElementsIndirect = (PFNGLDRAWELEMENTSINDIRECTPROC)libgl_sym("glDrawElementsIndirect");
    p->gl3wDrawElementsInstanced = (PFNGLDRAWELEMENTSINSTANCEDPROC)libgl_sym("glDrawElementsInstanced");
    p->gl3wDrawElementsInstancedBaseInstance = (PFNGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC)libgl_sym("glDrawElementsInstancedBaseInstance");
    p->gl3wDrawElementsInstancedBaseVertex = (PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC)libgl_sym("glDrawElementsInstancedBaseVertex");
    p->gl3wDrawElementsInstancedBaseVertexBaseInstance = (PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC)libgl_sym("glDrawElementsInstancedBaseVertexBaseInstance");
    p->gl3wDrawRangeElements = (PFNGLDRAWRANGEELEMENTSPROC)libgl_sym("glDrawRangeElements");
    p->gl3wDrawRangeElementsBaseVertex = (PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC)libgl_sym("glDrawRangeElementsBaseVertex");
    p->gl3wDrawTransformFeedback = (PFNGLDRAWTRANSFORMFEEDBACKPROC)libgl_sym("glDrawTransformFeedback");
    p->gl3wDrawTransformFeedbackInstanced = (PFNGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC)libgl_sym("glDrawTransformFeedbackInstanced");
    p->gl3wDrawTransformFeedbackStream = (PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC)libgl_sym("glDrawTransformFeedbackStream");
    p->gl3wDrawTransformFeedbackStreamInstanced = (PFNGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC)libgl_sym("glDrawTransformFeedbackStreamInstanced");
    p->gl3wEnable = (PFNGLENABLEPROC)libgl_sym("glEnable");
    p->gl3wEnableVertexArrayAttrib = (PFNGLENABLEVERTEXARRAYATTRIBPROC)libgl_sym("glEnableVertexArrayAttrib");
    p->gl3wEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC)libgl_sym("glEnableVertexAttribArray");
    p->gl3wEnablei = (PFNGLENABLEIPROC)libgl_sym("glEnablei");
    p->gl3wEndConditionalRender = (PFNGLENDCONDITIONALRENDERPROC)libgl_sym("glEndConditionalRender");
    p->gl3wEndQuery = (PFNGLENDQUERYPROC)libgl_sym("glEndQuery");
    p->gl3wEndQueryIndexed = (PFNGLENDQUERYINDEXEDPROC)libgl_sym("glEndQueryIndexed");
    p->gl3wEndTransformFeedback = (PFNGLENDTRANSFORMFEEDBACKPROC)libgl_sym("glEndTransformFeedback");
    p->gl3wFenceSync = (PFNGLFENCESYNCPROC)libgl_sym("glFenceSync");
    p->gl3wFinish = (PFNGLFINISHPROC)libgl_sym("glFinish");
    p->gl3wFlush = (PFNGLFLUSHPROC)libgl_sym("glFlush");
    p->gl3wFlushMappedBufferRange = (PFNGLFLUSHMAPPEDBUFFERRANGEPROC)libgl_sym("glFlushMappedBufferRange");
    p->gl3wFlushMappedNamedBufferRange = (PFNGLFLUSHMAPPEDNAMEDBUFFERRANGEPROC)libgl_sym("glFlushMappedNamedBufferRange");
    p->gl3wFramebufferParameteri = (PFNGLFRAMEBUFFERPARAMETERIPROC)libgl_sym("glFramebufferParameteri");
    p->gl3wFramebufferRenderbuffer = (PFNGLFRAMEBUFFERRENDERBUFFERPROC)libgl_sym("glFramebufferRenderbuffer");
    p->gl3wFramebufferTexture = (PFNGLFRAMEBUFFERTEXTUREPROC)libgl_sym("glFramebufferTexture");
    p->gl3wFramebufferTexture1D = (PFNGLFRAMEBUFFERTEXTURE1DPROC)libgl_sym("glFramebufferTexture1D");
    p->gl3wFramebufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DPROC)libgl_sym("glFramebufferTexture2D");
    p->gl3wFramebufferTexture3D = (PFNGLFRAMEBUFFERTEXTURE3DPROC)libgl_sym("glFramebufferTexture3D");
    p->gl3wFramebufferTextureLayer = (PFNGLFRAMEBUFFERTEXTURELAYERPROC)libgl_sym("glFramebufferTextureLayer");
    p->gl3wFrontFace = (PFNGLFRONTFACEPROC)libgl_sym("glFrontFace");
    p->gl3wGenBuffers = (PFNGLGENBUFFERSPROC)libgl_sym("glGenBuffers");
    p->gl3wGenFramebuffers = (PFNGLGENFRAMEBUFFERSPROC)libgl_sym("glGenFramebuffers");
    p->gl3wGenProgramPipelines = (PFNGLGENPROGRAMPIPELINESPROC)libgl_sym("glGenProgramPipelines");
    p->gl3wGenQueries = (PFNGLGENQUERIESPROC)libgl_sym("glGenQueries");
    p->gl3wGenRenderbuffers = (PFNGLGENRENDERBUFFERSPROC)libgl_sym("glGenRenderbuffers");
    p->gl3wGenSamplers = (PFNGLGENSAMPLERSPROC)libgl_sym("glGenSamplers");
    p->gl3wGenTextures = (PFNGLGENTEXTURESPROC)libgl_sym("glGenTextures");
    p->gl3wGenTransformFeedbacks = (PFNGLGENTRANSFORMFEEDBACKSPROC)libgl_sym("glGenTransformFeedbacks");
    p->gl3wGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)libgl_sym("glGenVertexArrays");
    p->gl3wGenerateMipmap = (PFNGLGENERATEMIPMAPPROC)libgl_sym("glGenerateMipmap");
    p->gl3wGenerateTextureMipmap = (PFNGLGENERATETEXTUREMIPMAPPROC)libgl_sym("glGenerateTextureMipmap");
    p->gl3wGetActiveAtomicCounterBufferiv = (PFNGLGETACTIVEATOMICCOUNTERBUFFERIVPROC)libgl_sym("glGetActiveAtomicCounterBufferiv");
    p->gl3wGetActiveAttrib = (PFNGLGETACTIVEATTRIBPROC)libgl_sym("glGetActiveAttrib");
    p->gl3wGetActiveSubroutineName = (PFNGLGETACTIVESUBROUTINENAMEPROC)libgl_sym("glGetActiveSubroutineName");
    p->gl3wGetActiveSubroutineUniformName = (PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC)libgl_sym("glGetActiveSubroutineUniformName");
    p->gl3wGetActiveSubroutineUniformiv = (PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC)libgl_sym("glGetActiveSubroutineUniformiv");
    p->gl3wGetActiveUniform = (PFNGLGETACTIVEUNIFORMPROC)libgl_sym("glGetActiveUniform");
    p->gl3wGetActiveUniformBlockName = (PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC)libgl_sym("glGetActiveUniformBlockName");
    p->gl3wGetActiveUniformBlockiv = (PFNGLGETACTIVEUNIFORMBLOCKIVPROC)libgl_sym("glGetActiveUniformBlockiv");
    p->gl3wGetActiveUniformName = (PFNGLGETACTIVEUNIFORMNAMEPROC)libgl_sym("glGetActiveUniformName");
    p->gl3wGetActiveUniformsiv = (PFNGLGETACTIVEUNIFORMSIVPROC)libgl_sym("glGetActiveUniformsiv");
    p->gl3wGetAttachedShaders = (PFNGLGETATTACHEDSHADERSPROC)libgl_sym("glGetAttachedShaders");
    p->gl3wGetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC)libgl_sym("glGetAttribLocation");
    p->gl3wGetBooleani_v = (PFNGLGETBOOLEANI_VPROC)libgl_sym("glGetBooleani_v");
    p->gl3wGetBooleanv = (PFNGLGETBOOLEANVPROC)libgl_sym("glGetBooleanv");
    p->gl3wGetBufferParameteri64v = (PFNGLGETBUFFERPARAMETERI64VPROC)libgl_sym("glGetBufferParameteri64v");
    p->gl3wGetBufferParameteriv = (PFNGLGETBUFFERPARAMETERIVPROC)libgl_sym("glGetBufferParameteriv");
    p->gl3wGetBufferPointerv = (PFNGLGETBUFFERPOINTERVPROC)libgl_sym("glGetBufferPointerv");
    p->gl3wGetBufferSubData = (PFNGLGETBUFFERSUBDATAPROC)libgl_sym("glGetBufferSubData");
    p->gl3wGetCompressedTexImage = (PFNGLGETCOMPRESSEDTEXIMAGEPROC)libgl_sym("glGetCompressedTexImage");
    p->gl3wGetCompressedTextureImage = (PFNGLGETCOMPRESSEDTEXTUREIMAGEPROC)libgl_sym("glGetCompressedTextureImage");
    p->gl3wGetCompressedTextureSubImage = (PFNGLGETCOMPRESSEDTEXTURESUBIMAGEPROC)libgl_sym("glGetCompressedTextureSubImage");
    p->gl3wGetDebugMessageLog = (PFNGLGETDEBUGMESSAGELOGPROC)libgl_sym("glGetDebugMessageLog");
    p->gl3wGetDebugMessageLogARB = (PFNGLGETDEBUGMESSAGELOGARBPROC)libgl_sym("glGetDebugMessageLogARB");
    p->gl3wGetDoublei_v = (PFNGLGETDOUBLEI_VPROC)libgl_sym("glGetDoublei_v");
    p->gl3wGetDoublev = (PFNGLGETDOUBLEVPROC)libgl_sym("glGetDoublev");
    p->gl3wGetError = (PFNGLGETERRORPROC)libgl_sym("glGetError");
    p->gl3wGetFloati_v = (PFNGLGETFLOATI_VPROC)libgl_sym("glGetFloati_v");
    p->gl3wGetFloatv = (PFNGLGETFLOATVPROC)libgl_sym("glGetFloatv");
    p->gl3wGetFragDataIndex = (PFNGLGETFRAGDATAINDEXPROC)libgl_sym("glGetFragDataIndex");
    p->gl3wGetFragDataLocation = (PFNGLGETFRAGDATALOCATIONPROC)libgl_sym("glGetFragDataLocation");
    p->gl3wGetFramebufferAttachmentParameteriv = (PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC)libgl_sym("glGetFramebufferAttachmentParameteriv");
    p->gl3wGetFramebufferParameteriv = (PFNGLGETFRAMEBUFFERPARAMETERIVPROC)libgl_sym("glGetFramebufferParameteriv");
    p->gl3wGetGraphicsResetStatus = (PFNGLGETGRAPHICSRESETSTATUSPROC)libgl_sym("glGetGraphicsResetStatus");
    p->gl3wGetGraphicsResetStatusARB = (PFNGLGETGRAPHICSRESETSTATUSARBPROC)libgl_sym("glGetGraphicsResetStatusARB");
    p->gl3wGetImageHandleARB = (PFNGLGETIMAGEHANDLEARBPROC)libgl_sym("glGetImageHandleARB");
    p->gl3wGetInteger64i_v = (PFNGLGETINTEGER64I_VPROC)libgl_sym("glGetInteger64i_v");
    p->gl3wGetInteger64v = (PFNGLGETINTEGER64VPROC)libgl_sym("glGetInteger64v");
    p->gl3wGetIntegeri_v = (PFNGLGETINTEGERI_VPROC)libgl_sym("glGetIntegeri_v");
    p->gl3wGetIntegerv = (PFNGLGETINTEGERVPROC)libgl_sym("glGetIntegerv");
    p->gl3wGetInternalformati64v = (PFNGLGETINTERNALFORMATI64VPROC)libgl_sym("glGetInternalformati64v");
    p->gl3wGetInternalformativ = (PFNGLGETINTERNALFORMATIVPROC)libgl_sym("glGetInternalformativ");
    p->gl3wGetMultisamplefv = (PFNGLGETMULTISAMPLEFVPROC)libgl_sym("glGetMultisamplefv");
    p->gl3wGetNamedBufferParameteri64v = (PFNGLGETNAMEDBUFFERPARAMETERI64VPROC)libgl_sym("glGetNamedBufferParameteri64v");
    p->gl3wGetNamedBufferParameteriv = (PFNGLGETNAMEDBUFFERPARAMETERIVPROC)libgl_sym("glGetNamedBufferParameteriv");
    p->gl3wGetNamedBufferPointerv = (PFNGLGETNAMEDBUFFERPOINTERVPROC)libgl_sym("glGetNamedBufferPointerv");
    p->gl3wGetNamedBufferSubData = (PFNGLGETNAMEDBUFFERSUBDATAPROC)libgl_sym("glGetNamedBufferSubData");
    p->gl3wGetNamedFramebufferAttachmentParameteriv = (PFNGLGETNAMEDFRAMEBUFFERATTACHMENTPARAMETERIVPROC)libgl_sym("glGetNamedFramebufferAttachmentParameteriv");
    p->gl3wGetNamedFramebufferParameteriv = (PFNGLGETNAMEDFRAMEBUFFERPARAMETERIVPROC)libgl_sym("glGetNamedFramebufferParameteriv");
    p->gl3wGetNamedRenderbufferParameteriv = (PFNGLGETNAMEDRENDERBUFFERPARAMETERIVPROC)libgl_sym("glGetNamedRenderbufferParameteriv");
    p->gl3wGetNamedStringARB = (PFNGLGETNAMEDSTRINGARBPROC)libgl_sym("glGetNamedStringARB");
    p->gl3wGetNamedStringivARB = (PFNGLGETNAMEDSTRINGIVARBPROC)libgl_sym("glGetNamedStringivARB");
    p->gl3wGetObjectLabel = (PFNGLGETOBJECTLABELPROC)libgl_sym("glGetObjectLabel");
    p->gl3wGetObjectPtrLabel = (PFNGLGETOBJECTPTRLABELPROC)libgl_sym("glGetObjectPtrLabel");
    p->gl3wGetPointerv = (PFNGLGETPOINTERVPROC)libgl_sym("glGetPointerv");
    p->gl3wGetProgramBinary = (PFNGLGETPROGRAMBINARYPROC)libgl_sym("glGetProgramBinary");
    p->gl3wGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC)libgl_sym("glGetProgramInfoLog");
    p->gl3wGetProgramInterfaceiv = (PFNGLGETPROGRAMINTERFACEIVPROC)libgl_sym("glGetProgramInterfaceiv");
    p->gl3wGetProgramPipelineInfoLog = (PFNGLGETPROGRAMPIPELINEINFOLOGPROC)libgl_sym("glGetProgramPipelineInfoLog");
    p->gl3wGetProgramPipelineiv = (PFNGLGETPROGRAMPIPELINEIVPROC)libgl_sym("glGetProgramPipelineiv");
    p->gl3wGetProgramResourceIndex = (PFNGLGETPROGRAMRESOURCEINDEXPROC)libgl_sym("glGetProgramResourceIndex");
    p->gl3wGetProgramResourceLocation = (PFNGLGETPROGRAMRESOURCELOCATIONPROC)libgl_sym("glGetProgramResourceLocation");
    p->gl3wGetProgramResourceLocationIndex = (PFNGLGETPROGRAMRESOURCELOCATIONINDEXPROC)libgl_sym("glGetProgramResourceLocationIndex");
    p->gl3wGetProgramResourceName = (PFNGLGETPROGRAMRESOURCENAMEPROC)libgl_sym("glGetProgramResourceName");
    p->gl3wGetProgramResourceiv = (PFNGLGETPROGRAMRESOURCEIVPROC)libgl_sym("glGetProgramResourceiv");
    p->gl3wGetProgramStageiv = (PFNGLGETPROGRAMSTAGEIVPROC)libgl_sym("glGetProgramStageiv");
    p->gl3wGetProgramiv = (PFNGLGETPROGRAMIVPROC)libgl_sym("glGetProgramiv");
    p->gl3wGetQueryBufferObjecti64v = (PFNGLGETQUERYBUFFEROBJECTI64VPROC)libgl_sym("glGetQueryBufferObjecti64v");
    p->gl3wGetQueryBufferObjectiv = (PFNGLGETQUERYBUFFEROBJECTIVPROC)libgl_sym("glGetQueryBufferObjectiv");
    p->gl3wGetQueryBufferObjectui64v = (PFNGLGETQUERYBUFFEROBJECTUI64VPROC)libgl_sym("glGetQueryBufferObjectui64v");
    p->gl3wGetQueryBufferObjectuiv = (PFNGLGETQUERYBUFFEROBJECTUIVPROC)libgl_sym("glGetQueryBufferObjectuiv");
    p->gl3wGetQueryIndexediv = (PFNGLGETQUERYINDEXEDIVPROC)libgl_sym("glGetQueryIndexediv");
    p->gl3wGetQueryObjecti64v = (PFNGLGETQUERYOBJECTI64VPROC)libgl_sym("glGetQueryObjecti64v");
    p->gl3wGetQueryObjectiv = (PFNGLGETQUERYOBJECTIVPROC)libgl_sym("glGetQueryObjectiv");
    p->gl3wGetQueryObjectui64v = (PFNGLGETQUERYOBJECTUI64VPROC)libgl_sym("glGetQueryObjectui64v");
    p->gl3wGetQueryObjectuiv = (PFNGLGETQUERYOBJECTUIVPROC)libgl_sym("glGetQueryObjectuiv");
    p->gl3wGetQueryiv = (PFNGLGETQUERYIVPROC)libgl_sym("glGetQueryiv");
    p->gl3wGetRenderbufferParameteriv = (PFNGLGETRENDERBUFFERPARAMETERIVPROC)libgl_sym("glGetRenderbufferParameteriv");
    p->gl3wGetSamplerParameterIiv = (PFNGLGETSAMPLERPARAMETERIIVPROC)libgl_sym("glGetSamplerParameterIiv");
    p->gl3wGetSamplerParameterIuiv = (PFNGLGETSAMPLERPARAMETERIUIVPROC)libgl_sym("glGetSamplerParameterIuiv");
    p->gl3wGetSamplerParameterfv = (PFNGLGETSAMPLERPARAMETERFVPROC)libgl_sym("glGetSamplerParameterfv");
    p->gl3wGetSamplerParameteriv = (PFNGLGETSAMPLERPARAMETERIVPROC)libgl_sym("glGetSamplerParameteriv");
    p->gl3wGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)libgl_sym("glGetShaderInfoLog");
    p->gl3wGetShaderPrecisionFormat = (PFNGLGETSHADERPRECISIONFORMATPROC)libgl_sym("glGetShaderPrecisionFormat");
    p->gl3wGetShaderSource = (PFNGLGETSHADERSOURCEPROC)libgl_sym("glGetShaderSource");
    p->gl3wGetShaderiv = (PFNGLGETSHADERIVPROC)libgl_sym("glGetShaderiv");
    p->gl3wGetString = (PFNGLGETSTRINGPROC)libgl_sym("glGetString");
    p->gl3wGetStringi = (PFNGLGETSTRINGIPROC)libgl_sym("glGetStringi");
    p->gl3wGetSubroutineIndex = (PFNGLGETSUBROUTINEINDEXPROC)libgl_sym("glGetSubroutineIndex");
    p->gl3wGetSubroutineUniformLocation = (PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC)libgl_sym("glGetSubroutineUniformLocation");
    p->gl3wGetSynciv = (PFNGLGETSYNCIVPROC)libgl_sym("glGetSynciv");
    p->gl3wGetTexImage = (PFNGLGETTEXIMAGEPROC)libgl_sym("glGetTexImage");
    p->gl3wGetTexLevelParameterfv = (PFNGLGETTEXLEVELPARAMETERFVPROC)libgl_sym("glGetTexLevelParameterfv");
    p->gl3wGetTexLevelParameteriv = (PFNGLGETTEXLEVELPARAMETERIVPROC)libgl_sym("glGetTexLevelParameteriv");
    p->gl3wGetTexParameterIiv = (PFNGLGETTEXPARAMETERIIVPROC)libgl_sym("glGetTexParameterIiv");
    p->gl3wGetTexParameterIuiv = (PFNGLGETTEXPARAMETERIUIVPROC)libgl_sym("glGetTexParameterIuiv");
    p->gl3wGetTexParameterfv = (PFNGLGETTEXPARAMETERFVPROC)libgl_sym("glGetTexParameterfv");
    p->gl3wGetTexParameteriv = (PFNGLGETTEXPARAMETERIVPROC)libgl_sym("glGetTexParameteriv");
    p->gl3wGetTextureHandleARB = (PFNGLGETTEXTUREHANDLEARBPROC)libgl_sym("glGetTextureHandleARB");
    p->gl3wGetTextureImage = (PFNGLGETTEXTUREIMAGEPROC)libgl_sym("glGetTextureImage");
    p->gl3wGetTextureLevelParameterfv = (PFNGLGETTEXTURELEVELPARAMETERFVPROC)libgl_sym("glGetTextureLevelParameterfv");
    p->gl3wGetTextureLevelParameteriv = (PFNGLGETTEXTURELEVELPARAMETERIVPROC)libgl_sym("glGetTextureLevelParameteriv");
    p->gl3wGetTextureParameterIiv = (PFNGLGETTEXTUREPARAMETERIIVPROC)libgl_sym("glGetTextureParameterIiv");
    p->gl3wGetTextureParameterIuiv = (PFNGLGETTEXTUREPARAMETERIUIVPROC)libgl_sym("glGetTextureParameterIuiv");
    p->gl3wGetTextureParameterfv = (PFNGLGETTEXTUREPARAMETERFVPROC)libgl_sym("glGetTextureParameterfv");
    p->gl3wGetTextureParameteriv = (PFNGLGETTEXTUREPARAMETERIVPROC)libgl_sym("glGetTextureParameteriv");
    p->gl3wGetTextureSamplerHandleARB = (PFNGLGETTEXTURESAMPLERHANDLEARBPROC)libgl_sym("glGetTextureSamplerHandleARB");
    p->gl3wGetTextureSubImage = (PFNGLGETTEXTURESUBIMAGEPROC)libgl_sym("glGetTextureSubImage");
    p->gl3wGetTransformFeedbackVarying = (PFNGLGETTRANSFORMFEEDBACKVARYINGPROC)libgl_sym("glGetTransformFeedbackVarying");
    p->gl3wGetTransformFeedbacki64_v = (PFNGLGETTRANSFORMFEEDBACKI64_VPROC)libgl_sym("glGetTransformFeedbacki64_v");
    p->gl3wGetTransformFeedbacki_v = (PFNGLGETTRANSFORMFEEDBACKI_VPROC)libgl_sym("glGetTransformFeedbacki_v");
    p->gl3wGetTransformFeedbackiv = (PFNGLGETTRANSFORMFEEDBACKIVPROC)libgl_sym("glGetTransformFeedbackiv");
    p->gl3wGetUniformBlockIndex = (PFNGLGETUNIFORMBLOCKINDEXPROC)libgl_sym("glGetUniformBlockIndex");
    p->gl3wGetUniformIndices = (PFNGLGETUNIFORMINDICESPROC)libgl_sym("glGetUniformIndices");
    p->gl3wGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)libgl_sym("glGetUniformLocation");
    p->gl3wGetUniformSubroutineuiv = (PFNGLGETUNIFORMSUBROUTINEUIVPROC)libgl_sym("glGetUniformSubroutineuiv");
    p->gl3wGetUniformdv = (PFNGLGETUNIFORMDVPROC)libgl_sym("glGetUniformdv");
    p->gl3wGetUniformfv = (PFNGLGETUNIFORMFVPROC)libgl_sym("glGetUniformfv");
    p->gl3wGetUniformiv = (PFNGLGETUNIFORMIVPROC)libgl_sym("glGetUniformiv");
    p->gl3wGetUniformuiv = (PFNGLGETUNIFORMUIVPROC)libgl_sym("glGetUniformuiv");
    p->gl3wGetVertexArrayIndexed64iv = (PFNGLGETVERTEXARRAYINDEXED64IVPROC)libgl_sym("glGetVertexArrayIndexed64iv");
    p->gl3wGetVertexArrayIndexediv = (PFNGLGETVERTEXARRAYINDEXEDIVPROC)libgl_sym("glGetVertexArrayIndexediv");
    p->gl3wGetVertexArrayiv = (PFNGLGETVERTEXARRAYIVPROC)libgl_sym("glGetVertexArrayiv");
    p->gl3wGetVertexAttribIiv = (PFNGLGETVERTEXATTRIBIIVPROC)libgl_sym("glGetVertexAttribIiv");
    p->gl3wGetVertexAttribIuiv = (PFNGLGETVERTEXATTRIBIUIVPROC)libgl_sym("glGetVertexAttribIuiv");
    p->gl3wGetVertexAttribLdv = (PFNGLGETVERTEXATTRIBLDVPROC)libgl_sym("glGetVertexAttribLdv");
    p->gl3wGetVertexAttribLui64vARB = (PFNGLGETVERTEXATTRIBLUI64VARBPROC)libgl_sym("glGetVertexAttribLui64vARB");
    p->gl3wGetVertexAttribPointerv = (PFNGLGETVERTEXATTRIBPOINTERVPROC)libgl_sym("glGetVertexAttribPointerv");
    p->gl3wGetVertexAttribdv = (PFNGLGETVERTEXATTRIBDVPROC)libgl_sym("glGetVertexAttribdv");
    p->gl3wGetVertexAttribfv = (PFNGLGETVERTEXATTRIBFVPROC)libgl_sym("glGetVertexAttribfv");
    p->gl3wGetVertexAttribiv = (PFNGLGETVERTEXATTRIBIVPROC)libgl_sym("glGetVertexAttribiv");
    p->gl3wGetnCompressedTexImage = (PFNGLGETNCOMPRESSEDTEXIMAGEPROC)libgl_sym("glGetnCompressedTexImage");
    p->gl3wGetnCompressedTexImageARB = (PFNGLGETNCOMPRESSEDTEXIMAGEARBPROC)libgl_sym("glGetnCompressedTexImageARB");
    p->gl3wGetnTexImage = (PFNGLGETNTEXIMAGEPROC)libgl_sym("glGetnTexImage");
    p->gl3wGetnTexImageARB = (PFNGLGETNTEXIMAGEARBPROC)libgl_sym("glGetnTexImageARB");
    p->gl3wGetnUniformdv = (PFNGLGETNUNIFORMDVPROC)libgl_sym("glGetnUniformdv");
    p->gl3wGetnUniformdvARB = (PFNGLGETNUNIFORMDVARBPROC)libgl_sym("glGetnUniformdvARB");
    p->gl3wGetnUniformfv = (PFNGLGETNUNIFORMFVPROC)libgl_sym("glGetnUniformfv");
    p->gl3wGetnUniformfvARB = (PFNGLGETNUNIFORMFVARBPROC)libgl_sym("glGetnUniformfvARB");
    p->gl3wGetnUniformiv = (PFNGLGETNUNIFORMIVPROC)libgl_sym("glGetnUniformiv");
    p->gl3wGetnUniformivARB = (PFNGLGETNUNIFORMIVARBPROC)libgl_sym("glGetnUniformivARB");
    p->gl3wGetnUniformuiv = (PFNGLGETNUNIFORMUIVPROC)libgl_sym("glGetnUniformuiv");
    p->gl3wGetnUniformuivARB = (PFNGLGETNUNIFORMUIVARBPROC)libgl_sym("glGetnUniformuivARB");
    p->gl3wHint = (PFNGLHINTPROC)libgl_sym("glHint");
    p->gl3wInvalidateBufferData = (PFNGLINVALIDATEBUFFERDATAPROC)libgl_sym("glInvalidateBufferData");
    p->gl3wInvalidateBufferSubData = (PFNGLINVALIDATEBUFFERSUBDATAPROC)libgl_sym("glInvalidateBufferSubData");
    p->gl3wInvalidateFramebuffer = (PFNGLINVALIDATEFRAMEBUFFERPROC)libgl_sym("glInvalidateFramebuffer");
    p->gl3wInvalidateNamedFramebufferData = (PFNGLINVALIDATENAMEDFRAMEBUFFERDATAPROC)libgl_sym("glInvalidateNamedFramebufferData");
    p->gl3wInvalidateNamedFramebufferSubData = (PFNGLINVALIDATENAMEDFRAMEBUFFERSUBDATAPROC)libgl_sym("glInvalidateNamedFramebufferSubData");
    p->gl3wInvalidateSubFramebuffer = (PFNGLINVALIDATESUBFRAMEBUFFERPROC)libgl_sym("glInvalidateSubFramebuffer");
    p->gl3wInvalidateTexImage = (PFNGLINVALIDATETEXIMAGEPROC)libgl_sym("glInvalidateTexImage");
    p->gl3wInvalidateTexSubImage = (PFNGLINVALIDATETEXSUBIMAGEPROC)libgl_sym("glInvalidateTexSubImage");
    p->gl3wIsBuffer = (PFNGLISBUFFERPROC)libgl_sym("glIsBuffer");
    p->gl3wIsEnabled = (PFNGLISENABLEDPROC)libgl_sym("glIsEnabled");
    p->gl3wIsEnabledi = (PFNGLISENABLEDIPROC)libgl_sym("glIsEnabledi");
    p->gl3wIsFramebuffer = (PFNGLISFRAMEBUFFERPROC)libgl_sym("glIsFramebuffer");
    p->gl3wIsImageHandleResidentARB = (PFNGLISIMAGEHANDLERESIDENTARBPROC)libgl_sym("glIsImageHandleResidentARB");
    p->gl3wIsNamedStringARB = (PFNGLISNAMEDSTRINGARBPROC)libgl_sym("glIsNamedStringARB");
    p->gl3wIsProgram = (PFNGLISPROGRAMPROC)libgl_sym("glIsProgram");
    p->gl3wIsProgramPipeline = (PFNGLISPROGRAMPIPELINEPROC)libgl_sym("glIsProgramPipeline");
    p->gl3wIsQuery = (PFNGLISQUERYPROC)libgl_sym("glIsQuery");
    p->gl3wIsRenderbuffer = (PFNGLISRENDERBUFFERPROC)libgl_sym("glIsRenderbuffer");
    p->gl3wIsSampler = (PFNGLISSAMPLERPROC)libgl_sym("glIsSampler");
    p->gl3wIsShader = (PFNGLISSHADERPROC)libgl_sym("glIsShader");
    p->gl3wIsSync = (PFNGLISSYNCPROC)libgl_sym("glIsSync");
    p->gl3wIsTexture = (PFNGLISTEXTUREPROC)libgl_sym("glIsTexture");
    p->gl3wIsTextureHandleResidentARB = (PFNGLISTEXTUREHANDLERESIDENTARBPROC)libgl_sym("glIsTextureHandleResidentARB");
    p->gl3wIsTransformFeedback = (PFNGLISTRANSFORMFEEDBACKPROC)libgl_sym("glIsTransformFeedback");
    p->gl3wIsVertexArray = (PFNGLISVERTEXARRAYPROC)libgl_sym("glIsVertexArray");
    p->gl3wLineWidth = (PFNGLLINEWIDTHPROC)libgl_sym("glLineWidth");
    p->gl3wLinkProgram = (PFNGLLINKPROGRAMPROC)libgl_sym("glLinkProgram");
    p->gl3wLogicOp = (PFNGLLOGICOPPROC)libgl_sym("glLogicOp");
    p->gl3wMakeImageHandleNonResidentARB = (PFNGLMAKEIMAGEHANDLENONRESIDENTARBPROC)libgl_sym("glMakeImageHandleNonResidentARB");
    p->gl3wMakeImageHandleResidentARB = (PFNGLMAKEIMAGEHANDLERESIDENTARBPROC)libgl_sym("glMakeImageHandleResidentARB");
    p->gl3wMakeTextureHandleNonResidentARB = (PFNGLMAKETEXTUREHANDLENONRESIDENTARBPROC)libgl_sym("glMakeTextureHandleNonResidentARB");
    p->gl3wMakeTextureHandleResidentARB = (PFNGLMAKETEXTUREHANDLERESIDENTARBPROC)libgl_sym("glMakeTextureHandleResidentARB");
    p->gl3wMapBuffer = (PFNGLMAPBUFFERPROC)libgl_sym("glMapBuffer");
    p->gl3wMapBufferRange = (PFNGLMAPBUFFERRANGEPROC)libgl_sym("glMapBufferRange");
    p->gl3wMapNamedBuffer = (PFNGLMAPNAMEDBUFFERPROC)libgl_sym("glMapNamedBuffer");
    p->gl3wMapNamedBufferRange = (PFNGLMAPNAMEDBUFFERRANGEPROC)libgl_sym("glMapNamedBufferRange");
    p->gl3wMemoryBarrier = (PFNGLMEMORYBARRIERPROC)libgl_sym("glMemoryBarrier");
    p->gl3wMemoryBarrierByRegion = (PFNGLMEMORYBARRIERBYREGIONPROC)libgl_sym("glMemoryBarrierByRegion");
    p->gl3wMinSampleShading = (PFNGLMINSAMPLESHADINGPROC)libgl_sym("glMinSampleShading");
    p->gl3wMinSampleShadingARB = (PFNGLMINSAMPLESHADINGARBPROC)libgl_sym("glMinSampleShadingARB");
    p->gl3wMultiDrawArrays = (PFNGLMULTIDRAWARRAYSPROC)libgl_sym("glMultiDrawArrays");
    p->gl3wMultiDrawArraysIndirect = (PFNGLMULTIDRAWARRAYSINDIRECTPROC)libgl_sym("glMultiDrawArraysIndirect");
    p->gl3wMultiDrawArraysIndirectCountARB = (PFNGLMULTIDRAWARRAYSINDIRECTCOUNTARBPROC)libgl_sym("glMultiDrawArraysIndirectCountARB");
    p->gl3wMultiDrawElements = (PFNGLMULTIDRAWELEMENTSPROC)libgl_sym("glMultiDrawElements");
    p->gl3wMultiDrawElementsBaseVertex = (PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC)libgl_sym("glMultiDrawElementsBaseVertex");
    p->gl3wMultiDrawElementsIndirect = (PFNGLMULTIDRAWELEMENTSINDIRECTPROC)libgl_sym("glMultiDrawElementsIndirect");
    p->gl3wMultiDrawElementsIndirectCountARB = (PFNGLMULTIDRAWELEMENTSINDIRECTCOUNTARBPROC)libgl_sym("glMultiDrawElementsIndirectCountARB");
    p->gl3wNamedBufferData = (PFNGLNAMEDBUFFERDATAPROC)libgl_sym("glNamedBufferData");
    p->gl3wNamedBufferPageCommitmentARB = (PFNGLNAMEDBUFFERPAGECOMMITMENTARBPROC)libgl_sym("glNamedBufferPageCommitmentARB");
    p->gl3wNamedBufferPageCommitmentEXT = (PFNGLNAMEDBUFFERPAGECOMMITMENTEXTPROC)libgl_sym("glNamedBufferPageCommitmentEXT");
    p->gl3wNamedBufferStorage = (PFNGLNAMEDBUFFERSTORAGEPROC)libgl_sym("glNamedBufferStorage");
    p->gl3wNamedBufferSubData = (PFNGLNAMEDBUFFERSUBDATAPROC)libgl_sym("glNamedBufferSubData");
    p->gl3wNamedFramebufferDrawBuffer = (PFNGLNAMEDFRAMEBUFFERDRAWBUFFERPROC)libgl_sym("glNamedFramebufferDrawBuffer");
    p->gl3wNamedFramebufferDrawBuffers = (PFNGLNAMEDFRAMEBUFFERDRAWBUFFERSPROC)libgl_sym("glNamedFramebufferDrawBuffers");
    p->gl3wNamedFramebufferParameteri = (PFNGLNAMEDFRAMEBUFFERPARAMETERIPROC)libgl_sym("glNamedFramebufferParameteri");
    p->gl3wNamedFramebufferReadBuffer = (PFNGLNAMEDFRAMEBUFFERREADBUFFERPROC)libgl_sym("glNamedFramebufferReadBuffer");
    p->gl3wNamedFramebufferRenderbuffer = (PFNGLNAMEDFRAMEBUFFERRENDERBUFFERPROC)libgl_sym("glNamedFramebufferRenderbuffer");
    p->gl3wNamedFramebufferTexture = (PFNGLNAMEDFRAMEBUFFERTEXTUREPROC)libgl_sym("glNamedFramebufferTexture");
    p->gl3wNamedFramebufferTextureLayer = (PFNGLNAMEDFRAMEBUFFERTEXTURELAYERPROC)libgl_sym("glNamedFramebufferTextureLayer");
    p->gl3wNamedRenderbufferStorage = (PFNGLNAMEDRENDERBUFFERSTORAGEPROC)libgl_sym("glNamedRenderbufferStorage");
    p->gl3wNamedRenderbufferStorageMultisample = (PFNGLNAMEDRENDERBUFFERSTORAGEMULTISAMPLEPROC)libgl_sym("glNamedRenderbufferStorageMultisample");
    p->gl3wNamedStringARB = (PFNGLNAMEDSTRINGARBPROC)libgl_sym("glNamedStringARB");
    p->gl3wObjectLabel = (PFNGLOBJECTLABELPROC)libgl_sym("glObjectLabel");
    p->gl3wObjectPtrLabel = (PFNGLOBJECTPTRLABELPROC)libgl_sym("glObjectPtrLabel");
    p->gl3wPatchParameterfv = (PFNGLPATCHPARAMETERFVPROC)libgl_sym("glPatchParameterfv");
    p->gl3wPatchParameteri = (PFNGLPATCHPARAMETERIPROC)libgl_sym("glPatchParameteri");
    p->gl3wPauseTransformFeedback = (PFNGLPAUSETRANSFORMFEEDBACKPROC)libgl_sym("glPauseTransformFeedback");
    p->gl3wPixelStoref = (PFNGLPIXELSTOREFPROC)libgl_sym("glPixelStoref");
    p->gl3wPixelStorei = (PFNGLPIXELSTOREIPROC)libgl_sym("glPixelStorei");
    p->gl3wPointParameterf = (PFNGLPOINTPARAMETERFPROC)libgl_sym("glPointParameterf");
    p->gl3wPointParameterfv = (PFNGLPOINTPARAMETERFVPROC)libgl_sym("glPointParameterfv");
    p->gl3wPointParameteri = (PFNGLPOINTPARAMETERIPROC)libgl_sym("glPointParameteri");
    p->gl3wPointParameteriv = (PFNGLPOINTPARAMETERIVPROC)libgl_sym("glPointParameteriv");
    p->gl3wPointSize = (PFNGLPOINTSIZEPROC)libgl_sym("glPointSize");
    p->gl3wPolygonMode = (PFNGLPOLYGONMODEPROC)libgl_sym("glPolygonMode");
    p->gl3wPolygonOffset = (PFNGLPOLYGONOFFSETPROC)libgl_sym("glPolygonOffset");
    p->gl3wPopDebugGroup = (PFNGLPOPDEBUGGROUPPROC)libgl_sym("glPopDebugGroup");
    p->gl3wPrimitiveRestartIndex = (PFNGLPRIMITIVERESTARTINDEXPROC)libgl_sym("glPrimitiveRestartIndex");
    p->gl3wProgramBinary = (PFNGLPROGRAMBINARYPROC)libgl_sym("glProgramBinary");
    p->gl3wProgramParameteri = (PFNGLPROGRAMPARAMETERIPROC)libgl_sym("glProgramParameteri");
    p->gl3wProgramUniform1d = (PFNGLPROGRAMUNIFORM1DPROC)libgl_sym("glProgramUniform1d");
    p->gl3wProgramUniform1dv = (PFNGLPROGRAMUNIFORM1DVPROC)libgl_sym("glProgramUniform1dv");
    p->gl3wProgramUniform1f = (PFNGLPROGRAMUNIFORM1FPROC)libgl_sym("glProgramUniform1f");
    p->gl3wProgramUniform1fv = (PFNGLPROGRAMUNIFORM1FVPROC)libgl_sym("glProgramUniform1fv");
    p->gl3wProgramUniform1i = (PFNGLPROGRAMUNIFORM1IPROC)libgl_sym("glProgramUniform1i");
    p->gl3wProgramUniform1iv = (PFNGLPROGRAMUNIFORM1IVPROC)libgl_sym("glProgramUniform1iv");
    p->gl3wProgramUniform1ui = (PFNGLPROGRAMUNIFORM1UIPROC)libgl_sym("glProgramUniform1ui");
    p->gl3wProgramUniform1uiv = (PFNGLPROGRAMUNIFORM1UIVPROC)libgl_sym("glProgramUniform1uiv");
    p->gl3wProgramUniform2d = (PFNGLPROGRAMUNIFORM2DPROC)libgl_sym("glProgramUniform2d");
    p->gl3wProgramUniform2dv = (PFNGLPROGRAMUNIFORM2DVPROC)libgl_sym("glProgramUniform2dv");
    p->gl3wProgramUniform2f = (PFNGLPROGRAMUNIFORM2FPROC)libgl_sym("glProgramUniform2f");
    p->gl3wProgramUniform2fv = (PFNGLPROGRAMUNIFORM2FVPROC)libgl_sym("glProgramUniform2fv");
    p->gl3wProgramUniform2i = (PFNGLPROGRAMUNIFORM2IPROC)libgl_sym("glProgramUniform2i");
    p->gl3wProgramUniform2iv = (PFNGLPROGRAMUNIFORM2IVPROC)libgl_sym("glProgramUniform2iv");
    p->gl3wProgramUniform2ui = (PFNGLPROGRAMUNIFORM2UIPROC)libgl_sym("glProgramUniform2ui");
    p->gl3wProgramUniform2uiv = (PFNGLPROGRAMUNIFORM2UIVPROC)libgl_sym("glProgramUniform2uiv");
    p->gl3wProgramUniform3d = (PFNGLPROGRAMUNIFORM3DPROC)libgl_sym("glProgramUniform3d");
    p->gl3wProgramUniform3dv = (PFNGLPROGRAMUNIFORM3DVPROC)libgl_sym("glProgramUniform3dv");
    p->gl3wProgramUniform3f = (PFNGLPROGRAMUNIFORM3FPROC)libgl_sym("glProgramUniform3f");
    p->gl3wProgramUniform3fv = (PFNGLPROGRAMUNIFORM3FVPROC)libgl_sym("glProgramUniform3fv");
    p->gl3wProgramUniform3i = (PFNGLPROGRAMUNIFORM3IPROC)libgl_sym("glProgramUniform3i");
    p->gl3wProgramUniform3iv = (PFNGLPROGRAMUNIFORM3IVPROC)libgl_sym("glProgramUniform3iv");
    p->gl3wProgramUniform3ui = (PFNGLPROGRAMUNIFORM3UIPROC)libgl_sym("glProgramUniform3ui");
    p->gl3wProgramUniform3uiv = (PFNGLPROGRAMUNIFORM3UIVPROC)libgl_sym("glProgramUniform3uiv");
    p->gl3wProgramUniform4d = (PFNGLPROGRAMUNIFORM4DPROC)libgl_sym("glProgramUniform4d");
    p->gl3wProgramUniform4dv = (PFNGLPROGRAMUNIFORM4DVPROC)libgl_sym("glProgramUniform4dv");
    p->gl3wProgramUniform4f = (PFNGLPROGRAMUNIFORM4FPROC)libgl_sym("glProgramUniform4f");
    p->gl3wProgramUniform4fv = (PFNGLPROGRAMUNIFORM4FVPROC)libgl_sym("glProgramUniform4fv");
    p->gl3wProgramUniform4i = (PFNGLPROGRAMUNIFORM4IPROC)libgl_sym("glProgramUniform4i");
    p->gl3wProgramUniform4iv = (PFNGLPROGRAMUNIFORM4IVPROC)libgl_sym("glProgramUniform4iv");
    p->gl3wProgramUniform4ui = (PFNGLPROGRAMUNIFORM4UIPROC)libgl_sym("glProgramUniform4ui");
    p->gl3wProgramUniform4uiv = (PFNGLPROGRAMUNIFORM4UIVPROC)libgl_sym("glProgramUniform4uiv");
    p->gl3wProgramUniformHandleui64ARB = (PFNGLPROGRAMUNIFORMHANDLEUI64ARBPROC)libgl_sym("glProgramUniformHandleui64ARB");
    p->gl3wProgramUniformHandleui64vARB = (PFNGLPROGRAMUNIFORMHANDLEUI64VARBPROC)libgl_sym("glProgramUniformHandleui64vARB");
    p->gl3wProgramUniformMatrix2dv = (PFNGLPROGRAMUNIFORMMATRIX2DVPROC)libgl_sym("glProgramUniformMatrix2dv");
    p->gl3wProgramUniformMatrix2fv = (PFNGLPROGRAMUNIFORMMATRIX2FVPROC)libgl_sym("glProgramUniformMatrix2fv");
    p->gl3wProgramUniformMatrix2x3dv = (PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC)libgl_sym("glProgramUniformMatrix2x3dv");
    p->gl3wProgramUniformMatrix2x3fv = (PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC)libgl_sym("glProgramUniformMatrix2x3fv");
    p->gl3wProgramUniformMatrix2x4dv = (PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC)libgl_sym("glProgramUniformMatrix2x4dv");
    p->gl3wProgramUniformMatrix2x4fv = (PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC)libgl_sym("glProgramUniformMatrix2x4fv");
    p->gl3wProgramUniformMatrix3dv = (PFNGLPROGRAMUNIFORMMATRIX3DVPROC)libgl_sym("glProgramUniformMatrix3dv");
    p->gl3wProgramUniformMatrix3fv = (PFNGLPROGRAMUNIFORMMATRIX3FVPROC)libgl_sym("glProgramUniformMatrix3fv");
    p->gl3wProgramUniformMatrix3x2dv = (PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC)libgl_sym("glProgramUniformMatrix3x2dv");
    p->gl3wProgramUniformMatrix3x2fv = (PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC)libgl_sym("glProgramUniformMatrix3x2fv");
    p->gl3wProgramUniformMatrix3x4dv = (PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC)libgl_sym("glProgramUniformMatrix3x4dv");
    p->gl3wProgramUniformMatrix3x4fv = (PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC)libgl_sym("glProgramUniformMatrix3x4fv");
    p->gl3wProgramUniformMatrix4dv = (PFNGLPROGRAMUNIFORMMATRIX4DVPROC)libgl_sym("glProgramUniformMatrix4dv");
    p->gl3wProgramUniformMatrix4fv = (PFNGLPROGRAMUNIFORMMATRIX4FVPROC)libgl_sym("glProgramUniformMatrix4fv");
    p->gl3wProgramUniformMatrix4x2dv = (PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC)libgl_sym("glProgramUniformMatrix4x2dv");
    p->gl3wProgramUniformMatrix4x2fv = (PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC)libgl_sym("glProgramUniformMatrix4x2fv");
    p->gl3wProgramUniformMatrix4x3dv = (PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC)libgl_sym("glProgramUniformMatrix4x3dv");
    p->gl3wProgramUniformMatrix4x3fv = (PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC)libgl_sym("glProgramUniformMatrix4x3fv");
    p->gl3wProvokingVertex = (PFNGLPROVOKINGVERTEXPROC)libgl_sym("glProvokingVertex");
    p->gl3wPushDebugGroup = (PFNGLPUSHDEBUGGROUPPROC)libgl_sym("glPushDebugGroup");
    p->gl3wQueryCounter = (PFNGLQUERYCOUNTERPROC)libgl_sym("glQueryCounter");
    p->gl3wReadBuffer = (PFNGLREADBUFFERPROC)libgl_sym("glReadBuffer");
    p->gl3wReadPixels = (PFNGLREADPIXELSPROC)libgl_sym("glReadPixels");
    p->gl3wReadnPixels = (PFNGLREADNPIXELSPROC)libgl_sym("glReadnPixels");
    p->gl3wReadnPixelsARB = (PFNGLREADNPIXELSARBPROC)libgl_sym("glReadnPixelsARB");
    p->gl3wReleaseShaderCompiler = (PFNGLRELEASESHADERCOMPILERPROC)libgl_sym("glReleaseShaderCompiler");
    p->gl3wRenderbufferStorage = (PFNGLRENDERBUFFERSTORAGEPROC)libgl_sym("glRenderbufferStorage");
    p->gl3wRenderbufferStorageMultisample = (PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC)libgl_sym("glRenderbufferStorageMultisample");
    p->gl3wResumeTransformFeedback = (PFNGLRESUMETRANSFORMFEEDBACKPROC)libgl_sym("glResumeTransformFeedback");
    p->gl3wSampleCoverage = (PFNGLSAMPLECOVERAGEPROC)libgl_sym("glSampleCoverage");
    p->gl3wSampleMaski = (PFNGLSAMPLEMASKIPROC)libgl_sym("glSampleMaski");
    p->gl3wSamplerParameterIiv = (PFNGLSAMPLERPARAMETERIIVPROC)libgl_sym("glSamplerParameterIiv");
    p->gl3wSamplerParameterIuiv = (PFNGLSAMPLERPARAMETERIUIVPROC)libgl_sym("glSamplerParameterIuiv");
    p->gl3wSamplerParameterf = (PFNGLSAMPLERPARAMETERFPROC)libgl_sym("glSamplerParameterf");
    p->gl3wSamplerParameterfv = (PFNGLSAMPLERPARAMETERFVPROC)libgl_sym("glSamplerParameterfv");
    p->gl3wSamplerParameteri = (PFNGLSAMPLERPARAMETERIPROC)libgl_sym("glSamplerParameteri");
    p->gl3wSamplerParameteriv = (PFNGLSAMPLERPARAMETERIVPROC)libgl_sym("glSamplerParameteriv");
    p->gl3wScissor = (PFNGLSCISSORPROC)libgl_sym("glScissor");
    p->gl3wScissorArrayv = (PFNGLSCISSORARRAYVPROC)libgl_sym("glScissorArrayv");
    p->gl3wScissorIndexed = (PFNGLSCISSORINDEXEDPROC)libgl_sym("glScissorIndexed");
    p->gl3wScissorIndexedv = (PFNGLSCISSORINDEXEDVPROC)libgl_sym("glScissorIndexedv");
    p->gl3wShaderBinary = (PFNGLSHADERBINARYPROC)libgl_sym("glShaderBinary");
    p->gl3wShaderSource = (PFNGLSHADERSOURCEPROC)libgl_sym("glShaderSource");
    p->gl3wShaderStorageBlockBinding = (PFNGLSHADERSTORAGEBLOCKBINDINGPROC)libgl_sym("glShaderStorageBlockBinding");
    p->gl3wStencilFunc = (PFNGLSTENCILFUNCPROC)libgl_sym("glStencilFunc");
    p->gl3wStencilFuncSeparate = (PFNGLSTENCILFUNCSEPARATEPROC)libgl_sym("glStencilFuncSeparate");
    p->gl3wStencilMask = (PFNGLSTENCILMASKPROC)libgl_sym("glStencilMask");
    p->gl3wStencilMaskSeparate = (PFNGLSTENCILMASKSEPARATEPROC)libgl_sym("glStencilMaskSeparate");
    p->gl3wStencilOp = (PFNGLSTENCILOPPROC)libgl_sym("glStencilOp");
    p->gl3wStencilOpSeparate = (PFNGLSTENCILOPSEPARATEPROC)libgl_sym("glStencilOpSeparate");
    p->gl3wTexBuffer = (PFNGLTEXBUFFERPROC)libgl_sym("glTexBuffer");
    p->gl3wTexBufferRange = (PFNGLTEXBUFFERRANGEPROC)libgl_sym("glTexBufferRange");
    p->gl3wTexImage1D = (PFNGLTEXIMAGE1DPROC)libgl_sym("glTexImage1D");
    p->gl3wTexImage2D = (PFNGLTEXIMAGE2DPROC)libgl_sym("glTexImage2D");
    p->gl3wTexImage2DMultisample = (PFNGLTEXIMAGE2DMULTISAMPLEPROC)libgl_sym("glTexImage2DMultisample");
    p->gl3wTexImage3D = (PFNGLTEXIMAGE3DPROC)libgl_sym("glTexImage3D");
    p->gl3wTexImage3DMultisample = (PFNGLTEXIMAGE3DMULTISAMPLEPROC)libgl_sym("glTexImage3DMultisample");
    p->gl3wTexPageCommitmentARB = (PFNGLTEXPAGECOMMITMENTARBPROC)libgl_sym("glTexPageCommitmentARB");
    p->gl3wTexParameterIiv = (PFNGLTEXPARAMETERIIVPROC)libgl_sym("glTexParameterIiv");
    p->gl3wTexParameterIuiv = (PFNGLTEXPARAMETERIUIVPROC)libgl_sym("glTexParameterIuiv");
    p->gl3wTexParameterf = (PFNGLTEXPARAMETERFPROC)libgl_sym("glTexParameterf");
    p->gl3wTexParameterfv = (PFNGLTEXPARAMETERFVPROC)libgl_sym("glTexParameterfv");
    p->gl3wTexParameteri = (PFNGLTEXPARAMETERIPROC)libgl_sym("glTexParameteri");
    p->gl3wTexParameteriv = (PFNGLTEXPARAMETERIVPROC)libgl_sym("glTexParameteriv");
    p->gl3wTexStorage1D = (PFNGLTEXSTORAGE1DPROC)libgl_sym("glTexStorage1D");
    p->gl3wTexStorage2D = (PFNGLTEXSTORAGE2DPROC)libgl_sym("glTexStorage2D");
    p->gl3wTexStorage2DMultisample = (PFNGLTEXSTORAGE2DMULTISAMPLEPROC)libgl_sym("glTexStorage2DMultisample");
    p->gl3wTexStorage3D = (PFNGLTEXSTORAGE3DPROC)libgl_sym("glTexStorage3D");
    p->gl3wTexStorage3DMultisample = (PFNGLTEXSTORAGE3DMULTISAMPLEPROC)libgl_sym("glTexStorage3DMultisample");
    p->gl3wTexSubImage1D = (PFNGLTEXSUBIMAGE1DPROC)libgl_sym("glTexSubImage1D");
    p->gl3wTexSubImage2D = (PFNGLTEXSUBIMAGE2DPROC)libgl_sym("glTexSubImage2D");
    p->gl3wTexSubImage3D = (PFNGLTEXSUBIMAGE3DPROC)libgl_sym("glTexSubImage3D");
    p->gl3wTextureBarrier = (PFNGLTEXTUREBARRIERPROC)libgl_sym("glTextureBarrier");
    p->gl3wTextureBuffer = (PFNGLTEXTUREBUFFERPROC)libgl_sym("glTextureBuffer");
    p->gl3wTextureBufferRange = (PFNGLTEXTUREBUFFERRANGEPROC)libgl_sym("glTextureBufferRange");
    p->gl3wTextureParameterIiv = (PFNGLTEXTUREPARAMETERIIVPROC)libgl_sym("glTextureParameterIiv");
    p->gl3wTextureParameterIuiv = (PFNGLTEXTUREPARAMETERIUIVPROC)libgl_sym("glTextureParameterIuiv");
    p->gl3wTextureParameterf = (PFNGLTEXTUREPARAMETERFPROC)libgl_sym("glTextureParameterf");
    p->gl3wTextureParameterfv = (PFNGLTEXTUREPARAMETERFVPROC)libgl_sym("glTextureParameterfv");
    p->gl3wTextureParameteri = (PFNGLTEXTUREPARAMETERIPROC)libgl_sym("glTextureParameteri");
    p->gl3wTextureParameteriv = (PFNGLTEXTUREPARAMETERIVPROC)libgl_sym("glTextureParameteriv");
    p->gl3wTextureStorage1D = (PFNGLTEXTURESTORAGE1DPROC)libgl_sym("glTextureStorage1D");
    p->gl3wTextureStorage2D = (PFNGLTEXTURESTORAGE2DPROC)libgl_sym("glTextureStorage2D");
    p->gl3wTextureStorage2DMultisample = (PFNGLTEXTURESTORAGE2DMULTISAMPLEPROC)libgl_sym("glTextureStorage2DMultisample");
    p->gl3wTextureStorage3D = (PFNGLTEXTURESTORAGE3DPROC)libgl_sym("glTextureStorage3D");
    p->gl3wTextureStorage3DMultisample = (PFNGLTEXTURESTORAGE3DMULTISAMPLEPROC)libgl_sym("glTextureStorage3DMultisample");
    p->gl3wTextureSubImage1D = (PFNGLTEXTURESUBIMAGE1DPROC)libgl_sym("glTextureSubImage1D");
    p->gl3wTextureSubImage2D = (PFNGLTEXTURESUBIMAGE2DPROC)libgl_sym("glTextureSubImage2D");
    p->gl3wTextureSubImage3D = (PFNGLTEXTURESUBIMAGE3DPROC)libgl_sym("glTextureSubImage3D");
    p->gl3wTextureView = (PFNGLTEXTUREVIEWPROC)libgl_sym("glTextureView");
    p->gl3wTransformFeedbackBufferBase = (PFNGLTRANSFORMFEEDBACKBUFFERBASEPROC)libgl_sym("glTransformFeedbackBufferBase");
    p->gl3wTransformFeedbackBufferRange = (PFNGLTRANSFORMFEEDBACKBUFFERRANGEPROC)libgl_sym("glTransformFeedbackBufferRange");
    p->gl3wTransformFeedbackVaryings = (PFNGLTRANSFORMFEEDBACKVARYINGSPROC)libgl_sym("glTransformFeedbackVaryings");
    p->gl3wUniform1d = (PFNGLUNIFORM1DPROC)libgl_sym("glUniform1d");
    p->gl3wUniform1dv = (PFNGLUNIFORM1DVPROC)libgl_sym("glUniform1dv");
    p->gl3wUniform1f = (PFNGLUNIFORM1FPROC)libgl_sym("glUniform1f");
    p->gl3wUniform1fv = (PFNGLUNIFORM1FVPROC)libgl_sym("glUniform1fv");
    p->gl3wUniform1i = (PFNGLUNIFORM1IPROC)libgl_sym("glUniform1i");
    p->gl3wUniform1iv = (PFNGLUNIFORM1IVPROC)libgl_sym("glUniform1iv");
    p->gl3wUniform1ui = (PFNGLUNIFORM1UIPROC)libgl_sym("glUniform1ui");
    p->gl3wUniform1uiv = (PFNGLUNIFORM1UIVPROC)libgl_sym("glUniform1uiv");
    p->gl3wUniform2d = (PFNGLUNIFORM2DPROC)libgl_sym("glUniform2d");
    p->gl3wUniform2dv = (PFNGLUNIFORM2DVPROC)libgl_sym("glUniform2dv");
    p->gl3wUniform2f = (PFNGLUNIFORM2FPROC)libgl_sym("glUniform2f");
    p->gl3wUniform2fv = (PFNGLUNIFORM2FVPROC)libgl_sym("glUniform2fv");
    p->gl3wUniform2i = (PFNGLUNIFORM2IPROC)libgl_sym("glUniform2i");
    p->gl3wUniform2iv = (PFNGLUNIFORM2IVPROC)libgl_sym("glUniform2iv");
    p->gl3wUniform2ui = (PFNGLUNIFORM2UIPROC)libgl_sym("glUniform2ui");
    p->gl3wUniform2uiv = (PFNGLUNIFORM2UIVPROC)libgl_sym("glUniform2uiv");
    p->gl3wUniform3d = (PFNGLUNIFORM3DPROC)libgl_sym("glUniform3d");
    p->gl3wUniform3dv = (PFNGLUNIFORM3DVPROC)libgl_sym("glUniform3dv");
    p->gl3wUniform3f = (PFNGLUNIFORM3FPROC)libgl_sym("glUniform3f");
    p->gl3wUniform3fv = (PFNGLUNIFORM3FVPROC)libgl_sym("glUniform3fv");
    p->gl3wUniform3i = (PFNGLUNIFORM3IPROC)libgl_sym("glUniform3i");
    p->gl3wUniform3iv = (PFNGLUNIFORM3IVPROC)libgl_sym("glUniform3iv");
    p->gl3wUniform3ui = (PFNGLUNIFORM3UIPROC)libgl_sym("glUniform3ui");
    p->gl3wUniform3uiv = (PFNGLUNIFORM3UIVPROC)libgl_sym("glUniform3uiv");
    p->gl3wUniform4d = (PFNGLUNIFORM4DPROC)libgl_sym("glUniform4d");
    p->gl3wUniform4dv = (PFNGLUNIFORM4DVPROC)libgl_sym("glUniform4dv");
    p->gl3wUniform4f = (PFNGLUNIFORM4FPROC)libgl_sym("glUniform4f");
    p->gl3wUniform4fv = (PFNGLUNIFORM4FVPROC)libgl_sym("glUniform4fv");
    p->gl3wUniform4i = (PFNGLUNIFORM4IPROC)libgl_sym("glUniform4i");
    p->gl3wUniform4iv = (PFNGLUNIFORM4IVPROC)libgl_sym("glUniform4iv");
    p->gl3wUniform4ui = (PFNGLUNIFORM4UIPROC)libgl_sym("glUniform4ui");
    p->gl3wUniform4uiv = (PFNGLUNIFORM4UIVPROC)libgl_sym("glUniform4uiv");
    p->gl3wUniformBlockBinding = (PFNGLUNIFORMBLOCKBINDINGPROC)libgl_sym("glUniformBlockBinding");
    p->gl3wUniformHandleui64ARB = (PFNGLUNIFORMHANDLEUI64ARBPROC)libgl_sym("glUniformHandleui64ARB");
    p->gl3wUniformHandleui64vARB = (PFNGLUNIFORMHANDLEUI64VARBPROC)libgl_sym("glUniformHandleui64vARB");
    p->gl3wUniformMatrix2dv = (PFNGLUNIFORMMATRIX2DVPROC)libgl_sym("glUniformMatrix2dv");
    p->gl3wUniformMatrix2fv = (PFNGLUNIFORMMATRIX2FVPROC)libgl_sym("glUniformMatrix2fv");
    p->gl3wUniformMatrix2x3dv = (PFNGLUNIFORMMATRIX2X3DVPROC)libgl_sym("glUniformMatrix2x3dv");
    p->gl3wUniformMatrix2x3fv = (PFNGLUNIFORMMATRIX2X3FVPROC)libgl_sym("glUniformMatrix2x3fv");
    p->gl3wUniformMatrix2x4dv = (PFNGLUNIFORMMATRIX2X4DVPROC)libgl_sym("glUniformMatrix2x4dv");
    p->gl3wUniformMatrix2x4fv = (PFNGLUNIFORMMATRIX2X4FVPROC)libgl_sym("glUniformMatrix2x4fv");
    p->gl3wUniformMatrix3dv = (PFNGLUNIFORMMATRIX3DVPROC)libgl_sym("glUniformMatrix3dv");
    p->gl3wUniformMatrix3fv = (PFNGLUNIFORMMATRIX3FVPROC)libgl_sym("glUniformMatrix3fv");
    p->gl3wUniformMatrix3x2dv = (PFNGLUNIFORMMATRIX3X2DVPROC)libgl_sym("glUniformMatrix3x2dv");
    p->gl3wUniformMatrix3x2fv = (PFNGLUNIFORMMATRIX3X2FVPROC)libgl_sym("glUniformMatrix3x2fv");
    p->gl3wUniformMatrix3x4dv = (PFNGLUNIFORMMATRIX3X4DVPROC)libgl_sym("glUniformMatrix3x4dv");
    p->gl3wUniformMatrix3x4fv = (PFNGLUNIFORMMATRIX3X4FVPROC)libgl_sym("glUniformMatrix3x4fv");
    p->gl3wUniformMatrix4dv = (PFNGLUNIFORMMATRIX4DVPROC)libgl_sym("glUniformMatrix4dv");
    p->gl3wUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC)libgl_sym("glUniformMatrix4fv");
    p->gl3wUniformMatrix4x2dv = (PFNGLUNIFORMMATRIX4X2DVPROC)libgl_sym("glUniformMatrix4x2dv");
    p->gl3wUniformMatrix4x2fv = (PFNGLUNIFORMMATRIX4X2FVPROC)libgl_sym("glUniformMatrix4x2fv");
    p->gl3wUniformMatrix4x3dv = (PFNGLUNIFORMMATRIX4X3DVPROC)libgl_sym("glUniformMatrix4x3dv");
    p->gl3wUniformMatrix4x3fv = (PFNGLUNIFORMMATRIX4X3FVPROC)libgl_sym("glUniformMatrix4x3fv");
    p->gl3wUniformSubroutinesuiv = (PFNGLUNIFORMSUBROUTINESUIVPROC)libgl_sym("glUniformSubroutinesuiv");
    p->gl3wUnmapBuffer = (PFNGLUNMAPBUFFERPROC)libgl_sym("glUnmapBuffer");
    p->gl3wUnmapNamedBuffer = (PFNGLUNMAPNAMEDBUFFERPROC)libgl_sym("glUnmapNamedBuffer");
    p->gl3wUseProgram = (PFNGLUSEPROGRAMPROC)libgl_sym("glUseProgram");
    p->gl3wUseProgramStages = (PFNGLUSEPROGRAMSTAGESPROC)libgl_sym("glUseProgramStages");
    p->gl3wValidateProgram = (PFNGLVALIDATEPROGRAMPROC)libgl_sym("glValidateProgram");
    p->gl3wValidateProgramPipeline = (PFNGLVALIDATEPROGRAMPIPELINEPROC)libgl_sym("glValidateProgramPipeline");
    p->gl3wVertexArrayAttribBinding = (PFNGLVERTEXARRAYATTRIBBINDINGPROC)libgl_sym("glVertexArrayAttribBinding");
    p->gl3wVertexArrayAttribFormat = (PFNGLVERTEXARRAYATTRIBFORMATPROC)libgl_sym("glVertexArrayAttribFormat");
    p->gl3wVertexArrayAttribIFormat = (PFNGLVERTEXARRAYATTRIBIFORMATPROC)libgl_sym("glVertexArrayAttribIFormat");
    p->gl3wVertexArrayAttribLFormat = (PFNGLVERTEXARRAYATTRIBLFORMATPROC)libgl_sym("glVertexArrayAttribLFormat");
    p->gl3wVertexArrayBindingDivisor = (PFNGLVERTEXARRAYBINDINGDIVISORPROC)libgl_sym("glVertexArrayBindingDivisor");
    p->gl3wVertexArrayElementBuffer = (PFNGLVERTEXARRAYELEMENTBUFFERPROC)libgl_sym("glVertexArrayElementBuffer");
    p->gl3wVertexArrayVertexBuffer = (PFNGLVERTEXARRAYVERTEXBUFFERPROC)libgl_sym("glVertexArrayVertexBuffer");
    p->gl3wVertexArrayVertexBuffers = (PFNGLVERTEXARRAYVERTEXBUFFERSPROC)libgl_sym("glVertexArrayVertexBuffers");
    p->gl3wVertexAttrib1d = (PFNGLVERTEXATTRIB1DPROC)libgl_sym("glVertexAttrib1d");
    p->gl3wVertexAttrib1dv = (PFNGLVERTEXATTRIB1DVPROC)libgl_sym("glVertexAttrib1dv");
    p->gl3wVertexAttrib1f = (PFNGLVERTEXATTRIB1FPROC)libgl_sym("glVertexAttrib1f");
    p->gl3wVertexAttrib1fv = (PFNGLVERTEXATTRIB1FVPROC)libgl_sym("glVertexAttrib1fv");
    p->gl3wVertexAttrib1s = (PFNGLVERTEXATTRIB1SPROC)libgl_sym("glVertexAttrib1s");
    p->gl3wVertexAttrib1sv = (PFNGLVERTEXATTRIB1SVPROC)libgl_sym("glVertexAttrib1sv");
    p->gl3wVertexAttrib2d = (PFNGLVERTEXATTRIB2DPROC)libgl_sym("glVertexAttrib2d");
    p->gl3wVertexAttrib2dv = (PFNGLVERTEXATTRIB2DVPROC)libgl_sym("glVertexAttrib2dv");
    p->gl3wVertexAttrib2f = (PFNGLVERTEXATTRIB2FPROC)libgl_sym("glVertexAttrib2f");
    p->gl3wVertexAttrib2fv = (PFNGLVERTEXATTRIB2FVPROC)libgl_sym("glVertexAttrib2fv");
    p->gl3wVertexAttrib2s = (PFNGLVERTEXATTRIB2SPROC)libgl_sym("glVertexAttrib2s");
    p->gl3wVertexAttrib2sv = (PFNGLVERTEXATTRIB2SVPROC)libgl_sym("glVertexAttrib2sv");
    p->gl3wVertexAttrib3d = (PFNGLVERTEXATTRIB3DPROC)libgl_sym("glVertexAttrib3d");
    p->gl3wVertexAttrib3dv = (PFNGLVERTEXATTRIB3DVPROC)libgl_sym("glVertexAttrib3dv");
    p->gl3wVertexAttrib3f = (PFNGLVERTEXATTRIB3FPROC)libgl_sym("glVertexAttrib3f");
    p->gl3wVertexAttrib3fv = (PFNGLVERTEXATTRIB3FVPROC)libgl_sym("glVertexAttrib3fv");
    p->gl3wVertexAttrib3s = (PFNGLVERTEXATTRIB3SPROC)libgl_sym("glVertexAttrib3s");
    p->gl3wVertexAttrib3sv = (PFNGLVERTEXATTRIB3SVPROC)libgl_sym("glVertexAttrib3sv");
    p->gl3wVertexAttrib4Nbv = (PFNGLVERTEXATTRIB4NBVPROC)libgl_sym("glVertexAttrib4Nbv");
    p->gl3wVertexAttrib4Niv = (PFNGLVERTEXATTRIB4NIVPROC)libgl_sym("glVertexAttrib4Niv");
    p->gl3wVertexAttrib4Nsv = (PFNGLVERTEXATTRIB4NSVPROC)libgl_sym("glVertexAttrib4Nsv");
    p->gl3wVertexAttrib4Nub = (PFNGLVERTEXATTRIB4NUBPROC)libgl_sym("glVertexAttrib4Nub");
    p->gl3wVertexAttrib4Nubv = (PFNGLVERTEXATTRIB4NUBVPROC)libgl_sym("glVertexAttrib4Nubv");
    p->gl3wVertexAttrib4Nuiv = (PFNGLVERTEXATTRIB4NUIVPROC)libgl_sym("glVertexAttrib4Nuiv");
    p->gl3wVertexAttrib4Nusv = (PFNGLVERTEXATTRIB4NUSVPROC)libgl_sym("glVertexAttrib4Nusv");
    p->gl3wVertexAttrib4bv = (PFNGLVERTEXATTRIB4BVPROC)libgl_sym("glVertexAttrib4bv");
    p->gl3wVertexAttrib4d = (PFNGLVERTEXATTRIB4DPROC)libgl_sym("glVertexAttrib4d");
    p->gl3wVertexAttrib4dv = (PFNGLVERTEXATTRIB4DVPROC)libgl_sym("glVertexAttrib4dv");
    p->gl3wVertexAttrib4f = (PFNGLVERTEXATTRIB4FPROC)libgl_sym("glVertexAttrib4f");
    p->gl3wVertexAttrib4fv = (PFNGLVERTEXATTRIB4FVPROC)libgl_sym("glVertexAttrib4fv");
    p->gl3wVertexAttrib4iv = (PFNGLVERTEXATTRIB4IVPROC)libgl_sym("glVertexAttrib4iv");
    p->gl3wVertexAttrib4s = (PFNGLVERTEXATTRIB4SPROC)libgl_sym("glVertexAttrib4s");
    p->gl3wVertexAttrib4sv = (PFNGLVERTEXATTRIB4SVPROC)libgl_sym("glVertexAttrib4sv");
    p->gl3wVertexAttrib4ubv = (PFNGLVERTEXATTRIB4UBVPROC)libgl_sym("glVertexAttrib4ubv");
    p->gl3wVertexAttrib4uiv = (PFNGLVERTEXATTRIB4UIVPROC)libgl_sym("glVertexAttrib4uiv");
    p->gl3wVertexAttrib4usv = (PFNGLVERTEXATTRIB4USVPROC)libgl_sym("glVertexAttrib4usv");
    p->gl3wVertexAttribBinding = (PFNGLVERTEXATTRIBBINDINGPROC)libgl_sym("glVertexAttribBinding");
    p->gl3wVertexAttribDivisor = (PFNGLVERTEXATTRIBDIVISORPROC)libgl_sym("glVertexAttribDivisor");
    p->gl3wVertexAttribFormat = (PFNGLVERTEXATTRIBFORMATPROC)libgl_sym("glVertexAttribFormat");
    p->gl3wVertexAttribI1i = (PFNGLVERTEXATTRIBI1IPROC)libgl_sym("glVertexAttribI1i");
    p->gl3wVertexAttribI1iv = (PFNGLVERTEXATTRIBI1IVPROC)libgl_sym("glVertexAttribI1iv");
    p->gl3wVertexAttribI1ui = (PFNGLVERTEXATTRIBI1UIPROC)libgl_sym("glVertexAttribI1ui");
    p->gl3wVertexAttribI1uiv = (PFNGLVERTEXATTRIBI1UIVPROC)libgl_sym("glVertexAttribI1uiv");
    p->gl3wVertexAttribI2i = (PFNGLVERTEXATTRIBI2IPROC)libgl_sym("glVertexAttribI2i");
    p->gl3wVertexAttribI2iv = (PFNGLVERTEXATTRIBI2IVPROC)libgl_sym("glVertexAttribI2iv");
    p->gl3wVertexAttribI2ui = (PFNGLVERTEXATTRIBI2UIPROC)libgl_sym("glVertexAttribI2ui");
    p->gl3wVertexAttribI2uiv = (PFNGLVERTEXATTRIBI2UIVPROC)libgl_sym("glVertexAttribI2uiv");
    p->gl3wVertexAttribI3i = (PFNGLVERTEXATTRIBI3IPROC)libgl_sym("glVertexAttribI3i");
    p->gl3wVertexAttribI3iv = (PFNGLVERTEXATTRIBI3IVPROC)libgl_sym("glVertexAttribI3iv");
    p->gl3wVertexAttribI3ui = (PFNGLVERTEXATTRIBI3UIPROC)libgl_sym("glVertexAttribI3ui");
    p->gl3wVertexAttribI3uiv = (PFNGLVERTEXATTRIBI3UIVPROC)libgl_sym("glVertexAttribI3uiv");
    p->gl3wVertexAttribI4bv = (PFNGLVERTEXATTRIBI4BVPROC)libgl_sym("glVertexAttribI4bv");
    p->gl3wVertexAttribI4i = (PFNGLVERTEXATTRIBI4IPROC)libgl_sym("glVertexAttribI4i");
    p->gl3wVertexAttribI4iv = (PFNGLVERTEXATTRIBI4IVPROC)libgl_sym("glVertexAttribI4iv");
    p->gl3wVertexAttribI4sv = (PFNGLVERTEXATTRIBI4SVPROC)libgl_sym("glVertexAttribI4sv");
    p->gl3wVertexAttribI4ubv = (PFNGLVERTEXATTRIBI4UBVPROC)libgl_sym("glVertexAttribI4ubv");
    p->gl3wVertexAttribI4ui = (PFNGLVERTEXATTRIBI4UIPROC)libgl_sym("glVertexAttribI4ui");
    p->gl3wVertexAttribI4uiv = (PFNGLVERTEXATTRIBI4UIVPROC)libgl_sym("glVertexAttribI4uiv");
    p->gl3wVertexAttribI4usv = (PFNGLVERTEXATTRIBI4USVPROC)libgl_sym("glVertexAttribI4usv");
    p->gl3wVertexAttribIFormat = (PFNGLVERTEXATTRIBIFORMATPROC)libgl_sym("glVertexAttribIFormat");
    p->gl3wVertexAttribIPointer = (PFNGLVERTEXATTRIBIPOINTERPROC)libgl_sym("glVertexAttribIPointer");
    p->gl3wVertexAttribL1d = (PFNGLVERTEXATTRIBL1DPROC)libgl_sym("glVertexAttribL1d");
    p->gl3wVertexAttribL1dv = (PFNGLVERTEXATTRIBL1DVPROC)libgl_sym("glVertexAttribL1dv");
    p->gl3wVertexAttribL1ui64ARB = (PFNGLVERTEXATTRIBL1UI64ARBPROC)libgl_sym("glVertexAttribL1ui64ARB");
    p->gl3wVertexAttribL1ui64vARB = (PFNGLVERTEXATTRIBL1UI64VARBPROC)libgl_sym("glVertexAttribL1ui64vARB");
    p->gl3wVertexAttribL2d = (PFNGLVERTEXATTRIBL2DPROC)libgl_sym("glVertexAttribL2d");
    p->gl3wVertexAttribL2dv = (PFNGLVERTEXATTRIBL2DVPROC)libgl_sym("glVertexAttribL2dv");
    p->gl3wVertexAttribL3d = (PFNGLVERTEXATTRIBL3DPROC)libgl_sym("glVertexAttribL3d");
    p->gl3wVertexAttribL3dv = (PFNGLVERTEXATTRIBL3DVPROC)libgl_sym("glVertexAttribL3dv");
    p->gl3wVertexAttribL4d = (PFNGLVERTEXATTRIBL4DPROC)libgl_sym("glVertexAttribL4d");
    p->gl3wVertexAttribL4dv = (PFNGLVERTEXATTRIBL4DVPROC)libgl_sym("glVertexAttribL4dv");
    p->gl3wVertexAttribLFormat = (PFNGLVERTEXATTRIBLFORMATPROC)libgl_sym("glVertexAttribLFormat");
    p->gl3wVertexAttribLPointer = (PFNGLVERTEXATTRIBLPOINTERPROC)libgl_sym("glVertexAttribLPointer");
    p->gl3wVertexAttribP1ui = (PFNGLVERTEXATTRIBP1UIPROC)libgl_sym("glVertexAttribP1ui");
    p->gl3wVertexAttribP1uiv = (PFNGLVERTEXATTRIBP1UIVPROC)libgl_sym("glVertexAttribP1uiv");
    p->gl3wVertexAttribP2ui = (PFNGLVERTEXATTRIBP2UIPROC)libgl_sym("glVertexAttribP2ui");
    p->gl3wVertexAttribP2uiv = (PFNGLVERTEXATTRIBP2UIVPROC)libgl_sym("glVertexAttribP2uiv");
    p->gl3wVertexAttribP3ui = (PFNGLVERTEXATTRIBP3UIPROC)libgl_sym("glVertexAttribP3ui");
    p->gl3wVertexAttribP3uiv = (PFNGLVERTEXATTRIBP3UIVPROC)libgl_sym("glVertexAttribP3uiv");
    p->gl3wVertexAttribP4ui = (PFNGLVERTEXATTRIBP4UIPROC)libgl_sym("glVertexAttribP4ui");
    p->gl3wVertexAttribP4uiv = (PFNGLVERTEXATTRIBP4UIVPROC)libgl_sym("glVertexAttribP4uiv");
    p->gl3wVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC)libgl_sym("glVertexAttribPointer");
    p->gl3wVertexBindingDivisor = (PFNGLVERTEXBINDINGDIVISORPROC)libgl_sym("glVertexBindingDivisor");
    p->gl3wViewport = (PFNGLVIEWPORTPROC)libgl_sym("glViewport");
    p->gl3wViewportArrayv = (PFNGLVIEWPORTARRAYVPROC)libgl_sym("glViewportArrayv");
    p->gl3wViewportIndexedf = (PFNGLVIEWPORTINDEXEDFPROC)libgl_sym("glViewportIndexedf");
    p->gl3wViewportIndexedfv = (PFNGLVIEWPORTINDEXEDFVPROC)libgl_sym("glViewportIndexedfv");
    p->gl3wWaitSync = (PFNGLWAITSYNCPROC)libgl_sym("glWaitSync");


    if (!p->gl3wGetIntegerv)
        return 0;

    p->gl3wGetIntegerv(GL_MAJOR_VERSION, &p->major);
    p->gl3wGetIntegerv(GL_MINOR_VERSION, &p->minor);

    if (p->major < 3)
        return 0;

    return 1;
}

int gl3wInit(GL3WContext *context) {
    int res;
    libgl_open();
    memset(context, 0, sizeof(*context));
    res = setup_context(context);
    return res;
}

void gl3wShutdown(GL3WContext *context) {
    libgl_close();
    memset(context, 0, sizeof(*context));
}

int gl3wIsSupported(const GL3WContext *context, int major, int minor) {
    if (context->major < 3)
        return 0;

    if (context->major == major)
        return context->minor >= minor;
    else
        return context->major >= major;
}

GL3WglProc gl3wGetProcAddress(const char *proc) {
    return libgl_sym(proc);
}
