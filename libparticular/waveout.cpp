#include <Windows.h>
#include "waveout.h"
#include <mmreg.h>
#include <math.h>

#ifndef _WIN32_WCE
#define HAVE_KS_HEADERS
#endif


#ifdef HAVE_KS_HEADERS
#include <ks.h>
#include <ksmedia.h>
#endif

WaveOut::~WaveOut()
{
	Close();
}

MMRESULT WaveOut::Update()
{
	MMRESULT status;
	for(HEADER * h=hdrs;h;)
	{
		if (h->hdr.dwFlags&WHDR_DONE)
		{
			n_playing--;
			buf_size_used-=h->hdr.dwBufferLength;
			status = waveOutUnprepareHeader(hWo,&h->hdr,sizeof(WAVEHDR));
			HEADER* f=h;
			h=h->next;
			hdr_free(f);
			if (status != MMSYSERR_NOERROR) return status;
		}
		else h=h->next;
	}

	while(data_written>(m_needplay ? 0 : m_avgblock))
	{
		unsigned delta = min(min(data_written,m_maxblock),buf_size - write_ptr);
		delta -= delta % m_pcmspec.align();
		if (delta == 0) break;
		data_written -= delta;
		buf_size_used += delta;
		
		HEADER * h = hdr_alloc();
		h->hdr.dwBytesRecorded = h->hdr.dwBufferLength = delta;
		h->hdr.lpData = buffer + write_ptr;
		write_ptr += delta;
		if (write_ptr >= buf_size)
		{
			write_ptr -= buf_size;
			memcpy(buffer + buf_size,buffer,write_ptr);
		}
		
		n_playing++;
		total_written += delta;
		m_needplay = false;

		status = waveOutPrepareHeader(hWo,&h->hdr,sizeof(WAVEHDR));
		if (status != MMSYSERR_NOERROR) return status;
		status = waveOutWrite(hWo,&h->hdr,sizeof(WAVEHDR));
		if (status != MMSYSERR_NOERROR) return status;
	}
	
	return MMSYSERR_NOERROR;
}

MMRESULT WaveOut::WriteData(const void * p_data,unsigned p_size)
{
	const char * data=(const char*)p_data;

	if (p_size > CanWrite()) return MMSYSERR_INVALPARAM;

	unsigned written=0;
	unsigned size = p_size;
	while(size>0)
	{
		unsigned ptr=(data_written + write_ptr)%buf_size;
		unsigned delta=size;
		if (ptr+delta>buf_size) delta=buf_size-ptr;
		memcpy(buffer+ptr,data,delta);
		data+=delta;
		size-=delta;
		written+=delta;
		data_written+=delta;
	}
	return Update();
}

MMRESULT WaveOut::flush()
{
	MMRESULT status;
	status = waveOutReset(hWo);
	if (status != MMSYSERR_NOERROR) return status;

	while(hdrs)
	{
		if (hdrs->hdr.dwFlags & WHDR_PREPARED)
		{
			status = waveOutUnprepareHeader(hWo,&hdrs->hdr,sizeof(WAVEHDR));
			if (status != MMSYSERR_NOERROR) return status;
		}
		hdr_free(hdrs);
	}
	reset_stuff();
	
	return MMSYSERR_NOERROR;
}

MMRESULT WaveOut::Flush()
{
	MMRESULT status;
	if (hWo)
	{
		status = flush();
		if (status != MMSYSERR_NOERROR) return status;
		if (!m_paused)
		{
			status = waveOutRestart(hWo);
			if (status != MMSYSERR_NOERROR) return status;
		}
	}
	return MMSYSERR_NOERROR;
}


MMRESULT WaveOut::ForcePlay()
{
	//console::formatter() << "ForcePlay()";
	MMRESULT status;
	if (hWo)
	{
		if (!m_paused) m_needplay = true;
		status = Update();
		if (status != MMSYSERR_NOERROR) return status;
	}
	return MMSYSERR_NOERROR;
}

WaveOut::WaveOut() : m_volume(0)
{
	hWo=0;
	hdrs=0;
	hdrs_free=0;
	buffer=0;
	buf_size=buf_size_used=0;
	data_written=write_ptr=0;
	m_maxblock = 0;
	m_avgblock = 0;
	n_playing=0;
	m_paused = m_needplay = false;
	
}

void WaveOut::Close()
{
	killwaveout();
	if (buffer) {LocalFree(buffer);buffer=0;}
	hdr_free_list(hdrs); hdrs = 0;
	hdr_free_list(hdrs_free); hdrs_free = 0;

	buf_size=buf_size_used=0;
	data_written=write_ptr=0;
	m_maxblock = 0;
	m_avgblock = 0;
	n_playing=0;
}

MMRESULT WaveOut::Open(const t_pcmspec & p_spec,double p_buffer_length,unsigned p_device)
{
	m_pcmspec = p_spec;


	WAVEFORMATEX wfx;
	wfx.wFormatTag = m_pcmspec.m_float ? WAVE_FORMAT_IEEE_FLOAT : WAVE_FORMAT_PCM;
	wfx.nChannels = (WORD)m_pcmspec.m_channels;
	wfx.nSamplesPerSec = m_pcmspec.m_sample_rate;
	wfx.nAvgBytesPerSec = m_pcmspec.align() * m_pcmspec.m_sample_rate;
	wfx.nBlockAlign = (WORD)m_pcmspec.align();
	wfx.wBitsPerSample = (WORD)m_pcmspec.m_bits_per_sample;
	wfx.cbSize = 0;

	MMRESULT status;
#ifdef HAVE_KS_HEADERS
	{
		WAVEFORMATEXTENSIBLE wfxe;
		wfxe.Format=wfx;
		wfxe.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
		wfxe.Format.cbSize=22;
		wfxe.Samples.wReserved=0;
		wfxe.dwChannelMask = m_pcmspec.m_channel_config;

		wfxe.SubFormat = m_pcmspec.m_float ? KSDATAFORMAT_SUBTYPE_IEEE_FLOAT : KSDATAFORMAT_SUBTYPE_PCM;
		status = waveOutOpen(&hWo,p_device-1,(WAVEFORMATEX*)&wfxe,0,0,CALLBACK_NULL);
		if (status == WAVERR_BADFORMAT)
			status = waveOutOpen(&hWo,p_device-1,&wfx,0,0,CALLBACK_NULL);
	}
#else
	status = waveOutOpen(&hWo,p_device,&wfx,0,0,CALLBACK_NULL);
#endif
	
	if (status != MMSYSERR_NOERROR) return status;

	buf_size = m_pcmspec.time_to_bytes(p_buffer_length);
	buf_size -= buf_size % m_pcmspec.align();
	
	m_maxblock = min(0x10000,buf_size>>2);
	m_avgblock = min(0x10000,buf_size>>4);


	buffer = (char*)LocalAlloc(LMEM_FIXED|LMEM_ZEROINIT,buf_size+m_maxblock);//extra space at the end of the buffer


	n_playing=0;

	waveOutRestart(hWo);
	reset_stuff();

	SetVolume(m_volume);

	Update();
	
	return MMSYSERR_NOERROR;
}

unsigned WaveOut::GetLatencyBytes()
{
	if (hWo)
	{
		unsigned buffered = 0;
		MMTIME time;
		memset(&time,0,sizeof(time));
		time.wType = TIME_BYTES;
		if (waveOutGetPosition(hWo,&time,sizeof(time))==MMSYSERR_NOERROR)
		{	
			__int64 blah = time.u.cb;
			if (blah + 0x100000 < total_played)//ms idiocy fix
				total_played = (blah&0xFFFFF) | (total_played&~0xFFFFF);
			else
				total_played = blah;
			buffered = (unsigned)(total_written - total_played);
		}
		return buffered + data_written;
	}
	else return 0;
}

double WaveOut::GetLatency()
{
	if (m_pcmspec.is_valid())
	{
		return m_pcmspec.bytes_to_time(GetLatencyBytes());
	}
	else return 0;
}


void WaveOut::reset_stuff()
{
	write_ptr=0;
	m_paused = false;
	m_needplay = false;
	n_playing=0;
	data_written=0;
    buf_size_used=0;
	total_written = 0;
	total_played = 0;
}

MMRESULT WaveOut::Pause(bool p_state)
{
	if (m_paused != p_state)
	{
		m_paused = p_state;
		if (hWo)
		{
			if (m_paused)
				return waveOutPause(hWo);
			else
				return waveOutRestart(hWo);
		}
		else return MMSYSERR_NOERROR;
	}
	else return MMSYSERR_NOERROR;
}

void WaveOut::killwaveout()
{
	if (hWo)
	{
		flush();
		waveOutClose(hWo);
		hWo=0;
	}
}

unsigned WaveOut::CanWrite()
{
	return m_paused ? 0 : buf_size-buf_size_used-data_written;
}


WaveOut::HEADER * WaveOut::hdr_alloc()
{
	HEADER * r;
	if (hdrs_free)
	{
		r=hdrs_free;
		hdrs_free=hdrs_free->next;
	}
	else
	{
		r=new HEADER;
	}
	r->next=hdrs;
	hdrs=r;
	memset(&r->hdr,0,sizeof(WAVEHDR));
	return r;
}

void WaveOut::hdr_free(HEADER * h)
{
	HEADER ** p=&hdrs;
	while(*p)
	{
		if (*p==h)
		{
			*p = (*p)->next;
			break;
		}		
		else p=&(*p)->next;
	}

	h->next=hdrs_free;
	hdrs_free=h;
}

void WaveOut::hdr_free_list(HEADER * h)
{
	while(h)
	{
		HEADER * t=h->next;
		delete h;
		h=t;
	}
}

MMRESULT WaveOut::SetVolume(double p_volume)
{
	m_volume = p_volume;

	if (hWo != 0)
	{
		unsigned scale_int = (unsigned)(pow(10,p_volume / 20) * 0xFFFF);
		if (scale_int > 0xFFFF) scale_int = 0xFFFF;
			
		DWORD status = (scale_int & 0xFFFF) | ((scale_int & 0xFFFF) << 16);
		return waveOutSetVolume(hWo,status);
	}
	else return MMSYSERR_NOERROR;
}
