#include "ParticleShadowMap.h"
#include "util/SlurpFiles.h"

#include "stb_image_write.h"

ParticleShadowMap::ParticleShadowMap(size_t width, size_t height, size_t depth)
: width(width), height(height), depth(depth)
{
    assert(depth % 4 == 0);

    while (1) {
        auto psmPropagateCSSource = util::SlurpTextFile("assets\\psmPropagateCS.glsl");
        auto cs = util::CompileShader(GL_COMPUTE_SHADER, psmPropagateCSSource);
        auto link = util::LinkProgram({cs.id});
        OutputDebugStringA("CS:\n"); OutputDebugStringA(cs.infoLog.c_str());
        OutputDebugStringA("Link:\n"); OutputDebugStringA(link.infoLog.c_str());
        propagateProgram = link.id;
        if (cs.success && link.success)
            break;
        DebugBreak();
    }

    // Prepare volume textures
    volumeTexture = util::CreateTexture(GL_TEXTURE_3D);
    glTextureStorage3D(*volumeTexture, 1, GL_R8, width, height, depth);
    glTextureParameteri(*volumeTexture, GL_TEXTURE_MAX_LEVEL, 0);
    glTextureParameteri(*volumeTexture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(*volumeTexture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTextureParameteri(*volumeTexture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*volumeTexture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*volumeTexture, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    GLint prevFBO{};
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &prevFBO);
    
    // Build FBO
    fbo = util::CreateFramebuffer();
    glBindFramebuffer(GL_FRAMEBUFFER, *fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, prevFBO);
    glNamedFramebufferTexture(*fbo, GL_COLOR_ATTACHMENT0, *volumeTexture, 0);
    auto status = glCheckNamedFramebufferStatus(*fbo, GL_DRAW_FRAMEBUFFER);
    GLenum err{};
    switch (status) {
    case GL_FRAMEBUFFER_COMPLETE: break;
    case 0: err = glGetError(); break;
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
    case GL_FRAMEBUFFER_UNSUPPORTED:
    case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
    case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
    default:
        abort(); break;
    }

}

void ParticleShadowMap::Clear() {
    uint8_t const ones[] = { 0xFF, 0xFF, 0xFF, 0xFF };
    glClearTexImage(*volumeTexture, 0, GL_RED, GL_UNSIGNED_BYTE, ones);
}

void ParticleShadowMap::BindAsTarget() {
    glBindFramebuffer(GL_FRAMEBUFFER, *fbo);
}

void ParticleShadowMap::BindAsTexture(GLint base) {
    glBindTextures(base, 1, volumeTexture.get());
}

void ParticleShadowMap::SaveTiled(char const* filename) {
    (void)filename;
#define PC_DUMP_TILES 1
#define PC_DUMP_COLLAGE 1
#if PC_DUMP_TILES
    std::string name = std::string(filename) + ".png";
    auto ComposeCollage = [&](GLuint tex, std::string name) {
        std::vector<uint8_t> buf(width * height * depth);
        glGetTextureImage(tex, 0, GL_RED, GL_UNSIGNED_BYTE, buf.size(), buf.data());
#define PC_GRID_COLLAGE 1
#if PC_DUMP_COLLAGE
        size_t imagesPerLine = (int)sqrt(depth);
        size_t lineStride = width * imagesPerLine;
        size_t lines = depth / imagesPerLine + !!(depth % imagesPerLine);
        std::vector<uint8_t> collage(lineStride * height * lines);
        auto* p = buf.data();
        for (size_t slice = 0; slice < depth; ++slice) {
            size_t column = slice % imagesPerLine;
            size_t line = slice / imagesPerLine;
            auto* q = collage.data() + column * width + line * lineStride * height;
            for (size_t pixelRow = 0; pixelRow < height; ++pixelRow) {
                memcpy(q, p, width * 1);
#if PC_GRID_COLLAGE
                if (pixelRow == 0 || pixelRow == height - 1) {
                    memset(q, 0x30, width * 1);
                }
                else {
                    q[0] = q[width - 1] = 0x30;
                }
#endif
                p += width * 1;
                q += lineStride;
            }
        }
        stbi_write_png(name.c_str(), width * imagesPerLine, height * lines, 1, collage.data(), lineStride);
#else
        stbi_write_png(name.c_str(), width, height * depth, 1, buf.data(), width * 1);
#endif
    };
    ComposeCollage(*volumeTexture, name);
#endif
}

void ParticleShadowMap::Propagate() {
    glUseProgram(*propagateProgram);
    glBindImageTexture(0, *volumeTexture, 0, GL_TRUE, 0, GL_READ_WRITE, GL_R8);
    glUniform1i(glGetUniformLocation(*propagateProgram, "u_Volume"), 0);
    glDispatchCompute(width, height, 1);
    glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R8);
#define PC_FILL_GRADIENT 0
#define PC_FILL_CHECKERBOARD 0
#if PC_FILL_GRADIENT
    std::vector<char> buf(width * height * depth);
    char* p = buf.data();
    for (size_t slice = 0; slice < depth; ++slice) {
        for (size_t row = 0; row < height; ++row) {
            for (size_t col = 0; col < width; ++col) {
                *p++ = static_cast<uint8_t>(0xFFU * slice / depth);
            }
        }
    }
#endif
#if PC_FILL_CHECKERBOARD
    std::vector<char> buf(width * height * depth);
    char* p = buf.data();
    for (size_t slice = 0; slice < depth; ++slice) {
        for (size_t row = 0; row < height; ++row) {
            for (size_t col = 0; col < width; ++col) {
                if ((col%64) < 32 != (row%64) >= 32)
                    *p = 0xFFU;
                else
                    *p = 0x00;
                ++p;
            }
        }
    }
#endif
#if PC_FILL_GRADIENT || PC_FILL_CHECKERBOARD
    glTextureSubImage3D(*volumeTexture, 0, 0, 0, 0, width, height, depth, GL_RED, GL_UNSIGNED_BYTE, buf.data());
#endif
}