#include "DiscreteField.h"

#include <algorithm>

DiscreteField::DiscreteField(float3 cellSize, float3 cellCount)
    : cellSize(cellSize), cellCount(cellCount)
    , lcg(9001u)
{
    cells.resize(static_cast<size_t>(cellCount.ProductOfElements()), float3::zero);
    std::generate(cells.begin(), cells.end(), [&]{ return float3::RandomDir(lcg); });
}

float3 DiscreteField::Sample(float3 tc) const {
    tc = tc.Div(cellSize);
    float3 wholes, fracs;
    for (size_t i = 0; i < 3; ++i) {
        tc[i] = std::fmod(tc[i], cellCount[i]);
        if (tc[i] < 0.0f) {
            tc[i] += cellCount[i];
        }
        fracs[i] = std::modf(tc[i], &wholes[i]);
    }
    float3 const ret = Lerp(
        Lerp(
            Lerp(Load(wholes + float3(0.0f, 0.0f, 0.0f)), Load(wholes + float3(1.0f, 0.0f, 0.0f)), fracs[0]),
            Lerp(Load(wholes + float3(0.0f, 1.0f, 0.0f)), Load(wholes + float3(1.0f, 1.0f, 0.0f)), fracs[0]),
            fracs[1]),
        Lerp(
            Lerp(Load(wholes + float3(0.0f, 0.0f, 1.0f)), Load(wholes + float3(1.0f, 0.0f, 1.0f)), fracs[0]),
            Lerp(Load(wholes + float3(0.0f, 1.0f, 1.0f)), Load(wholes + float3(1.0f, 1.0f, 1.0f)), fracs[0]),
            fracs[1]),
        fracs[2]);
    return ret;
}

float3 DiscreteField::Load(float3 wholes) const {
    return cells.at(MakeIndex(
        static_cast<size_t>(wholes[0]),
        static_cast<size_t>(wholes[1]),
        static_cast<size_t>(wholes[2])));
}

size_t DiscreteField::MakeIndex(size_t x, size_t y, size_t z) const {
    size_t const xWrap = x % static_cast<size_t>(cellCount[0]);
    size_t const yWrap = y % static_cast<size_t>(cellCount[1]);
    size_t const zWrap = z % static_cast<size_t>(cellCount[2]);
    return xWrap + yWrap * static_cast<size_t>(cellCount[0]) + zWrap * static_cast<size_t>(cellCount[1] * cellCount[0]);
}