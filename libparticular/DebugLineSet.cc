#include "DebugLineSet.h"
#include "util/SlurpFiles.h"

DebugLineSet::DebugLineSet() {
    while (1) {
        auto vsSource = util::SlurpTextFile("assets\\debugLineVS.glsl");
        auto fsSource = util::SlurpTextFile("assets\\debugLineFS.glsl");
        auto vs = util::CompileShader(GL_VERTEX_SHADER, vsSource);
        auto fs = util::CompileShader(GL_FRAGMENT_SHADER, fsSource);
        auto link = util::LinkProgram({vs.id, fs.id});
        OutputDebugStringA("VS:\n"); OutputDebugStringA(vs.infoLog.c_str());
        OutputDebugStringA("FS:\n"); OutputDebugStringA(fs.infoLog.c_str());
        OutputDebugStringA("Link:\n"); OutputDebugStringA(link.infoLog.c_str());
        program = link.id;
        if (vs.success && fs.success && link.success)
            break;
        DebugBreak();
    }
}

void DebugLineSet::Add(float3 vA, float3 vB, float4 cA, float4 cB) {
    vertices.push_back({vA, cA});
    vertices.push_back({vB, cB});
}

void DebugLineSet::Clear() {
    vertices.clear();
}

void DebugLineSet::Draw(Frustum const& frustum) {
    if (vertices.empty()) return;
    util::ResourcePtr vb = util::CreateBuffer();
    glBindBuffer(GL_ARRAY_BUFFER, *vb);
    glNamedBufferData(*vb, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
    glUseProgram(*program);
    GLint vao{};
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &vao);
    glVertexArrayVertexBuffer(vao, 0, *vb, 0, sizeof(Vertex));
    glVertexArrayAttribBinding(vao, 0, 0);
    glVertexArrayAttribBinding(vao, 1, 0);
    glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribFormat(vao, 1, 4, GL_FLOAT, GL_FALSE, sizeof(float3));
    glVertexArrayBindingDivisor(vao, 0, 0);
    glVertexArrayBindingDivisor(vao, 1, 0);
    glEnableVertexArrayAttrib(vao, 0);
    glEnableVertexArrayAttrib(vao, 1);
    glUniformMatrix4fv(glGetUniformLocation(*program, "u_VP"), 1, GL_FALSE, frustum.ComputeViewProjMatrix().Transposed().ptr());
    glDrawArrays(GL_LINES, 0, vertices.size());
}