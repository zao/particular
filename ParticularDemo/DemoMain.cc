#include "particular/Particular.h"

#include <Windows.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#pragma comment(lib,"SDL2.lib")

namespace particular {
	void Host::GetSize(int* width, int* height) {
		SDL_Window* window = static_cast<SDL_Window*>(this->wnd);
		SDL_GetWindowSize(window, width, height);
	}

	double Host::GetWallTime() {
		static double freq = static_cast<double>(SDL_GetPerformanceFrequency());
		uint64_t counter = SDL_GetPerformanceCounter();
		return static_cast<double>(counter) / freq;
	}
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	SetProcessDPIAware();
    SDL_Init(SDL_INIT_NOPARACHUTE | SDL_INIT_VIDEO | SDL_INIT_AUDIO);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
    SDL_Window* wnd = SDL_CreateWindow("particular", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280, 720, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    SDL_GLContext context = SDL_GL_CreateContext(wnd);
	(void)context;

	{
		particular::Host parHost{};
		parHost.wnd = wnd;
		auto parInstance = particular::CreateInstance(&parHost);
		bool stopRunning{};
		while (1) {
			SDL_Event ev;
			while (SDL_PollEvent(&ev)) {
				if (ev.type == SDL_KEYUP && ev.key.keysym.sym == SDLK_ESCAPE) {
					stopRunning = true;
				}
				if (ev.type == SDL_QUIT) {
					stopRunning = true;
				}
			}
			if (stopRunning) {
				break;
			}
			parInstance->RunFrame();
			SDL_GL_SwapWindow(wnd);
		}
	}
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(wnd);
    SDL_Quit();
	return 0;
}